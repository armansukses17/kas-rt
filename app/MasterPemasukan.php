<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterPemasukan extends Model
{
    protected $table = 'master_pemasukan';
    protected $fillable = [
        'nama_pemasukan',
        'keterangan'
    ];
}
