@extends('master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h2>Kas Masuk</h2>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body" style="padding: 0;">
                            <div class="col-lg-12" style="padding: 0;">
                                <div class="card card-primary card-outline card-outline-tabs" style="margin: 0;">
                                    <div class="card-header p-0 border-bottom-0">
                                        <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="tabKasUmum" data-toggle="pill" href="#custom-tabs-three-kas-umum" role="tab" aria-controls="custom-tabs-three-kas-umum" aria-selected="true">Kas Umum</a>
                                            </li>
                                            <li class="nav-item">
                                                <?php 
                                                if ($checkMasterPemasukan == 'ready') {
                                                    $tabKasWarga = 'tabKasBulanan';
                                                } else {
                                                    $tabKasWarga = 'custom-tabs-not-ready';
                                                }
                                                ?>
                                                <a class="nav-link" id="{{ $tabKasWarga }}" data-toggle="pill" href="#custom-tabs-three-kas-bulanan" role="tab" aria-controls="custom-tabs-three-kas-bulanan" aria-selected="false">Kas Bulanan</a>
                                            </li>
                                            <!-- <li class="nav-item pull-right" style="position: absolute;right: 0;">
                                                <a class="btn btn-success pull-right" href="#cccccc" role="tab">Perbarui Nama Warga</a>
                                            </li> -->
                                        </ul>
                                    </div>
                                    <div class="card-body" id="cardBodyContentTab">
                                        <div class="tab-content" id="custom-tabs-three-tabContent">
                                            <div class="tab-pane fade active show" id="custom-tabs-three-kas-umum" role="tabpanel" aria-labelledby="tabKasUmum">
                                                 <div id="dataKasUmum" class="card-body table-responsive p-0 mt-3">
                                                    <!-- <div class="hidden-sm hidden-xs btn-group mb-2">
                                                        <a href="#modalAddData" id="btnAddTransPemasukan" role="button" class="btn btn-danger btn-sm btn-add" data-toggle="modal"> Tambah Data </a>
                                                    </div>
                                                    <hr> -->
                                                    <div class="tile">
                                                        <form class="row">
                                                            <div class="form-group col-md-6">
                                                                <div class="row">
                                                                    <div class="form-group col-md-12" id="outerFilter">
                                                                        <div class="input-group">
                                                                            <div class="form-outline">
                                                                                <select class="chosen-select form-control" name="filterDataKasUmum" id="filterDataKasUmum" style="width: 150px;">
                                                                                    <?php 
                                                                                    $year = date('Y');
                                                                                    for ($i=2021; $i <= $year; $i++)
                                                                                    { 
                                                                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            </div>
                                                                            <!-- <button type="button" class="btn btn-primary btn-sm"> Filter </button> -->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6"> 
                                                                <a href="#modalAddData" id="btnAddTransPemasukan" role="button" class="btn btn-danger btn-md btn-add" data-toggle="modal" style="float: right;"> Tambah Data </a>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <table class="table table-hover table-bordered" id="tblDataKasUmum">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 10px;">No</th>
                                                                <th style="width: 120px;">Jenis Pemasukan</th>
                                                                <th style="width: 70px;">Tanggal / Bulan</th>
                                                                <th>Nominal</th>
                                                                <th style="width: 200px;">Keterangan</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="custom-tabs-three-kas-bulanan" role="tabpanel" aria-labelledby="tabKasBulanan">
                                                <div class="card-body table-responsive p-0 mt-3">
                                                    <div class="form-group col-md-12">
                                                        <div class="input-group">
                                                            <div class="form-outline">
                                                                <select class="chosen-select form-control" name="filterDataKasBulanan" id="filterDataKasBulanan" style="width: 150px;">
                                                                    <?php 
                                                                    $year = date('Y');
                                                                    for ($i=2021; $i <= $year; $i++)
                                                                    { 
                                                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="dataKasBulanan">
                                
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modalAddData" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-modal="true">
    <div class="modal-dialog modalLarge">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Kas Masuk</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="msgAlert"></div>
            <div id="addTransPemasukan">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="addTransactionIn"> 
                                {{ csrf_field() }}
                                <table class="table table-bordered table-hover" id="tableAddData">
                                    <thead style="line-height: 30px;">
                                        <tr>
                                            <th style="width: 230px;">Jenis pemasukan</th>
                                            <th style="width: 125px;">Tanggal Pemasukan</th>
                                            <th style="width: 125px;">Nominal</th>
                                            <th style="width: 210px;">Keterangan</th>
                                            <th width="5%">#</th>
                                        </tr>
                                    </thead>
                                    <tbody id="bodyListDataUmum">
                                        <tr id="row-1">
                                            <td>
                                                <select class="chosen-select form-control" name="jenis_pemasukan[]" id="jenis_pemasukan-1">
                                                    <option value="">-- Jenis Pemasukan --</option>
                                                    @foreach ($masterPemasukan as $row)
                                                        <option value="{{ $row->id }}">{{ $row->nama_pemasukan }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" name="tanggal_pemasukan[]" id="tanggal_pemasukan-1" class="form-control datepicker-bts-modal" autocomplete="off">
                                            </td>
                                            <td>
                                               <input type="text" name="nominal[]" id="nominal-1" class="form-control money" autocomplete="off">
                                            </td>
                                            <td>
                                                <textarea class="form-control" rows="2" name="keterangan[]"  id="keterangan-1" placeholder="Keterangan ..."></textarea>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-md btn-group"> 
                                                    <button type="button" class="btn btn-md btn-danger btn-remove delete-record" title="Remove" data-id="remove-1">
                                                        <i class="fa fa-times" aria-hidden="true" style="line-height: 23px;"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="modal-footer" style="padding-right: 0;">
                                    <button type="button" class="btn btn-md btn-warning btn-add pull-right" onclick="addItem()" title="Tambah">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Tambah
                                    </button>
                                    <input type="submit" class="btn btn-primary" value="Simpan">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- End: modal -->

<style>
.modal tbody#bodyListDataUmum td {
    padding: 2px;
}
/* ----------------- */
.modal .table th, 
.modal .table td,
.modal .table td input,
.modal .table td textarea,
.modal .table td select {
    font-size: 10px;
} 
#bodyListData td {
    padding: 0;
}
#bodyListData td.normal-pd {
     padding: 3px 10px;
}
#bodyListData input {
    padding: 0 10px !important;
    text-align: right;
    height: 30px;
    font-size: 10px;
}
#bodyListData input.nmWarga {
    padding: 0 10px !important;
    text-align: left;
}
#totalKas {
    background: #e2f0ff;
    color: #007bff;
    height: 70px;
    line-height: 70px;
    text-align: center;
    font-size: 20px;
    font-weight: bold;
}
#tableKasBulananWarga tr th,
#tableKasBulananWarga tr td {
    font-size: 10px;
}
#tableKasBulananWarga thead th {
    padding: 2px 5px;
}
.footRow {
    height: 50px;
    background: #e2f0ff;
}
</style>

<script>
var count = 0;

$(function() {
    let yearsNow = '<?php echo date('Y'); ?>';
    $('#filterDataKasUmum, #filterDataKasBulanan').val(yearsNow)
    callKasUmum();
});

$('#btnAddTransPemasukan').on('click', function() {
    $('#addTransactionIn')[0].reset(); // reset form
    $('.footer-close').show();
});

$('#custom-tabs-not-ready').on('click', function() {
    alert('Data ini belum siap, silahkan isi master pemasukan terlebih dahulu !');
})

function focusCol(id, value) {
    if (value == 0) {
        $('#'+id).select();
    }
}

function changeValue(id, value, title) {
    var num = id.match(/[\d\.]+/g);
    var colValue = $('#'+id).val();
    if (colValue != '') {
        var nominalPerMont = '';
        var explode = title.split("***");
        var getMonthNumber = explode[1];
        var monthUpdate = getMonthNumber;
        // ------------------------------------
        var theID = num.toString();
        var getMonthName = explode[0];
        updateKasWarga(theID, value, title, getMonthName);
        // ------------------------------------
        setTimeout(function() { 
            var month_1 = $('#subtotal_januari').attr('title');
            var month_2 = $('#subtotal_februari').attr('title');
            var month_3 = $('#subtotal_maret').attr('title');
            var month_4 = $('#subtotal_april').attr('title');
            var month_5 = $('#subtotal_mei').attr('title');
            var month_6 = $('#subtotal_juni').attr('title');
            var month_7 = $('#subtotal_juli').attr('title');
            var month_8 = $('#subtotal_agustus').attr('title');
            var month_9 = $('#subtotal_september').attr('title');
            var month_10 = $('#subtotal_oktober').attr('title');
            var month_11 = $('#subtotal_november').attr('title');
            var month_12 = $('#subtotal_desember').attr('title'); 

            var totalMonth_1 = $('#subtotal_januari').text();
            var totalMonth_2 = $('#subtotal_februari').text();
            var totalMonth_3 = $('#subtotal_maret').text();
            var totalMonth_4 = $('#subtotal_april').text();
            var totalMonth_5 = $('#subtotal_mei').text();
            var totalMonth_6 = $('#subtotal_juni').text();
            var totalMonth_7 = $('#subtotal_juli').text();
            var totalMonth_8 = $('#subtotal_agustus').text();
            var totalMonth_9 = $('#subtotal_september').text();
            var totalMonth_10 = $('#subtotal_oktober').text();
            var totalMonth_11 = $('#subtotal_november').text();
            var totalMonth_12 = $('#subtotal_desember').text();     

            // =================================================================

            if (month_1 == monthUpdate) { nominalPerMont = totalMonth_1; }
            else if (month_2 == monthUpdate) { nominalPerMont = totalMonth_2; }
            else if (month_3 == monthUpdate) { nominalPerMont = totalMonth_3; }
            else if (month_4 == monthUpdate) { nominalPerMont = totalMonth_4; }
            else if (month_5 == monthUpdate) { nominalPerMont = totalMonth_5; }
            else if (month_6 == monthUpdate) { nominalPerMont = totalMonth_6; }
            else if (month_7 == monthUpdate) { nominalPerMont = totalMonth_7; }
            else if (month_8 == monthUpdate) { nominalPerMont = totalMonth_8; }
            else if (month_9 == monthUpdate) { nominalPerMont = totalMonth_9; }
            else if (month_10 == monthUpdate) { nominalPerMont = totalMonth_10; }
            else if (month_11 == monthUpdate) { nominalPerMont = totalMonth_11; }
            else if (month_12 == monthUpdate) { nominalPerMont = totalMonth_12; }
            else { monthUpdate = 0; }
            
            var month = title;
            var updateFrom = 'Kas bulanan rt';
            var nominal = nominalPerMont;
            var date = title;
            updateTransaksiPemasukan(date, nominal, updateFrom, month);
        }, 200);
    }
    return false;
}

function updateKasWarga(id, value, title, onlyMonth) {
    $.ajax({
        type: "PUT",
        url : "{{ '/updateKasWarga' }}",
        data: {
            _token: '{{ csrf_token() }}',
            id: id,
            value: value,
            title: title,
            year: $('#filterDataKasBulanan').val()
        },
        dataType: "json",
        async: false,
        success: function(data) {
            if (data.s == 'success') {
                toastr.success('Berhasil diinput !');
                // isiTableKasWarga();
                // -----------------------------------------
                var monthClass = onlyMonth+'Class';
                var subTotalPerMonth = 0;
                setTimeout(function() {   
                    $("tbody#bodyListData").each(function() {
                        var length = $(this).find('.'+monthClass).length;
                        // alert(length)
                        var $no = 0;
                        for (i=1; i<=length; i++) {
                            var str = $(this).find('.'+monthClass).eq($no).val();
                            var deleteDot = str.replace(/\./g, '');
                            subTotalPerMonth += parseInt(deleteDot);
                            $no++;
                        }
                    });
                    $('#subtotal_'+onlyMonth).text(subTotalPerMonth);
                    myTotalKas();
                }, 200);
                return false;
            } 
            else {
                toastr.error(data.m);
            }
        },
        error: function(data) {
            toastr.error('Error !');
        }
    });
    return false; 
}

function updateTransaksiPemasukan(date, nominal, ket, month) { 
    $.ajax({
        type: "POST",
        url : "{{ '/saveTransPemasukanBulanan' }}",
        data: {
            _token: '{{ csrf_token() }}',
            tanggal_pemasukan: date,
            nominal: nominal,
            keterangan: ket,
            month: month,
            year: $('#filterDataKasBulanan').val()
        },
        dataType: "json",
        success: function(data) {
            if (data.s == 'success') {
                console.log('Ok');
            } 
            else {
                console.log('Error');
            }
        },
        error: function(data) {
            console.log('Error !!!');
        }
    }); 
    return false;
}

$('#addTransactionIn').on('submit', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        type: "POST",
        url : "{{ '/saveTransPemasukanUmum' }}",
        data: formData,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
            if (data.s == 'success') {
                toastr.success(data.m);
                callKasUmum();
                $('.modal').modal('hide');
                $('#addTransactionIn')[0].reset(); // reset form
            } 
            else {  
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">'+value+'</span>';
                });
                toastr.error(messages);
            }
        },
        error: function(data) {
            toastr.error('Error !!!');
        }
    }); 
});

function myTotalKas() {
    setTimeout(function() { 
        var month_1 = $('#subtotal_januari').text();
        var month_2 = $('#subtotal_februari').text();
        var month_3 = $('#subtotal_maret').text();
        var month_4 = $('#subtotal_april').text();
        var month_5 = $('#subtotal_mei').text();
        var month_6 = $('#subtotal_juni').text();
        var month_7 = $('#subtotal_juli').text();
        var month_8 = $('#subtotal_agustus').text();
        var month_9 = $('#subtotal_september').text();
        var month_10 = $('#subtotal_oktober').text();
        var month_11 = $('#subtotal_november').text();
        var month_12 = $('#subtotal_desember').text();  
   
        // alert(month_12)
        var totalKas = parseInt(month_1) + parseInt(month_2) + parseInt(month_3) + parseInt(month_4) + parseInt(month_5) + parseInt(month_6) +
                        parseInt(month_7) + parseInt(month_8) + parseInt(month_9) + parseInt(month_10) + parseInt(month_11) + parseInt(month_12);
        $('#totalKasNominal').text(formatCurrency(totalKas));
     }, 100);
    return false;
}

function listTableKasUmum(filterBy='') {
    $('#tblDataKasUmum').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ '/dtTransPemasukan' }}",
            data: {
                // _token: '{{ csrf_token() }}',
                filterBy: filterBy
            },
            type: "GET"
        },
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false }, // no autoincrement
            { data: 'nama_pemasukan', name: 'nama_pemasukan' },
            { data: 'tanggalBulanan', name: 'tanggalBulanan' },
            { data: 'formatNominal', name: 'formatNominal' },
            { data: 'keteranganBulanan', name: 'keteranganBulanan' },
            // { data: 'action', name: 'action' }
        ],
        // "order": [[ '1', "desc" ]]
    });
}

function callKasUmum() {
    $("#tblDataKasUmum").dataTable().fnDestroy();
    let filterByYear = $('#filterDataKasUmum').val();
    listTableKasUmum(filterByYear);
}

function callColumnKasBulanan() {
    $('#dataKasBulanan').html(`
            <div class="tile mt-3">
            <div class="card-body table-responsive p-0">
                <table id="tableKasBulananWarga" class="table table-bordered table-hover" style="border: 1px solid #ccc; background: #fff; margin-bottom: 5px;">
                    <thead style="line-height: 30px;">
                        <tr>
                            <th style="width: 10px;">No</th>
                            <th width="200px">NAMA WARGA</th>
                            <th width="60px" class="text-center">JANUARI</th>
                            <th width="60px" class="text-center">FEBRUARI</th>
                            <th width="60px" class="text-center">MARET</th>
                            <th width="60px" class="text-center">APRIL</th>
                            <th width="60px" class="text-center">MEI</th>
                            <th width="60px" class="text-center">JUNI</th>
                            <th width="60px" class="text-center">JULI</th>
                            <th width="60px" class="text-center">AGUSTUS</th>
                            <th width="60px" class="text-center">SEPTEMBER</th>
                            <th width="60px" class="text-center">OKTOBER</th>
                            <th width="60px" class="text-center">NOVEMBER</th>
                            <th width="60px" class="text-center">DESEMBER</th>
                        </tr>
                    </thead>
                    <tbody id="bodyListData">
                        <img src="images/loading.gif" id="loading" alt="Loading ..." style="position: absolute; top: 50%; left: 50%;">
                    </tbody>
                    <tfoot id="footerListData" style="display: none;">
                        <tr class="footRow">
                            <td colspan="2" class="text-center" style="font-size: 15px;"><b>SUBTOTAL :</b></td>
                            <td id="subtotal_januari" class="text-center" title="01"></td>
                            <td id="subtotal_februari" class="text-center" title="02"></td>
                            <td id="subtotal_maret" class="text-center" title="03"></td>
                            <td id="subtotal_april" class="text-center" title="04"></td>
                            <td id="subtotal_mei" class="text-center" title="05"></td>
                            <td id="subtotal_juni" class="text-center" title="06"></td>
                            <td id="subtotal_juli" class="text-center" title="07"></td>
                            <td id="subtotal_agustus" class="text-center" title="08"></td>
                            <td id="subtotal_september" class="text-center" title="09"></td>
                            <td id="subtotal_oktober" class="text-center" title="10"></td>
                            <td id="subtotal_november" class="text-center" title="11"></td>
                            <td id="subtotal_desember" class="text-center" title="12"></td>
                        </tr>
                    </tfoot>
                </table>
                <div id="totalKas" style="display: none;">
                    <span style="color: #000;">TOTAL KAS BULANAN (Rp) :</span> 
                    <span id="totalKasNominal"></span>
                </div>
            </div>
        </div>
    `);
    setTimeout(function() { 
        $('#loading').hide();
        isiTableKasWarga();
    }, 500);
    return false;
}

$('#tabKasUmum').on('click', function() {
    $('#dataKasUmum').show();
    callKasUmum();
    $('#cardBodyContentTab').css('padding', '15px');
});

$('#tabKasBulanan').on('click', function() {
    $('#dataKasUmum').hide();
    callColumnKasBulanan();
    $('#cardBodyContentTab').css('padding', '0');
});

function isiTableKasWarga() {
    var selectYear = $('#filterDataKasBulanan').val();
    $.ajax({
        type: "GET",
        url : "{{ '/getKasWarga/' }}"+selectYear,
        dataType: "json",
        async: false,
        success: function(data) {
            if (data.s == 'success') 
            {
                let thisDisabled = '';
                if (data.year == 'thisYear') 
                {
                    thisDisabled = '';
                }
                else if (data.year == 'lastYear') 
                {
                    thisDisabled = 'disabled';
                }
                else
                {
                    thisDisabled = '';
                }

                var html = '';
                var start = '';
                var subtotalJanuari = 0; var subtotalFebruari = 0; var subtotalMaret = 0; var subtotalApril = 0; var subtotalMei = 0; var subtotalJuni = 0;
                var subtotalJuli = 0; var subtotalAgustus = 0; var subtotalSeptember = 0; var subtotalOktober = 0; var subtotalNovember = 0; var subtotalDesember = 0;
                // ---------------------------------------------
                var format_januari = '';
                var format_februari = '';
                var format_maret = '';
                var format_april = '';
                var format_mei = '';
                var format_juni = '';
                var format_juli = '';
                var format_agustus = '';
                var format_september= '';
                var format_oktober = '';
                var format_november = '';
                var format_desember = '';

                for (i=0; i<data.data.length; i++) {
                    start = i + 1;
                    format_januari = formatCurrency(data.data[i].januari);
                    format_februari = formatCurrency(data.data[i].februari);
                    format_maret = formatCurrency(data.data[i].maret);
                    format_april = formatCurrency(data.data[i].april);
                    format_mei = formatCurrency(data.data[i].mei);
                    format_juni = formatCurrency(data.data[i].juni);
                    format_juli = formatCurrency(data.data[i].juli);
                    format_agustus = formatCurrency(data.data[i].agustus);
                    format_september= formatCurrency(data.data[i].september);
                    format_oktober = formatCurrency(data.data[i].oktober);
                    format_november = formatCurrency(data.data[i].november);
                    format_desember = formatCurrency(data.data[i].desember);
                    html += `<tr id="row-${start}">
                            <td class="text-center noListData"></td>
                            <td class="normal-pd">
                                ${data.data[i].nama_warga}
                                <input type="hidden" name="idWarga[]" id="idWarga-${start}" value="${data.data[i].id_warga}" ${thisDisabled}>
                            </td>
                            <td>
                                <input type="text" name="januari[]" id="januari-${data.data[i].id}" onchange="changeValue(this.id, this.value, this.title)" onclick="focusCol(this.id, this.value)" title="januari***01" class="form-control money colNominal januariClass" value="${format_januari}" autocomplete="off" onkeypress="return isNumberKey(event)" ${thisDisabled}>
                            </td>
                            <td>
                                <input type="text" name="februari[]" id="februari-${data.data[i].id}" class="form-control money colNominal februariClass" onchange="changeValue(this.id, this.value, this.title)" onclick="focusCol(this.id, this.value)" title="februari***02" value="${format_februari}" autocomplete="off" onkeypress="return isNumberKey(event)" ${thisDisabled}>
                            </td>
                            <td>
                                <input type="text" name="maret[]" id="maret-${data.data[i].id}" class="form-control money colNominal maretClass" onchange="changeValue(this.id, this.value, this.title)" onclick="focusCol(this.id, this.value)" title="maret***03" value="${format_maret}" autocomplete="off" onkeypress="return isNumberKey(event)" ${thisDisabled}>
                            </td>
                            <td>
                                <input type="text" name="april[]" id="april-${data.data[i].id}" class="form-control money colNominal aprilClass" onchange="changeValue(this.id, this.value, this.title)" onclick="focusCol(this.id, this.value)" title="april***04" value="${format_april}" autocomplete="off" onkeypress="return isNumberKey(event)" ${thisDisabled}>
                            </td>
                            <td>
                                <input type="text" name="mei[]" id="mei-${data.data[i].id}" class="form-control money colNominal meiClass" onchange="changeValue(this.id, this.value, this.title)" onclick="focusCol(this.id, this.value)" title="mei***05" value="${format_mei}" autocomplete="off" onkeypress="return isNumberKey(event)" ${thisDisabled}>
                            </td>
                            <td>
                                <input type="text" name="juni[]" id="juni-${data.data[i].id}" class="form-control money colNominal juniClass" onchange="changeValue(this.id, this.value, this.title)" onclick="focusCol(this.id, this.value)" title="juni***06" value="${format_juni}" autocomplete="off" onkeypress="return isNumberKey(event)" ${thisDisabled}>
                            </td>
                            <td>
                                <input type="text" name="juli[]" id="juli-${data.data[i].id}" class="form-control money colNominal juliClass" onchange="changeValue(this.id, this.value, this.title)" onclick="focusCol(this.id, this.value)" title="juli***07" value="${format_juli}" autocomplete="off" onkeypress="return isNumberKey(event)" ${thisDisabled}>
                            </td>
                            <td>
                                <input type="text" name="agustus[]" id="agustus-${data.data[i].id}" class="form-control money colNominal agustusClass" onchange="changeValue(this.id, this.value, this.title)" onclick="focusCol(this.id, this.value)" title="agustus***08" value="${format_agustus}" autocomplete="off" onkeypress="return isNumberKey(event)" ${thisDisabled}>
                            </td>
                            <td>
                                <input type="text" name="september[]" id="september-${data.data[i].id}" class="form-control money colNominal septemberClass" onchange="changeValue(this.id, this.value, this.title)" onclick="focusCol(this.id, this.value)" title="september***09" value="${format_september}" autocomplete="off" onkeypress="return isNumberKey(event)" ${thisDisabled}>
                            </td>
                            <td>
                                <input type="text" name="oktober[]" id="oktober-${data.data[i].id}" class="form-control money colNominal oktoberClass" onchange="changeValue(this.id, this.value, this.title)" onclick="focusCol(this.id, this.value)" title="oktober***10" value="${format_oktober}" autocomplete="off" onkeypress="return isNumberKey(event)" ${thisDisabled}>
                            </td>
                            <td>
                                <input type="text" name="november[]" id="november-${data.data[i].id}" class="form-control money colNominal novemberClass" onchange="changeValue(this.id, this.value, this.title)" onclick="focusCol(this.id, this.value)" title="november***11" value="${format_november}" autocomplete="off" onkeypress="return isNumberKey(event)" ${thisDisabled}>
                            </td>
                            <td>
                                <input type="text" name="desember[]" id="desember-${data.data[i].id}" class="form-control money colNominal desemberClass" onchange="changeValue(this.id, this.value, this.title)" onclick="focusCol(this.id, this.value)" title="desember***12" value="${format_desember}" autocomplete="off" onkeypress="return isNumberKey(event)" ${thisDisabled}>
                            </td>
                        </tr>
                    `;
                    // ----------------------------------------
                    subtotalJanuari += data.data[i].januari;
                    subtotalFebruari += data.data[i].februari;
                    subtotalMaret += data.data[i].maret;
                    subtotalApril += data.data[i].april;
                    subtotalMei += data.data[i].mei;
                    subtotalJuni += data.data[i].juni;
                    subtotalJuli += data.data[i].juli;
                    subtotalAgustus += data.data[i].agustus;
                    subtotalSeptember += data.data[i].september;
                    subtotalOktober += data.data[i].oktober;
                    subtotalNovember += data.data[i].november;
                    subtotalDesember += data.data[i].desember;
                }
                $('#bodyListData').html(html);

                // ----------------------------------------
                $('#subtotal_januari').text(subtotalJanuari);
                $('#subtotal_februari').text(subtotalFebruari);
                $('#subtotal_maret').text(subtotalMaret);
                $('#subtotal_april').text(subtotalApril);
                $('#subtotal_mei').text(subtotalMei);
                $('#subtotal_juni').text(subtotalJuni);
                $('#subtotal_juli').text(subtotalJuli);
                $('#subtotal_agustus').text(subtotalAgustus);
                $('#subtotal_september').text(subtotalSeptember);
                $('#subtotal_oktober').text(subtotalOktober);
                $('#subtotal_november').text(subtotalNovember);
                $('#subtotal_desember').text(subtotalDesember);      
                // ----------------------------------------
                $('#footerListData, #totalKas').show();
                noAutoincrement();
                // ----------------------------------------    
                myTotalKas();
            } 
            else {
                alert('error');
            }
        }
    }); 
}

function noAutoincrement() {
    $("td.noListData").each(function(i, v) {
        $(v).text(i + 1);
    });
}

function addItem() {
	var no = count + 2;
	count++;
    // alert(no)
    var html = `
        <tr id="row-${no}">
            <td>
                <select class="chosen-select form-control" name="jenis_pemasukan[]" id="jenis_pemasukan-${no}">
                    <option value="">-- Jenis Pemasukan --</option>
                    @foreach ($masterPemasukan as $row)
                        <option value="{{ $row->id }}">{{ $row->nama_pemasukan }}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <input type="text" name="tanggal_pemasukan[]" id="tanggal_pemasukan-${no}" class="form-control datepicker-bts-modal" autocomplete="off">
            </td>
            <td>
                <input type="text" name="nominal[]" id="nominal-${no}" class="form-control money" autocomplete="off">
            </td>
            <td>
                <textarea class="form-control" rows="2" name="keterangan[]" id="keterangan-${no}" placeholder="Keterangan ..."></textarea>
            </td>
            <td>
                <div class="hidden-sm hidden-md btn-group"> 
                    <button type="button" class="btn btn-md btn-danger btn-remove delete-record" title="Remove" data-id="remove-${no}">
                        <i class="fa fa-times" aria-hidden="true" style="line-height: 23px;"></i>
                    </button>
                </div>
            </td>
        </tr>
    `;
	$("#bodyListDataUmum").append(html);
}

$(document).on( 'click', '.delete-record', function(e) {
	e.preventDefault();    
	var key = $(this).attr('data-id');
    var getID = key.replace("remove-", "");
    $('#row-'+ getID).remove();
    return true;
});

$('#filterDataKasUmum').on('change', function() {
    var currentYear = new Date().getFullYear();
    if (this.value < currentYear) {
        $('#btnAddTransPemasukan').hide();
    } else {
        $('#btnAddTransPemasukan').show();
    }
    callKasUmum();
});

$('#filterDataKasBulanan').on('change', function() {
    callColumnKasBulanan();
});
</script>
@endsection
