<?php

namespace App;

use App\MasterPemasukan;
use App\TransPemasukan;
use DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class LapPemasukanExcel implements FromView, ShouldAutoSize, WithEvents
{
    public function view(): View
    {
        $filterType = isset($_GET['filterType']) ? $_GET['filterType'] : '';
        $filterBy = isset($_GET['filterBy']) ? $_GET['filterBy'] : '';
        $yearWithMonth = isset($_GET['yearWithMonth']) ? $_GET['yearWithMonth'] : '';
        $format = '';
         
        if (empty($filterBy)) 
        {
            $value = 'notFound!';
        } 
        else 
        {
            if ($filterType == 1) 
            {
                $format = "m%Y";
                $value = "$filterBy$yearWithMonth";
                $infoReportBy = strtoupper(monthIndo($filterBy));
                $headerInfo = "DATA LAPORAN PEMASUKAN KAS PER BULAN $infoReportBy $yearWithMonth";
            }
            elseif ($filterType == 2) 
            {
                $format = 'Y';
                $value = $filterBy;
                $headerInfo = "DATA LAPORAN PEMASUKAN KAS PER TAHUN $value";
            }
        }

        $data = DB::table('transaksi_pemasukan as a')
            ->select('b.nama_pemasukan', DB::raw("(DATE_FORMAT(a.tanggal_pemasukan,'%d/%m/%Y')) as tanggal_pemasukan"), 'a.nominal', 'a.keterangan')
            ->leftJoin('master_pemasukan as b', 'b.id', '=', 'a.kategori_pemasukan_id')
            ->where(DB::raw("(DATE_FORMAT(a.tanggal_pemasukan, '%$format'))"), '=', $value)
            ->orderBy(DB::raw("(DATE_FORMAT(a.tanggal_pemasukan,'%m'))"))
            ->get();

        return view('laporan.lapPemasukanExcel', [
            'data' => $data,
            'headerInfo' => $headerInfo
        ]);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) 
            {
                $cellHeaderTitle = 'A1:E1'; // header title
                $styleArrayTitle = [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ];
                $event->sheet->getStyle($cellHeaderTitle)->applyFromArray($styleArrayTitle);
                // -----------------------------------------------------------------------------
                // -----------------------------------------------------------------------------
                $cellHeaderTable = 'A3:E3'; // header tbl
                $styleHeaderTable = [
                    'font' => [
                        'bold' => TRUE,
                        'size' => 13,
                        'color' => [ 'rgb' => 'ffffff' ]
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['rgb' => '000000'],
                        ],
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => '008000',
                        ]
                    ],
                ];
                // $event->sheet->getDelegate()->getStyle($cellHeaderTable)->getFont()->setSize(13);
                $event->sheet->getStyle($cellHeaderTable)->applyFromArray($styleHeaderTable);
            }
        ];
    }
}
