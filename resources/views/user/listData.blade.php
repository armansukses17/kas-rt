@extends('layout.app')
@section('user', 'active')
@section('page-header','Data User')
@section('content')
<div class="row">
    <!-- data table start -->
    <div class="col-12 mt-5">
        <div class="card">
            <div class="card-body">
                <div class="hidden-sm hidden-xs btn-group">
                    <a href="#modalAdd" role="button" id="modalAddData" class="btn btn-danger btn-sm btn-add" data-toggle="modal"><i class="fa fa-plus"></i> Tambah User </a>
                </div>
                <br><br>
                <!-- <div class="table-responsive"> -->
                    <table class="table table-hover table-bordered" id="tblData">
                        <thead class="bg-light text-capitalize">
                            <tr>
                                <th width="20px">No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th width="60px">Role</th>
                                <th width="60px">Status</th>
                                <th width="60px">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                <!-- </div> -->
            </div>
        </div>
    </div>
    <!-- data table end -->
</div>

<!-- start modal add -->
<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

            <form id="formAddData">
                <div class="modal-body">
                    <div class="row">
                        <div class="card-body">
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Nama User">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Email User">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password"  name="password" id="password" class="form-control" placeholder="Password User">
                                </div>
                                <div class="form-group">
                                    <label for="role">Role</label>
                                    <br>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="adminChecked" name="roleAccess" class="custom-control-input" value="admin">
                                        <label class="custom-control-label" for="adminChecked">Admin</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="rtChecked" name="roleAccess" class="custom-control-input" value="rt">
                                        <label class="custom-control-label" for="rtChecked">RT</label>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal add -->

<!-- start modal edit -->
<div class="modal fade" id="modalEditData" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            
            <form id="formEditData">
                {{ method_field('PUT') }}
                <div class="modal-body">
                    <div class="row">
                        <div class="card-body">
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input type="text" name="edit_name" id="edit_name" class="form-control" placeholder="Nama User">
                                    <input type="hidden" name="id" id="keyEdit"> 
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="edit_email" id="edit_email" class="form-control" placeholder="Email User">
                                </div>
                                <!-- <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="edit_password" id="edit_password" class="form-control" value="">
                                </div> -->
                                <div class="form-group">
                                    <label for="role">Role</label>
                                    <br>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="edit_adminChecked" name="edit_roleAccess" class="custom-control-input" value="admin">
                                        <label class="custom-control-label" for="edit_adminChecked">Admin</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="edit_rtChecked" name="edit_roleAccess" class="custom-control-input" value="rt">
                                        <label class="custom-control-label" for="edit_rtChecked">RT</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="role">Akun Aktif ?</label>
                                    <br>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="edit_active" name="edit_is_active" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="edit_active">Aktif</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="edit_nonActive" name="edit_is_active" class="custom-control-input" value="0">
                                        <label class="custom-control-label" for="edit_nonActive">Tidak Aktif</label>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal edit -->

<!-- start modal hapus -->
<div class="modal fade" id="modalHapus" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Modal Hapus Warga</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form action="{{url('deleteUser')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="card-body">
                        <div class="form-group">
                            Apakah kamu yakin ingin menghapus data <b class="nama"></b> ?
                            <!-- <label for="" class="warga"></label> -->
                            <input type="hidden" name="id" class="id">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Hapus</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal hapus -->
@endsection
@section('script')
<script>
$(document).on('ready', function() {
    // $('#tblData').DataTable().ajax.reload();
});
$(function() {
    $("#tblData").dataTable().fnDestroy();
    $('#tblData').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ '/datatablesUser' }}",
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false }, // no autoincrement
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'role', name: 'role' },
            { data: 'is_active', name: 'is_active' },
            { data: 'action', name: 'action' }
        ],
        "order": [[ '1', "desc" ]]
    });
});

$('#formAddData').on('submit', function(e) {
    e.preventDefault();
    var formData = new FormData(this);

    $.ajax({
        type: "POST",
        url : "{{ '/saveUser' }}",
        data: formData,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
            if (data.s == 'success') {
                toastr.success(data.m);
                $('#tblData').DataTable().ajax.reload();
                $('.modal').modal('hide');
                $('#formAddData')[0].reset(); // reset form
            } 
            else {
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">- '+value+'</span><br>';
                });
                toastr.error(messages);
            }
        },
        error: function(data) {
            toastr.error('Error !!!');
        }
    }); 
});

function editUser(param) {
    // $('#edit_password').val('');
    $.ajax({
        type: "GET",
        url : "{{ '/user/edit/' }}"+param,
        dataType: "json",
        success: function(data) {
            if (data.s == 'success') {
                $('#modalEditData').modal();
                $('#keyEdit').val(btoa(param));
                $('#edit_name').val(data.data[0].name);
                $('#edit_email').val(data.data[0].email);
                if (data.data[0].role == 'admin') {
                    $('#edit_adminChecked').prop('checked', true);
                } else {
                    $('#edit_rtChecked').prop('checked', true);
                }
                if (data.data[0].is_active == '1') {
                    $('#edit_active').prop('checked', true);
                } else {
                    $('#edit_nonActive').prop('checked', true);
                }
            } 
            else {
                alert('error');
            }
        }
    }); 
}

$('#formEditData').on('submit', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        type: "POST",
        url : "{{ '/updateUser' }}",
        data: formData,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
            if (data.s == 'success') {
                toastr.success(data.m); 
                $('#tblData').DataTable().ajax.reload();
                $('.modal').modal('hide');
            } 
            else {
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">- '+value+'</span><br>';
                });
                toastr.error(messages);
            }
        }
    }); 
});

// button hapus
$('table').on('click', '.btn-hapus', function () {
    var id = $(this).attr('data-value');
    var nama = $(this).attr('data-nama');
    $('.id').val(id);
    $('.nama').text(nama);
    $('#modalHapus').modal('show');
});
</script>
@endsection