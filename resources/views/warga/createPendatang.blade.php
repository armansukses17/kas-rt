@extends('master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h2>Input Warga Pendatang</h2>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>

    @if(Session::has('success_msg'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            {{Session::get('success_msg')}}
        </div>
    @endif

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <div class="card-header">
                    <h5 class="header-title">Data Keluarga</h5>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="formAddData" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <fieldset>
                                    <div class="form-group">
                                        <label>Nomor Kepala Keluarga (No. KK)</label>
                                        <input type="text" name="no_kk" id="no_kk" class="form-control" autocomplete="off">
                                        <input type="hidden" name="status_warga" value="<?php echo base64_encode('Pendatang'); ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="required">Upload KK</label>
                                        <input type="file" name="file_kk" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Anggota Keluarga</label>
                                     </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card card-primary card-outline card-outline-tabs">
                                                <div class="card-header p-0 border-bottom-0">
                                                    <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                                        <li class="nav-item">
                                                            <a class="nav-link active" id="column-suami-tab" data-toggle="pill" href="#column-suami" role="tab" aria-controls="column-suami" aria-selected="true">SUAMI</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" id="column-istri-tab" data-toggle="pill" href="#column-istri" role="tab" aria-controls="column-istri" aria-selected="false">ISTRI</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" id="column-anak-tab" data-toggle="pill" href="#column-anak" role="tab" aria-controls=column-anak" aria-selected="false">ANAK</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="card-body">
                                                    <div class="tab-content" id="custom-tabs-three-tabContent">
                                                        <div class="tab-pane fade active show" id="column-suami" role="tabpanel" aria-labelledby="column-suami-tab">
                                                            <div class="form-group">
                                                                <label>Nama Lengkap</label>
                                                                <input type="text" name="nama_suami" id="nama_suami" class="form-control" autocomplete="off">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>NIK</label>
                                                                <input type="text" name="nik_suami" id="nik_suami" class="form-control" autocomplete="off">
                                                            </div>
                                                            <!-- <div class="form-group">
                                                                <label>Status Hubungan Keluarga</label>
                                                                <select class="form-control" name="status_hub_kel_suami" id="status_hub_kel_suami">
                                                                    <option value="">-- Pilih --</option>
                                                                    <option value="Kepala Keluarga">Kepala Keluarga</option>
                                                                    <option value="Istri">Istri</option>
                                                                    <option value="Anak">Anak</option>
                                                                </select>
                                                            </div> -->
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label>Tempat Lahir</label>
                                                                        <input type="text" name="tempat_lahir_suami" id="tempat_lahir_suami" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label>Tanggal Lahir</label>
                                                                        <input type="text" name="tanggal_lahir_suami" id="tanggal_lahir_suami" class="form-control datepicker-bts" autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group">
                                                                <label for="role">Jenis Kelamin</label>
                                                                <br>
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="opt1_suami" name="jenis_kelamin_suami" class="custom-control-input" value="L">
                                                                    <label class="custom-control-label" for="opt1_suami">Laki - Laki</label>
                                                                </div>
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="opt2_suami" name="jenis_kelamin_suami" class="custom-control-input" value="P">
                                                                    <label class="custom-control-label" for="opt2_suami">Perempuan</label>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group">
                                                                <label>Agama</label>
                                                                <select class="form-control" name="agama_suami" id="agama_suami">
                                                                    <option value="">-- Pilih --</option>
                                                                    <option value="islam">Islam</option>
                                                                    <option value="kristen">Kristen</option>
                                                                    <option value="hindu">Hindu</option>
                                                                    <option value="budha">Budha</option>
                                                                    <option value="katolik">Katolik</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>No. telp</label>
                                                                <input type="text" name="no_telp_suami" id="no_telp_suami" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Alamat</label>
                                                                <textarea name="alamat_suami" id="alamat_suami" class="form-control input-sm" rows="4"></textarea>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Pekerjaan</label>
                                                                <select class="form-control" name="pekerjaan_suami" id="pekerjaan_suami">
                                                                    <option value="">-- Pilih --</option>
                                                                    <option value="PNS">PNS</option>
                                                                    <option value="TNI">TNI</option>
                                                                    <option value="Polri">Polri</option>
                                                                    <option value="Guru">Guru</option>
                                                                    <option value="Wiraswasta">Wiraswasta</option>
                                                                    <option value="Karyawan Swasta">Karyawan Swasta</option>
                                                                    <option value="Buruh">Buruh</option>
                                                                    <option value="Petani">Petani</option>
                                                                    <option value="Nelayan">Nelayan</option>
                                                                    <option value="Lainnya">Lainnya</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="column-istri" role="tabpanel" aria-labelledby="column-istri-tab">
                                                            <div class="form-group">
                                                                <label>Nama Lengkap</label>
                                                                <input type="text" name="nama_istri" id="nama_istri" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>NIK</label>
                                                                <input type="text" name="nik_istri" id="nik_istri" class="form-control">
                                                            </div>
                                                            <!-- <div class="form-group">
                                                                <label>Status Hubungan Keluarga</label>
                                                                <select class="form-control" name="status_hub_kel_istri" id="status_hub_kel_istri">
                                                                    <option value="">-- Pilih --</option>
                                                                    <option value="Kepala Keluarga">Kepala Keluarga</option>
                                                                    <option value="Istri">Istri</option>
                                                                    <option value="Anak">Anak</option>
                                                                </select>
                                                            </div> -->
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label>Tempat Lahir</label>
                                                                        <input type="text" name="tempat_lahir_istri" id="tempat_lahir_istri" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label>Tanggal Lahir</label>
                                                                        <input type="text" name="tanggal_lahir_istri" id="tanggal_lahir_istri" class="form-control datepicker-bts" autocomplete="off">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group">
                                                                <label for="role">Jenis Kelamin</label>
                                                                <br>
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="opt1_istri" name="jenis_kelamin_istri" class="custom-control-input" value="L">
                                                                    <label class="custom-control-label" for="opt1_istri">Laki - Laki</label>
                                                                </div>
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="opt2_istri" name="jenis_kelamin_istri" class="custom-control-input" value="P">
                                                                    <label class="custom-control-label" for="opt2_istri">Perempuan</label>
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group">
                                                                <label>Agama</label>
                                                                <select class="form-control" name="agama_istri" id="agama_istri">
                                                                    <option value="">-- Pilih --</option>
                                                                    <option value="islam">Islam</option>
                                                                    <option value="kristen">Kristen</option>
                                                                    <option value="hindu">Hindu</option>
                                                                    <option value="budha">Budha</option>
                                                                    <option value="katolik">Katolik</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>No. telp</label>
                                                                <input type="text" name="no_telp_istri" id="no_telp_istri" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Alamat</label>
                                                                <textarea name="alamat_istri" id="alamat_istri" class="form-control input-sm" rows="4"></textarea>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Pekerjaan</label>
                                                                <select class="form-control" name="pekerjaan_istri" id="pekerjaan_istri">
                                                                    <option value="">-- Pilih --</option>
                                                                    <option value="PNS">PNS</option>
                                                                    <option value="TNI">TNI</option>
                                                                    <option value="Polri">Polri</option>
                                                                    <option value="Guru">Guru</option>
                                                                    <option value="Wiraswasta">Wiraswasta</option>
                                                                    <option value="Karyawan Swasta">Karyawan Swasta</option>
                                                                    <option value="Buruh">Buruh</option>
                                                                    <option value="Petani">Petani</option>
                                                                    <option value="Nelayan">Nelayan</option>
                                                                    <option value="Lainnya">Lainnya</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="column-anak" role="tabpanel" aria-labelledby="column-anak-tab">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <spa style="font-size: 14px;"n>Nama Anak (Urutkan dari yang tertua)</span>
                                                                </div>
                                                                <div class="col-md-6 text-right">
                                                                    <button type="button" class="btn btn-sm btn-success btn-add" onclick="addItem()" title="Tambah">
                                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <table class="table table-bordered table-hover" id="tableAddData">
                                                                <thead style="line-height: 30px;">
                                                                    <tr>
                                                                        <th style="width: 140px;">Nik</th>
                                                                        <th style="width: 210px;">Nama Anak</th>
                                                                        <th style="width: 140px;">Tempat Lahir</th>
                                                                        <th style="width: 90px;">Tanggal Lahir</th>
                                                                        <th style="width: 90px;">Jenis Kelamin</th>
                                                                        <th style="width: 90px;">Agama</th>
                                                                        <th style="width: 20px;">#</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="bodyListData">
                                                                    <tr id="row-1">
                                                                        <td>
                                                                            <input type="text" name="nik_anak[]" id="nik_anak-1" class="form-control" autocomplete="off">
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" name="nama_anak[]" id="nama_anak-1" class="form-control" autocomplete="off">
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" name="tempat_lahir_anak[]" id="tempat_lahir_anak-1" class="form-control" autocomplete="off">
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" name="tanggal_lahir_anak[]" id="tanggal_lahir_anak-1" class="form-control datepicker-bts-modal" autocomplete="off">
                                                                        </td>
                                                                        <td>
                                                                            <select class="chosen-select form-control" name="jenis_kelamin_anak[]" id="jenis_kelamin_anak-1">
                                                                                <option value="">-- Pilih --</option>
                                                                                <option value="L">Laki - laki</option>
                                                                                <option value="P">Perempuan</option>
                                                                            </select>
                                                                        </td>
                                                                        <td>
                                                                            <select class="chosen-select form-control" name="agama_anak[]" id="agama_anak-1">
                                                                                <option value="">-- Pilih --</option>
                                                                                <option value="islam">Islam</option>
                                                                                <option value="kristen">Kristen</option>
                                                                                <option value="hindu">Hindu</option>
                                                                                <option value="budha">Budha</option>
                                                                                <option value="katolik">Katolik</option>
                                                                            </select>
                                                                        </td>
                                                                        <td>
                                                                            <div class="hidden-sm hidden-md btn-group"> 
                                                                                <button type="button" class="btn btn-md btn-danger btn-remove delete-record" title="Remove" data-id="remove-1">
                                                                                    <i class="fa fa-times" aria-hidden="true" style="line-height: 23px;"></i>
                                                                                </button>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.card -->
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="form-group text-right">
                                        <a href="{{ '/dataWargaPendatang' }}"><button type="button" class="btn btn-danger mt-4 pl-4 pr-4">Batal</button></a>
                                        <button type="submit" class="btn btn-primary mt-4 pl-4 pr-4">Simpan</button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
            </div>
            <!-- /.card -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<style>
#tableAddData #bodyListData tr td {
    padding: 0 !important;
}
table tbody td .form-control,
thead tr th {
    font-size: 10px !important;
}
</style>

<script>
var count = 0;
$(document).on('ready', function() {
    // $('#tblData').DataTable().ajax.reload();
});

function addItem() {
	var no = count + 2;
	count++;
    // alert(no)
    var html = `
        <tr id="row-${no}">
            <td>
                <input type="text" name="nik_anak[]" id="nik_anak-${no}" class="form-control" autocomplete="off">
            </td>
            <td>
                <input type="text" name="nama_anak[]" id="nama_anak-${no}" class="form-control" autocomplete="off">
            </td>
            <td>
                <input type="text" name="tempat_lahir_anak[]" id="tempat_lahir_anak-${no}" class="form-control" autocomplete="off">
            </td>
            <td>
                <input type="text" name="tanggal_lahir_anak[]" id="tanggal_lahir_anak-${no}" class="form-control datepicker-bts-modal" autocomplete="off">
            </td>
            <td>
                <select class="chosen-select form-control" name="jenis_kelamin_anak[]" id="jenis_kelamin_anak-${no}">
                    <option value="">-- Pilih --</option>
                    <option value="L">Laki - laki</option>
                    <option value="P">Perempuan</option>
                </select>
            </td>
            <td>
                <select class="chosen-select form-control" name="agama_anak[]" id="agama_anak-${no}">
                    <option value="">-- Pilih --</option>
                    <option value="islam">Islam</option>
                    <option value="kristen">Kristen</option>
                    <option value="hindu">Hindu</option>
                    <option value="budha">Budha</option>
                    <option value="katolik">Katolik</option>
                </select>
            </td>
            <td>
                <div class="hidden-sm hidden-md btn-group"> 
                    <button type="button" class="btn btn-md btn-danger btn-remove delete-record" title="Remove" data-id="remove-${no}">
                        <i class="fa fa-times" aria-hidden="true" style="line-height: 23px;"></i>
                    </button>
                </div>
            </td>
        </tr>
    `;
	$("#bodyListData").append(html);
}

$(document).on( 'click', '.delete-record', function(e) {
	e.preventDefault();    
	var key = $(this).attr('data-id');
    var getID = key.replace("remove-", "");
    $('#row-'+ getID).remove();
    return true;
});

$('#formAddData').on('submit', function(e) {
    e.preventDefault();
    var formData = new FormData(this);

    $.ajax({
        type: "POST",
        url : "{{ '/saveWargaPendatang' }}",
        data: formData,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
            if (data.s == 'success') {
                setTimeout(function() { 
                    window.location.href = "{{ '/dataWargaPendatang' }}";
                }, 1000);
                toastr.success(data.m);
                $('#formAddData')[0].reset(); // reset form
            } 
            else {
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">- '+value+'</span><br>';
                });
                toastr.error(messages);
            }
        },
        error: function(data) {
            toastr.error('Error !!!');
        }
    }); 
});
</script>
@endsection
