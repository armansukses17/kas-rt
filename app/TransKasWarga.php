<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransKasWarga extends Model
{
    protected $table = 'transaksi_kas_warga';
    protected $fillable = [
        'nama_warga',
        'tahun',
        'januari',
        'februari',
        'maret',
        'april',
        'mei',
        'juni',
        'juli',
        'agustus',
        'september',
        'oktober',
        'november',
        'desember'
    ];
}
