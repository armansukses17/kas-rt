<footer class="main-footer">
    <strong>Copyright @ {{ date('Y') }} .</strong> All rights reserved.
    <div class="float-right d-none d-sm-inline-block"><b>Version</b>1.0</div>
</footer>
<aside class="control-sidebar control-sidebar-dark"></aside>

<script>
    $.widget.bridge("uibutton", $.ui.button);
</script>
<script src="{{ '/plugins/bootstrap/js/bootstrap.bundle.min.js' }}"></script>

<script type="text/javascript" src="{{ '/js/plugins/jquery.dataTables.min.js' }}"></script>
<script type="text/javascript" src="{{ '/js/plugins/dataTables.bootstrap.min.js' }}"></script>
<!-- <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"> </script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"> </script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"> </script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"> </script> -->

<script src="{{ '/js/bootstrap-datepicker.min.js' }}"></script>
<script src="{{ '/plugins/moment/moment.min.js' }}"></script>
<script src="{{ '/plugins/select2/js/select2.full.min.js' }}"></script>
<!-- <script src="{{ '/plugins/inputmask/jquery.inputmask.min.js' }}"></script> -->
<script src="{{ '/js/jquery.mask.js' }}"></script>
<script src="{{ '/plugins/sweetalert2/sweetalert2.min.js' }}"></script>
<script src="{{ '/plugins/toastr/toastr.min.js' }}"></script>
<script src="{{ '/plugins/daterangepicker/daterangepicker.js' }}"></script>
<script src="{{ '/dist/js/adminlte.js' }}"></script>
<script src="{{ '/js/customs.js' }}"></script>

<script>
// Only Number
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
// format uang
function formatCurrency(num) {
    var	number_string = num.toString(),
        sisa 	= number_string.length % 3,
        rupiah 	= number_string.substr(0, sisa),
        ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
    // ---------------------------------
    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }
    return rupiah;
}
</script>