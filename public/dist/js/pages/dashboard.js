/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

/* global moment:false, Chart:false, Sparkline:false */

$(function () {
    'use strict'

    // Make the dashboard widgets sortable Using jquery UI
    $('.connectedSortable').sortable({
        placeholder: 'sort-highlight',
        connectWith: '.connectedSortable',
        handle: '.card-header, .nav-tabs',
        forcePlaceholderSize: true,
        zIndex: 999999
    })
    $('.connectedSortable .card-header').css('cursor', 'move')

    // jQuery UI sortable for the todo list
    $('.todo-list').sortable({
        placeholder: 'sort-highlight',
        handle: '.handle',
        forcePlaceholderSize: true,
        zIndex: 999999
    })

  // bootstrap WYSIHTML5 - text editor
  // $('.textarea').summernote()

    $('.daterange').daterangepicker({
        ranges: {
            Today: [moment(), moment()],
            Yesterday: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate: moment()
        }, function (start, end) {
        // eslint-disable-next-line no-alert
        alert('You chose: ' + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    })
  
    // ALERT
    var Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
    });

    $(".swalDefaultSuccess").click(function () {
        Toast.fire({
            icon: "success",
            title: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
        });
    });
    $(".swalDefaultInfo").click(function () {
        Toast.fire({
            icon: "info",
            title: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
        });
    });
    $(".swalDefaultError").click(function () {
        Toast.fire({
            icon: "error",
            title: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
        });
    });
    $(".swalDefaultWarning").click(function () {
        Toast.fire({
            icon: "warning",
            title: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
        });
    });
    $(".swalDefaultQuestion").click(function () {
        Toast.fire({
            icon: "question",
            title: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
        });
    });

    $(".toastrDefaultSuccess").click(function () {
        toastr.success("Lorem ipsum dolor sit amet, consetetur sadipscing elitr.");
    });
    $(".toastrDefaultInfo").click(function () {
        toastr.info("Lorem ipsum dolor sit amet, consetetur sadipscing elitr.");
    });
    $(".toastrDefaultError").click(function () {
        toastr.error("Lorem ipsum dolor sit amet, consetetur sadipscing elitr.");
    });
    $(".toastrDefaultWarning").click(function () {
        toastr.warning("Lorem ipsum dolor sit amet, consetetur sadipscing elitr.");
    });

    $(".toastsDefaultDefault").click(function () {
        $(document).Toasts("create", {
            title: "Toast Title",
            body: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
        });
    });
    $(".toastsDefaultTopLeft").click(function () {
        $(document).Toasts("create", {
            title: "Toast Title",
            position: "topLeft",
            body: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
        });
    });
    $(".toastsDefaultBottomRight").click(function () {
        $(document).Toasts("create", {
            title: "Toast Title",
            position: "bottomRight",
            body: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
        });
    });
    $(".toastsDefaultBottomLeft").click(function () {
        $(document).Toasts("create", {
            title: "Toast Title",
            position: "bottomLeft",
            body: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
        });
    });
    $(".toastsDefaultAutohide").click(function () {
        $(document).Toasts("create", {
            title: "Toast Title",
            autohide: true,
            delay: 750,
            body: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
        });
    });
    $(".toastsDefaultNotFixed").click(function () {
        $(document).Toasts("create", {
            title: "Toast Title",
            fixed: false,
            body: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
        });
    });
    $(".toastsDefaultFull").click(function () {
        $(document).Toasts("create", {
            body: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
            title: "Toast Title",
            subtitle: "Subtitle",
            icon: "fas fa-envelope fa-lg",
        });
    });
    $(".toastsDefaultFullImage").click(function () {
        $(document).Toasts("create", {
            body: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
            title: "Toast Title",
            subtitle: "Subtitle",
            image: "../../dist/img/user3-128x128.jpg",
            imageAlt: "User Picture",
        });
    });
    $(".toastsDefaultSuccess").click(function () {
        $(document).Toasts("create", {
            class: "bg-success",
            title: "Toast Title",
            subtitle: "Subtitle",
            body: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
        });
    });
    $(".toastsDefaultInfo").click(function () {
        $(document).Toasts("create", {
            class: "bg-info",
            title: "Toast Title",
            subtitle: "Subtitle",
            body: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
        });
    });
    $(".toastsDefaultWarning").click(function () {
        $(document).Toasts("create", {
            class: "bg-warning",
            title: "Toast Title",
            subtitle: "Subtitle",
            body: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
        });
    });
    $(".toastsDefaultDanger").click(function () {
        $(document).Toasts("create", {
            class: "bg-danger",
            title: "Toast Title",
            subtitle: "Subtitle",
            body: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
        });
    });
    $(".toastsDefaultMaroon").click(function () {
        $(document).Toasts("create", {
            class: "bg-maroon",
            title: "Toast Title",
            subtitle: "Subtitle",
            body: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
        });
    });

    //  zzz
    //Initialize Select2 Elements
    // $(".select2").select2();

    //Initialize Select2 Elements
    // $(".select2bs4").select2({
    //     theme: "bootstrap4",
    // });

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", { placeholder: "dd/mm/yyyy" });
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", { placeholder: "mm/dd/yyyy" });
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $("#reservation").daterangepicker();
    //Date range picker with time picker
    $("#reservationtime").daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: "MM/DD/YYYY hh:mm A",
        },
    });

    // Bootstrap Date picker
    $(".datepicker-bts").datepicker({
        format: 'dd-mm-yyyy',
        orientation: "bottom auto",
        autoclose: true
    });
})
