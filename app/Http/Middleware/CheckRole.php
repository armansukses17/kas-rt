<?php
  
namespace App\Http\Middleware;
  
use Closure;
use Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) // , ... $roles
    {
        // if (Auth::user() && Auth::user()->level == 1) {
        //      return $next($request);
        // }
        $roles = array_slice(func_get_args(), 2); // nama2 levelnya
        // dd($roles);
        foreach ($roles as $role) 
        { 
            $user = Auth::user()->level;
            if ($user == $role) 
            {
                return $next($request);
            }
        }
        return redirect('login')->with('error', "You don't have access.");
    }
}
