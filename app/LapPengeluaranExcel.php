<?php

namespace App;

use App\MasterPemasukan;
// use App\MasterPengeluaran;
use App\TransPengeluaran;
use DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class LapPengeluaranExcel implements FromView, ShouldAutoSize, WithEvents
{
    public function view(): View
    {
        $filterType = isset($_GET['filterType2']) ? $_GET['filterType2'] : '';
        $filterBy = isset($_GET['filterBy2']) ? $_GET['filterBy2'] : '';
        $yearWithMonth = isset($_GET['yearWithMonth2']) ? $_GET['yearWithMonth2'] : '';
        $format = '';
         
        if (empty($filterBy)) 
        {
            $value = 'notFound!';
        } 
        else 
        {
            if ($filterType == 1) 
            {
                $format = "m%Y";
                $value = "$filterBy$yearWithMonth";
                $infoReportBy = strtoupper(monthIndo($filterBy));
                $headerInfo = "DATA LAPORAN PENGELUARAN KAS PER BULAN $infoReportBy $yearWithMonth";
            }
            elseif ($filterType == 2) 
            {
                $format = 'Y';
                $value = $filterBy;
                $headerInfo = "DATA LAPORAN PENGELUARAN KAS PER TAHUN $value";
            }
        }

        $data = DB::table('transaksi_pengeluaran as a')
            ->select('b.nama_pemasukan', DB::raw("(DATE_FORMAT(a.tanggal_pengeluaran,'%d/%m/%Y')) as tanggal_pengeluaran"), 'a.nominal', 'a.keterangan')
            ->leftJoin('master_pemasukan as b', 'b.id', '=', 'a.kategori_pengeluaran_id')
            ->where(DB::raw("(DATE_FORMAT(a.tanggal_pengeluaran, '%$format'))"), '=', $value)
            ->orderBy(DB::raw("(DATE_FORMAT(a.tanggal_pengeluaran,'%m'))"))
            ->get();

        return view('laporan.lapPengeluaranExcel', [
            'data' => $data,
            'headerInfo' => $headerInfo
        ]);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) 
            {
                $cellHeaderTitle = 'A1:E1'; // header title
                $styleArrayTitle = [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ];
                $event->sheet->getStyle($cellHeaderTitle)->applyFromArray($styleArrayTitle);
                // -----------------------------------------------------------------------------
                // -----------------------------------------------------------------------------
                $cellHeaderTable = 'A3:E3'; // header tbl
                $styleHeaderTable = [
                    'font' => [
                        'bold' => TRUE,
                        'size' => 13,
                        'color' => [ 'rgb' => 'ffffff' ]
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['rgb' => '000000'],
                        ],
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => '008000',
                        ]
                    ],
                ];
                // $event->sheet->getDelegate()->getStyle($cellHeaderTable)->getFont()->setSize(13);
                $event->sheet->getStyle($cellHeaderTable)->applyFromArray($styleHeaderTable);
            }
        ];
    }
}
