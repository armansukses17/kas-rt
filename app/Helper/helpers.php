<?php 

if (! function_exists('monthIndo')) 
{
    function monthIndo($getMonth)
    {
        if ($getMonth == '01') { $month = 'Januari'; }
        elseif ($getMonth == '02') { $month = 'Februari'; }
        elseif ($getMonth == '03') { $month = 'Maret'; }
        elseif ($getMonth == '04') { $month = 'April'; }
        elseif ($getMonth == '05') { $month = 'Mei'; }
        elseif ($getMonth == '06') { $month = 'Juni'; }
        elseif ($getMonth == '07') { $month = 'Juli'; }
        elseif ($getMonth == '08') { $month = 'Agustus'; }
        elseif ($getMonth == '09') { $month = 'September'; }
        elseif ($getMonth == '10') { $month = 'Oktober'; }
        elseif ($getMonth == '11') { $month = 'November'; }
        elseif ($getMonth == '12') { $month = 'Desember'; }
        return $month;
    }
}
