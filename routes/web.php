<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::group(['middleware'=> 'auth'], function() {
    Route::get('', 'HomeController@index')->name('home');
    Route::get('dashboard', 'HomeController@index')->name('home');
    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

    // ######################################################################
    // ROLE 1 & 2
    // ######################################################################
    Route::group(['middleware' => ['role:1, 2']], function() 
    {
        // Master pemasukan
        Route::get('masterPemasukan', 'Pemasukan@masterPemasukan');
        Route::get('dtMasterPemasukan', 'Pemasukan@datatablesMasterPemasukan');
        Route::post('saveMasterPemasukan', 'Pemasukan@storeMasterPemasukan');
        Route::get('masterPemasukan/edit/{id}', 'Pemasukan@editMasterPemasukan');
        Route::put('updateMasterPemasukan', 'Pemasukan@updateMasterPemasukan');

        // Master Pengeluaran
        Route::get('masterPengeluaran', 'Pengeluaran@masterPengeluaran');
        Route::get('dtMasterPengeluaran', 'Pengeluaran@datatablesMasterPengeluaran');
        Route::post('saveMasterPengeluaran', 'Pengeluaran@storeMasterPengeluaran');
        Route::get('masterPengeluaran/edit/{id}', 'Pengeluaran@editMasterPengeluaran');
        Route::put('updateMasterPengeluaran', 'Pengeluaran@updateMasterPengeluaran');

        // Transaksi pemasukan
        Route::get('transaksiPemasukan', 'Pemasukan@transaksiPemasukan');
        Route::get('dtTransPemasukan', 'Pemasukan@datatablesTransPemasukan');
        Route::post('saveTransPemasukanUmum', 'Pemasukan@storeTransPemasukanUmum');
        Route::post('saveTransPemasukanBulanan', 'Pemasukan@storeTransPemasukanBulanan');
        Route::get('masterPemasukan/edit/{id}', 'Pemasukan@editMasterPemasukan');
        Route::put('updateMasterPemasukan', 'Pemasukan@updateMasterPemasukan');
        Route::post('searchWarga', 'Pemasukan@searchWarga');
        Route::get('getKasWarga/{param}', 'Pemasukan@getKasWarga');
        Route::put('updateKasWarga', 'Pemasukan@updateKasWarga');
        Route::post('bulananRtDibulanBaru', 'Pemasukan@insertOtomatisKasBuanan');

        // Transaksi pengeluaran
        Route::get('transaksiPengeluaran', 'Pengeluaran@transaksiPengeluaran');
        Route::get('dtTransPengeluaran', 'Pengeluaran@datatablesTransPengeluaran');
        Route::post('saveTransPengeluaran', 'Pengeluaran@storeTransPengeluaran');
        Route::get('masterPengeluaran/edit/{id}', 'Pengeluaran@editMasterPengeluaran');
        Route::put('updateMasterPengeluaran', 'Pengeluaran@updateMasterPengeluaran');

        // Laporan
        Route::get('laporan', 'Laporan@index');
        Route::get('dtLaporan', 'Laporan@datatablesLaporan');
        Route::get('dtLapTransPemasukan', 'Laporan@datatablesLapTransPemasukan');
        Route::get('dtLapTransPengeluaran', 'Laporan@datatablesLapTransPengeluaran');
        Route::get('dtLapTransMasukKeluarKas', 'Laporan@datatablesLapTransMasukKeluarKas');
        Route::get('lapPemasukanPdf', 'Laporan@printPdfPemasukan');
        Route::get('lapPemasukan', 'Laporan@printExcelsPemasukan');
        Route::get('lapPengeluaran', 'Laporan@printExcelsPengeluaran');
        Route::get('lapMasukKeluarKas', 'Laporan@printExcelsMasukKeluar');
        
        /// Warga
        Route::get('dataWarga', 'WargaController@index');
        Route::get('dataWargaPendatang', 'WargaController@indexPendatang');
        Route::get('warga/create', 'WargaController@createWarga');
        Route::get('warga/createPendatang', 'WargaController@createWargaPendatang');
        Route::get('datatablesWarga', 'WargaController@datatablesWarga');
        Route::get('datatablesWargaPendatang', 'WargaController@datatablesWargaPendatang');
        Route::post('saveWarga', 'WargaController@store');
        Route::post('saveWargaPendatang', 'WargaController@storePendatang');
        Route::get('warga/edit/{id}', 'WargaController@edit');
        Route::put('updateWarga', 'WargaController@update');
        Route::put('updateWargaPendatang', 'WargaController@updatePendatang');
        Route::post('deleteWarga','WargaController@destroy');
        Route::put('updateAnak', 'WargaController@updateAnak');
        Route::post('addDataAnak', 'WargaController@storeDataAnak');
        Route::get('anak/delete/{id}/{id2}', 'WargaController@destroyAnak');
        Route::get('warga/deleteKK/{id}/{id2}', 'WargaController@destroyKK');

        // User
        Route::get('userAccess', 'UserController@index');
        Route::get('datatablesUser', 'UserController@datatablesUser');
        Route::get('user/edit/{id}', 'UserController@edit');
        Route::put('updatePassword', 'UserController@updatePassword');
    });
});
