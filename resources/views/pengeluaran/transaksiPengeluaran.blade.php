@extends('master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h2>Kas Keluar</h2>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- <div class="card-header">
                            <div class="hidden-sm hidden-xs btn-group">
                                <a href="#modalAddData" id="btnAddTransPengeluaran" role="button" class="btn btn-danger btn-sm btn-add" data-toggle="modal"> Tambah Data </a>
                            </div>
                        </div> -->
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="card-body table-responsive p-0">
                                <div class="tile">
                                    <form class="row">
                                        <div class="form-group col-md-6">
                                            <div class="row">
                                                <div class="form-group col-md-12" id="outerFilter">
                                                    <div class="input-group">
                                                        <div class="form-outline">
                                                            <select class="chosen-select form-control" name="filterDataKasUmum" id="filterDataKasUmum" style="width: 150px;">
                                                                <?php 
                                                                $year = date('Y');
                                                                for ($i=2021; $i <= $year; $i++)
                                                                { 
                                                                    echo '<option value="'.$i.'">'.$i.'</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <!-- <button type="button" class="btn btn-primary btn-sm"> Filter </button> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6"> 
                                            <a href="#modalAddData" id="btnAddTransPengeluaran" role="button" class="btn btn-danger btn-md btn-add" data-toggle="modal" style="float: right;"> Tambah Data </a>
                                        </div>
                                    </form>
                                </div>
                                <table class="table table-hover table-bordered" id="tblData">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px;">No</th>
                                            <th style="width: 120px;">Jenis Pengeluaran</th>
                                            <th>Tanggal</th>
                                            <th>Nominal</th>
                                            <th style="width: 200px;">Keterangan</th>
                                            <!-- <th style="width: 50px;">Aksi</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modalAddData" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-modal="true">
    <div class="modal-dialog modalLarge">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Kas Keluar</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="msgAlert"></div>
            <div id="addTransPengeluaran">
                <div class="modal-body">
                    <div class="info-add">
                        <span style="color: #f00;">*Info </span>Tanggal pengeluaran tidak boleh melewati tanggal saat ini.
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form id="addTransactionOut"> 
                                {{ csrf_field() }}
                                <table class="table table-bordered table-hover" id="tableAddData">
                                    <thead style="line-height: 30px;">
                                        <tr>
                                            <th style="width: 230px;">Jenis pengeluaran</th>
                                            <th style="width: 125px;">Tanggal Pengeluaran</th>
                                            <th style="width: 125px;">Nominal</th>
                                            <th style="width: 210px;">Keterangan</th>
                                            <th width="5%">#</th>
                                        </tr>
                                    </thead>
                                    <tbody id="bodyListData">
                                        <tr id="row-1">
                                            <td>
                                                <select class="chosen-select form-control" name="jenis_pengeluaran[]" id="jenis_pengeluaran-1">
                                                    <option value="">-- Jenis Pengeluaran --</option>
                                                    @foreach ($masterPengeluaran as $row)
                                                        <option value="{{ $row->id }}">{{ $row->nama_pemasukan }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" name="tanggal_pengeluaran[]" id="tanggal_pengeluaran-1" class="form-control datepicker-bts-modal2" autocomplete="off">
                                            </td>
                                            <td>
                                               <input type="text" name="nominal[]" id="nominal-1" class="form-control money" autocomplete="off">
                                            </td>
                                            <td>
                                                <textarea class="form-control" rows="2" name="keterangan[]"  id="keterangan-1" placeholder="Keterangan ..."></textarea>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-md btn-group"> 
                                                    <button type="button" class="btn btn-md btn-danger btn-remove delete-record" title="Remove" data-id="remove-1">
                                                        <i class="fa fa-times" aria-hidden="true" style="line-height: 23px;"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="modal-footer" style="padding-right: 0;">
                                    <button type="button" class="btn btn-md btn-warning btn-add pull-right" onclick="addItem()" title="Tambah">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Tambah
                                    </button>
                                    <input type="submit" class="btn btn-primary" value="Simpan">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer footer-close">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Keluar</button>
                </div> -->
            </div>
        </div>
    </div>
</div><!-- End: modal -->

<style>
.info-add {
    font-size: 12px;
}
.modal .table th, 
.modal .table td,
.modal .table td input,
.modal .table td textarea,
.modal .table td select {
    font-size: 10px;
} 

.modal .table tbody td {
    padding: 2px;
}

/* DISABLED NEXT MONTH */
.datepicker table tr td.disabled, 
.datepicker table tr td.disabled:hover {
    background: #e8e6f5;
    color: #a5a5a5;
    cursor: no-drop;
}
/* ------------ */
</style>

<script>
// Bootstrap Date picker
$("body").delegate(".datepicker-bts-modal2", "focusin", function () {
    $(this).datepicker({
        format: 'dd-mm-yyyy',
        orientation: "bottom auto",
        autoclose: true,
        endDate: '+0m'
    });
});

var count = 0;

$('#filterDataKasUmum').on('change', function() {
    var currentYear = new Date().getFullYear();
    if (this.value < currentYear) {
        $('#btnAddTransPengeluaran').hide();
    } else {
        $('#btnAddTransPengeluaran').show();
    }
    callDataPengeluaran();
});

function listTablePengeluaran(filterBy='') {
    $('#tblData').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ '/dtTransPengeluaran' }}",
            data: {
                filterBy: filterBy
            },
            type: "GET"
        },
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false }, // no autoincrement
            { data: 'jenisPengeluaran', name: 'jenisPengeluaran' },
            { data: 'tanggalBulanan', name: 'tanggalBulanan' },
            { data: 'formatNominal', name: 'formatNominal' },
            { data: 'keterangan', name: 'keterangan' },
            // { data: 'action', name: 'action' }
        ],
        // "order": [[ '1', "desc" ]]
    });
}

$(function() {
    let yearsNow = '<?php echo date('Y'); ?>';
    $('#filterDataKasUmum').val(yearsNow);

    callDataPengeluaran();
});

function callDataPengeluaran() {
    $("#tblData").dataTable().fnDestroy();
    let filterByYear = $('#filterDataKasUmum').val();
    listTablePengeluaran(filterByYear);
}

$('#addTransactionOut').on('submit', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        type: "POST",
        url : "{{ '/saveTransPengeluaran' }}",
        data: formData,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
            if (data.s == 'success') {
                toastr.success(data.m);
                $('#tblData').DataTable().ajax.reload();
                $('.modal').modal('hide');
                $('#addTransactionOut')[0].reset(); // reset form
            } 
            else {  
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">'+value+'</span>';
                });
                toastr.error(messages);
            }
        },
        error: function(data) {
            toastr.error('Error !!!');
        }
    }); 
});

$('#btnAddTransPengeluaran').on('click', function() {
    $('#addTransactionOut')[0].reset(); // reset form
});

function addItem() {
	var no = count + 2;
	count++;
    // alert(no)
    var html = `
        <tr id="row-${no}">
            <td>
                <select class="chosen-select form-control" name="jenis_pengeluaran[]" id="jenis_pengeluaran-${no}">
                    <option value="">-- Jenis Pengeluaran --</option>
                    @foreach ($masterPengeluaran as $row)
                        <option value="{{ $row->id }}">{{ $row->nama_pemasukan }}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <input type="text" name="tanggal_pengeluaran[]" id="tanggal_pengeluaran-${no}" class="form-control datepicker-bts-modal2" autocomplete="off">
            </td>
            <td>
                <input type="text" name="nominal[]" id="nominal-${no}" class="form-control money" autocomplete="off">
            </td>
            <td>
                <textarea class="form-control" rows="2" name="keterangan[]" id="keterangan-${no}" placeholder="Keterangan ..."></textarea>
            </td>
            <td>
                <div class="hidden-sm hidden-md btn-group"> 
                    <button type="button" class="btn btn-md btn-danger btn-remove delete-record" title="Remove" data-id="remove-${no}">
                        <i class="fa fa-times" aria-hidden="true" style="line-height: 23px;"></i>
                    </button>
                </div>
            </td>
        </tr>
    `;
	$("#bodyListData").append(html);
}

$(document).on( 'click', '.delete-record', function(e) {
	e.preventDefault();    
	var key = $(this).attr('data-id');
    var getID = key.replace("remove-", "");
    $('#row-'+ getID).remove();
    return true;
});
</script>
@endsection
