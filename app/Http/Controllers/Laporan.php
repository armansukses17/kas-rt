<?php

namespace App\Http\Controllers;

use App\Warga;
use App\MasterPemasukan;
use App\TransPemasukan;
use App\MasterPengeluaran;
use App\TransPengeluaran;
use App\LapPemasukanExcel;
use App\LapPengeluaranExcel;
use App\LapMasukKeluarKasExcel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Excel;
use PDF;

class Laporan extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('laporan.index');
    }

    public function datatablesLapTransPemasukan()
    {
        $filterType = isset($_GET['filterType']) ? $_GET['filterType'] : '';
        $filterBy = isset($_GET['filterBy']) ? $_GET['filterBy'] : '';
        $yearWithMonth = isset($_GET['yearWithMonth']) ? $_GET['yearWithMonth'] : '';
        $format = '';

        if (empty($filterBy)) {
            $value = 'notFound!';
        } else {
            if ($filterType == 1) {
                $format = "m%Y";
                $value = "$filterBy$yearWithMonth";
            }
            elseif ($filterType == 2) {
                $format = 'Y';
                $value = $filterBy;
            }
        }

        $data = DB::table('transaksi_pemasukan as a')
            ->select('a.*', 'b.nama_pemasukan')
            ->leftJoin('master_pemasukan as b', 'b.id', '=', 'a.kategori_pemasukan_id')
            ->where(DB::raw("(DATE_FORMAT(a.tanggal_pemasukan, '%$format'))"), '=', $value)
            ->orderBy(DB::raw("DATE_FORMAT(a.tanggal_pemasukan,'%m')"), 'DESC')
            ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('formatNominal', function ($a) {
                $nominal = number_format($a->nominal, 0, ".", ".");
                return $nominal;
            })
            ->addColumn('tanggalBulanan', function ($a) {
                if ($a->keterangan == 'Kas bulanan rt') {
                    $getMonth = substr($a->tanggal_pemasukan, 5, 2);
                    $month = monthIndo($getMonth);
                    $tgl = '<span class="right badge badge-success">'. $month .' '. \Carbon\Carbon::parse($a->tanggal_pemasukan)->format('Y').'</span>';
                } else {
                    $getMonth = substr($a->tanggal_pemasukan, 5, 2);
                    $month = monthIndo($getMonth);
                    $day = \Carbon\Carbon::parse($a->tanggal_pemasukan)->format('d');
                    $year = \Carbon\Carbon::parse($a->tanggal_pemasukan)->format('Y');
                    $tgl = $day .' '. $month .' '. $year;
                }
               return $tgl;
            })
            ->addColumn('keteranganBulanan', function ($a) {
                if ($a->keterangan == 'Kas bulanan rt') {
                    $getMonth = substr($a->tanggal_pemasukan, 5, 2);
                    $month = monthIndo($getMonth);
                    $thisYear = \Carbon\Carbon::parse($a->tanggal_pemasukan)->format('Y');
                    $ket = "Kas bulanan RT dibulan $month $thisYear";
                } else {
                    $ket = $a->keterangan;
                }
               return $ket;
            })
            ->rawColumns(['formatNominal', 'tanggalBulanan', 'keteranganBulanan'])
            ->make(true);
    }

    public function datatablesLapTransPengeluaran()
    {
        $filterType = isset($_GET['filterType2']) ? $_GET['filterType2'] : '';
        $filterBy = isset($_GET['filterBy2']) ? $_GET['filterBy2'] : '';
        $yearWithMonth = isset($_GET['yearWithMonth2']) ? $_GET['yearWithMonth2'] : '';
        $format = '';

        if (empty($filterBy)) {
            $value = 'notFound!';
        } else {
            if ($filterType == 1) {
                $format = "m%Y";
                $value = "$filterBy$yearWithMonth";
            }
            elseif ($filterType == 2) {
                $format = 'Y';
                $value = $filterBy;
            }
        }

        $data = DB::table('transaksi_pengeluaran as a')
            ->select('a.*', 'b.nama_pemasukan')
            ->leftJoin('master_pemasukan as b', 'b.id', '=', 'a.kategori_pengeluaran_id')
            ->where(DB::raw("(DATE_FORMAT(a.tanggal_pengeluaran, '%$format'))"), '=', $value)
            ->orderBy(DB::raw("DATE_FORMAT(a.tanggal_pengeluaran,'%m')"), 'DESC')
            ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('formatNominal', function ($a) {
                $nominal = number_format($a->nominal, 0, ".", ".");
                return $nominal;
            })
            ->addColumn('tanggal_pengeluaran', function ($a) {
                $getMonth = substr($a->tanggal_pengeluaran, 5, 2);
                $month = monthIndo($getMonth);
                $day = \Carbon\Carbon::parse($a->tanggal_pengeluaran)->format('d');
                $year = \Carbon\Carbon::parse($a->tanggal_pengeluaran)->format('Y');
                $tgl = $day .' '. $month .' '. $year;
                return $tgl;
            })
            ->rawColumns(['formatNominal', 'tanggal_pengeluaran'])
            ->make(true);
    }

    public function datatablesLapTransMasukKeluarKas()
    {
        $filterType = isset($_GET['filterType3']) ? $_GET['filterType3'] : '';
        $filterBy = isset($_GET['filterBy3']) ? $_GET['filterBy3'] : '';
        $yearWithMonth = isset($_GET['yearWithMonth3']) ? $_GET['yearWithMonth3'] : '';
        $format = '';
        $format2 = '';
         
        if (empty($filterBy)) 
        {
            $value = 'notFound!';
            $year = '';
            $format = '';
            $format2 = '';
            $where = '';
        } 
        else 
        {
            if ($filterType == 2) 
            {
                $format = '%Y/%m';
                $format2 = '%M %Y';
                $where = '%Y';
                $value = $filterBy;
            }
        }

        $data = DB::table('transaksi_pemasukan as a')
            ->select(
                DB::raw("(DATE_FORMAT(a.tanggal_pemasukan, '$format2')) as tanggal"),
                DB::raw("SUM(a.nominal) as kas_masuk"),  
                DB::raw("(SELECT sum(b.nominal) FROM transaksi_pengeluaran b 
                            WHERE DATE_FORMAT(b.tanggal_pengeluaran, '$format') = DATE_FORMAT(a.tanggal_pemasukan, '$format') 
                            GROUP BY ( DATE_FORMAT( b.tanggal_pengeluaran, '%Y/%m' ) ) LIMIT 1) as `kas_keluar`")
            )
            ->where(DB::raw("(DATE_FORMAT(a.tanggal_pemasukan, '$where'))"), '=', $value)
            ->groupBy(DB::raw("(DATE_FORMAT(a.tanggal_pemasukan,'$format'))"))
            ->get();

        $return = ['s' => 'success', 'm' => $data];
        return response()->json($return, 200);
    }

    public function printPdfPemasukan() 
    {
        $data = TransPemasukan::all();
    	$pdf = PDF::loadview('laporan.laporanPemasukanPdf.', ['data' => $data]);
    	return $pdf->download('laporan-pemasukan-pdf.pdf');
    }

    public function printExcelsPemasukan()
    {
        $dateNow = date('YmdHis');
        return Excel::download(new LapPemasukanExcel, "LaporanPemasukan$dateNow.xlsx");
    }

    public function printExcelsPengeluaran()
    {
        $dateNow = date('YmdHis');
        return Excel::download(new LapPengeluaranExcel, "LaporanPengeluaran$dateNow.xlsx");
    }

    public function printExcelsMasukKeluar()
    {
        $dateNow = date('YmdHis');
        return Excel::download(new LapMasukKeluarKasExcel, "LaporanMasukKeluarKasExcel$dateNow.xlsx");
    }
}
