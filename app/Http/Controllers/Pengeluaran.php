<?php

namespace App\Http\Controllers;

use App\Warga;
use App\MasterPemasukan;
// use App\MasterPengeluaran;
use App\TransPengeluaran;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class Pengeluaran extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {        
    }

    public function masterPengeluaran_OFF()
    {
        return view('pengeluaran.masterPengeluaran');
    }

    public function transaksiPengeluaran()
    {
        $data = DB::table('master_pemasukan')->where('nama_pemasukan', '!=', 'Kas bulanan RT')->orderBy('id', 'desc')->get();
        return view('pengeluaran.transaksiPengeluaran',  ['masterPengeluaran' => $data]);
    }

    public function datatablesMasterPengeluaran_OFF()
    {
        $data = DB::table('master_pengeluaran')->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $btn = '
                    <div class="hidden-sm hidden-xs btn-group">
                        <button class="btn btn-xs btn-info btn-edit" onclick="editMasterPengeluaran('.$row->id.')" title="Edit Data">
                            <i class="nav-icon fas fa-edit"></i>
                        </button>
                    </div>
                ';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function datatablesTransPengeluaran(Request $request)
    {
        $year = $request->filterBy;
        $data = DB::table('transaksi_pengeluaran as a')
            ->select('a.*', 'b.nama_pemasukan as jenisPengeluaran')
            ->leftJoin('master_pemasukan as b', 'b.id', '=', 'a.kategori_pengeluaran_id')
            ->where('b.nama_pemasukan', '!=', 'Kas bulanan RT')
            ->where(DB::raw("(DATE_FORMAT(a.tanggal_pengeluaran, '%Y'))"), '=', $year)
            ->orderBy(DB::raw("DATE_FORMAT(a.tanggal_pengeluaran,'%m')"), 'DESC')
            ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('formatNominal', function ($a) {
                $nominal = number_format($a->nominal, 0, ".", ".");
                return $nominal;
            })
            ->addColumn('tanggalBulanan', function ($a) {
                $getMonth = substr($a->tanggal_pengeluaran, 5, 2);
                $month = monthIndo($getMonth);
                $day = \Carbon\Carbon::parse($a->tanggal_pengeluaran)->format('d');
                $year = \Carbon\Carbon::parse($a->tanggal_pengeluaran)->format('Y');
                $tgl =  $day .' '. $month .' '. $year;
                return $tgl;
            })
            ->rawColumns(['formatNominal', 'tanggalBulanan'])
            ->make(true);
    }

    public function storeMasterPengeluaran_OFF(Request $request)
    {
        if ($request->ajax()) 
        {
            $rules = array(
                'judul_pengeluaran' => 'required'
            );
            $message = array(
                'judul_pengeluaran.required' => 'Judul Pengeluaran wajib diisi !',
            );
            $validator = Validator::make($request->all(), $rules, $message);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            }
            
            $data = new MasterPengeluaran();
            $data->nama_pengeluaran = $request->judul_pengeluaran;
            $data->keterangan = isset($request->keterangan) ? $request->keterangan : '';
            $data->save();
            $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
        }
        return response()->json($return, 200);
    }

    public function storeTransPengeluaran(Request $request)
    {
        if ($request->ajax()) 
        {
            // ----------------------------------
            DB::beginTransaction();
            // ----------------------------------
            try {
                $rules = array(
                    'jenis_pengeluaran.*' => 'required',
                    'tanggal_pengeluaran.*' => 'required',
                    'nominal.*' => 'required'
                );
                $message = array(
                    'jenis_pengeluaran.*.required' => 'Jenis pengeluaran wajib diisi !',
                    'tanggal_pengeluaran.*.required' => 'Tanggal wajib diisi !',
                    'nominal.*.required' => 'Jumlah / nominal wajib diisi !'
                );
                $validator = Validator::make($request->all(), $rules, $message);
                if ($validator->fails()) {
                    return response()->json(['errors' => $validator->errors()->all()]);
                }
                // ----------------------------------------------

                for ($i = 0; $i < count($request->jenis_pengeluaran); $i++) 
                {
                    $nominal = str_replace('.', '', $request->nominal[$i]);

                    $dataArray[] = [
                        'kategori_pengeluaran_id' => $request->jenis_pengeluaran[$i],
                        'tanggal_pengeluaran' => \Carbon\Carbon::parse($request->tanggal_pengeluaran[$i])->format('Y-m-d'),
                        'nominal' => $nominal,
                        'keterangan' => isset($request->keterangan[$i]) ? $request->keterangan[$i] : '',
                        'created_at' => \Carbon\Carbon::now()->toDateTimeString(), // untuk mengisi field ini ketika insert array
                        'updated_at' => \Carbon\Carbon::now()->toDateTimeString() // untuk mengisi field ini ketika insert array
                    ];
                }
                TransPengeluaran::insert($dataArray);
                $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !', 'data' => $dataArray];
                //  ----------------------------------------------
                DB::commit();
                // ----------------------------------------------
            } catch (\Throwable $e) {
                // ----------------------------------------------
                DB::rollback();
                // ----------------------------------------------
                throw $e;
                $return = ['error' => $e];
            } 
            return response()->json($return);
        }
    }

    public function editMasterPengeluaran_OFF($id)
    {
        $getData = MasterPengeluaran::where('id', $id)->get();
        $return = ['s' => 'success', 'data' => $getData];
        return response()->json($return, 200);
    }

    public function updateMasterPengeluaran_OFF(Request $request)
    {
        if ($request->ajax()) 
        {
            $rules = array(
                'edit_judul_pengeluaran' => 'required'
            );
            $message = array(
                'edit_judul_pengeluaran.required' => 'Judul Pengeluaran wajib diisi !',
            );
            $validator = Validator::make($request->all(), $rules, $message);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            }

            MasterPengeluaran::where('id', $request->id)->update([
                'nama_pengeluaran' => $request->edit_judul_pengeluaran,
                'keterangan' => isset($request->edit_keterangan) ? $request->edit_keterangan : ''
            ]);
            $return = ['s' => 'success', 'm' => 'Data berhasil diperbarui !'];
        }
        return response()->json($return, 200);
    }
}
