<?php

namespace App\Http\Controllers;

use App\Warga;
use App\MasterPemasukan;
use App\TransPemasukan;
use App\TransKasWarga;
use App\TotKas;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class Pemasukan extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {      
        echo 'Something when wrong !';  
    }

    public function masterPemasukan()
    {
        return view('pemasukan.masterPemasukan');
    }

    public function checkSuamiIstri($suami, $istri)
    {
        if (!empty($suami) AND !empty($istri))
        {
            return $suami .'/'. $istri;
        }
        elseif (!empty($suami))
        {
            return $suami;
        }
        else
        {
            return $istri;
        }
    }

    public function getKasWarga($paramYears)
    {
        if ($paramYears == date('Y'))
        {
            $getCount = TransKasWarga::select('id', 'tahun')
                ->where('tahun', '=', date('Y'))
                ->get();
            $check = count($getCount);
            // print_r($check); die;
            if ($check == 0) // pembukaan kas ditahun baru
            {
                // ----------------------------------
                DB::beginTransaction();
                // ----------------------------------
                try {

                    $warga = Warga::all();

                    foreach ($warga as $row)
                    {
                        $nmWarga = $this->checkSuamiIstri($row->nama_suami, $row->nama_istri);

                        $insertArray[] = [
                            'id_warga' => $row->id,
                            'nama_warga' => $nmWarga,
                            'tahun' => date('Y'),
                            'januari' => 0,
                            'februari' => 0,
                            'maret' => 0,
                            'april' => 0,
                            'mei' => 0,
                            'juni' => 0,
                            'juli' => 0,
                            'agustus' => 0,
                            'september' => 0,
                            'oktober' => 0,
                            'november' => 0,
                            'januari' => 0,
                            'desember' => 0,
                            'created_at' => \Carbon\Carbon::now()->toDateTimeString(), // untuk mengisi field ini ketika insert array
                            'updated_at' => \Carbon\Carbon::now()->toDateTimeString() // untuk mengisi field ini ketika insert array
                        ];
                    } 
                    TransKasWarga::insert($insertArray);
                    $data = DB::table('transaksi_kas_warga')->where('tahun', date('Y'))->orderBy('nama_warga', 'asc')->get();
                    // -------------------------------------------------------------------------------------------------------
                    $return = ['s' => 'success', 'm' => 'insert', 'data' => $data, 'year' => 'thisYear'];
                    //  ----------------------------------------------
                    DB::commit();
                    // ----------------------------------------------
                } catch (\Throwable $e) {
                    // ----------------------------------------------
                    DB::rollback();
                    // ----------------------------------------------
                    throw $e;
                    $return = ['error' => $e];
                } 
            }
            else 
            {
                // ----------------------------------
                DB::beginTransaction();
                // ----------------------------------
                try {
                    // Ambil array transaksi kas warga saat ini
                    $dataKas = TransKasWarga::select('id_warga')
                        ->where('tahun', date('Y'))
                        ->orderBy('id_warga', 'asc')
                        ->get();
                    foreach ($dataKas as $rowKas) 
                    {
                        $dataKasWarga[] = $rowKas->id_warga;
                    }
                    // ------------------------------------------------------------------------------------
                    // Ambil data array all warga
                    $getAllWarga = DB::table('warga')->select('id')->orderBy('id', 'asc')->get();
                    foreach ($getAllWarga as $rowWarga) 
                    {
                        $dataWarga[] = $rowWarga->id;
                    }
                    // ------------------------------------------------------------------------------------
                    // filter 2 array tsb, AMBIL DATA YANG BELUM ADA di tabel transaksi kas
                    $result = array_diff($dataWarga, $dataKasWarga);
                    foreach($result as $val) // di loop data warga yg baru untuk disimpan
                    {
                        $getWarga = DB::table('warga')->where('id', $val)->orderBy('id', 'asc')->get();
                        $countToInsert = count($getWarga);
                        if ($countToInsert > 0)
                        {
                            foreach($getWarga as $x)
                            {
                                $nmWarga = $this->checkSuamiIstri($x->nama_suami, $x->nama_istri);

                                $insertNewWarga = [
                                    'id_warga' => $x->id,
                                    'nama_warga' => $nmWarga,
                                    'tahun' => date('Y')
                                ];
                                TransKasWarga::insert($insertNewWarga);
                            }
                        }
                    }
                    // -------------------------------------------------------------------------------------------------------
                    $dataWarga = DB::table('warga')->select('*')->get();
                    // -------------------------------------------------------------------------------------------------------
                    foreach ($dataWarga as $z)
                    {
                        $nmWarga = $this->checkSuamiIstri($z->nama_suami, $z->nama_istri);
                        // selalu update nama warga saat tab kas bulanan diklik !
                        TransKasWarga::where('id_warga', $z->id)->update([
                            'nama_warga' => $nmWarga
                        ]);
                    }
                    // -------------------------------------------------------------------------------------------------------
                    $data = DB::table('transaksi_kas_warga')->where('tahun', date('Y'))->orderBy('nama_warga', 'asc')->get();
                    // -------------------------------------------------------------------------------------------------------
                    $this->insertOtomatisKasBuanan($paramYears);
                    $return = ['s' => 'success', 'm' => 'updatess', 'data' => $data, 'year' => 'thisYear'];
                    //  ----------------------------------------------
                    DB::commit();
                    // ----------------------------------------------
                } catch (\Throwable $e) {
                    // ----------------------------------------------
                    DB::rollback();
                    // ----------------------------------------------
                    throw $e;
                    $return = ['error' => $e];
                } 
            }
        }
        else 
        { 
            // Ambil data kas tahun lalu
            $data = DB::table('transaksi_kas_warga')->where('tahun', $paramYears)->orderBy('nama_warga', 'asc')->get();
            $return = ['s' => 'success', 'm' => 'updatess', 'data' => $data, 'year' => 'lastYear'];
        }
        return response()->json($return);
    }

    public function insertOtomatisKasBuananDiSetNol($paramYears)
    {
        $thisMonth = date('Y-m');
        $thisYear = date('Y');
        $arr = [
            "$thisYear-01", 
            "$thisYear-02", 
            "$thisYear-03", 
            "$thisYear-04", 
            "$thisYear-05", 
            "$thisYear-06", 
            "$thisYear-07",
            "$thisYear-08",
            "$thisYear-09",
            "$thisYear-10",
            "$thisYear-11",
            "$thisYear-12"
        ];

        if ($paramYears == date('Y'))
        {
            $dataKas = DB::table('transaksi_pemasukan')
                ->whereIn(DB::raw("(DATE_FORMAT(tanggal_pemasukan,'%Y-%m'))"), $arr)
                ->get();

            foreach ($dataKas as $row) 
            {
                $dataKas = DB::table('transaksi_pemasukan')
                    ->whereIn(DB::raw("(DATE_FORMAT(tanggal_pemasukan,'%Y-%m'))"), substr($row->tanggal_pemasukan, 0, 7))
                    ->get();
                if (count($dataKas) == 0)
                {
                    $dataArray = [
                        'kategori_pemasukan_id' => 1, // khusus kas bulanan rt
                        'tanggal_pemasukan' => date('Y-m-d'), // new month
                        'nominal' => 0,
                        'keterangan' => 'Kas bulanan rt'
                    ];
                    // -------------------------------
                    TransPemasukan::create($dataArray);
                }
            }
        }
        else
        {
            $return = ['m' => 'Tidak valid !'];
        }
        return response()->json($return);
    }

    public function insertOtomatisKasBuanan($paramYears)
    {
        $thisMonth = date('Ym');
        if ($paramYears == date('Y'))
        {
            $dataKas = DB::table('transaksi_pemasukan')
                ->where('keterangan', '=', 'Kas bulanan rt')
                ->where(DB::raw("(DATE_FORMAT(tanggal_pemasukan,'%Y%m'))"), '=', $thisMonth)
                ->get();

            if (count($dataKas) == 0)
            {
                // insert in new month with nominal = 0
                $dataArray = [
                    'kategori_pemasukan_id' => 1, // khusus kas bulanan rt
                    'tanggal_pemasukan' => date('Y-m-d'), // new month
                    'nominal' => 0,
                    'keterangan' => 'Kas bulanan rt'
                ];
                // -------------------------------
                TransPemasukan::create($dataArray);
                $return = ['s' => 'success', 'm' => true];
            }
            else
            {
                $return = ['m' => 'Tidak ada data baru !'];
            }
        }
        else
        {
            $return = ['m' => 'Tidak valid !'];
        }
        return response()->json($return);
    }

    public function transaksiPemasukan()
    {
        $checkData = count(MasterPemasukan::all());
        if ($checkData == 0) { $masterPemasukan = 'NotReady'; } else { $masterPemasukan = 'ready'; }
        $data = DB::table('master_pemasukan')->where('nama_pemasukan', '!=', 'Kas bulanan RT')->orderBy('id', 'desc')->get();
        return view('pemasukan.transaksiPemasukan', ['masterPemasukan' => $data, 'checkMasterPemasukan' => $masterPemasukan]);
    }

    public function datatablesMasterPemasukan()
    {
        $data = DB::table('master_pemasukan')->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $btn = '
                    <div class="hidden-sm hidden-xs btn-group">
                        <button class="btn btn-xs btn-info btn-edit" onclick="editMasterPemasukan('.$row->id.')" title="Edit Data">
                            <i class="nav-icon fas fa-edit"></i>
                        </button>
                    </div>
                ';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function datatablesTransPemasukan(Request $request)
    {
        $year = $request->filterBy;
        $data = DB::table('transaksi_pemasukan as a')
            ->select('a.*', 'b.nama_pemasukan')
            ->leftJoin('master_pemasukan as b', 'b.id', '=', 'a.kategori_pemasukan_id')
            ->where('b.nama_pemasukan', '!=', 'Kas bulanan RT')
            ->where(DB::raw("(DATE_FORMAT(a.tanggal_pemasukan, '%Y'))"), '=', $year)
            ->orderBy(DB::raw("DATE_FORMAT(a.tanggal_pemasukan,'%m')"), 'DESC')
            ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('formatNominal', function ($a) {
                $nominal = number_format($a->nominal, 0, ".", ".");
                return $nominal;
            })
            ->addColumn('tanggalBulanan', function ($a) {
                $getMonth = substr($a->tanggal_pemasukan, 5, 2);
                $month = monthIndo($getMonth);
                $day = \Carbon\Carbon::parse($a->tanggal_pemasukan)->format('d');
                $year = \Carbon\Carbon::parse($a->tanggal_pemasukan)->format('Y');
                $tgl =  $day .' '. $month .' '. $year;
               return $tgl;
            })
            ->addColumn('keteranganBulanan', function ($a) {
                if ($a->keterangan == 'Kas bulanan rt') {
                    $getMonth = substr($a->tanggal_pemasukan, 5, 2);
                    $month = monthIndo($getMonth);
                    $thisYear = \Carbon\Carbon::parse($a->tanggal_pemasukan)->format('Y');
                    $tgl = "Kas bulanan RT dibulan $month $thisYear";
                } else {
                    $tgl = $a->keterangan;
                }
               return $tgl;
            })
            ->rawColumns(['formatNominal', 'tanggalBulanan', 'keteranganBulanan'])
            ->make(true);
    }

    public function storeMasterPemasukan(Request $request)
    {
        if ($request->ajax()) 
        {
            $rules = array(
                'judul_pemasukan' => 'required'
            );
            $message = array(
                'judul_pemasukan.required' => 'Judul pemasukan wajib diisi !',
            );
            $validator = Validator::make($request->all(), $rules, $message);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            }
            // -------------------------------------------------
            $getData = MasterPemasukan::all();
            $checkData = count($getData);

            if ($checkData == 0)
            {
                if ($request->judul_pemasukan != 'Kas bulanan RT')
                {
                    $return = ['errors' => ['Masukan inputan pertama dengan judul "Kas bulanan RT" !']];
                    return response()->json($return);
                    die;
                }
                else
                {
                    $data = new MasterPemasukan();
                    $data->nama_pemasukan = $request->judul_pemasukan;
                    $data->keterangan = isset($request->keterangan) ? $request->keterangan : '';
                    $data->save();
                    $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
                    return response()->json($return, 200);
                }
            }
            else 
            {
                // asumsi sudah ada data d db
                if ($request->judul_pemasukan == 'Kas bulanan RT')
                {
                    $return = ['errors' => ['Data ini sudah pernah diinput !']];
                    return response()->json($return);
                    die;
                }
                else 
                {
                    $data = new MasterPemasukan();
                    $data->nama_pemasukan = $request->judul_pemasukan;
                    $data->keterangan = isset($request->keterangan) ? $request->keterangan : '';
                    $data->save();
                    $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
                    return response()->json($return, 200);
                }
            }
        }
    }

    public function storeTransPemasukanUmum(Request $request)
    {
        if ($request->ajax()) 
        {
            // ----------------------------------
            DB::beginTransaction();
            // ----------------------------------
            try {
                $rules = array(
                    'jenis_pemasukan.*' => 'required',
                    'tanggal_pemasukan.*' => 'required',
                    'nominal.*' => 'required'
                );
                $message = array(
                    'jenis_pemasukan.*.required' => 'Jenis pemasukan wajib diisi !',
                    'tanggal_pemasukan.*.required' => 'Tanggal wajib diisi !',
                    'nominal.*.required' => 'Jumlah / nominal wajib diisi !'
                );
                $validator = Validator::make($request->all(), $rules, $message);
                if ($validator->fails()) {
                    return response()->json(['errors' => $validator->errors()->all()]);
                }
                // ----------------------------------------------

                for ($i = 0; $i < count($request->jenis_pemasukan); $i++) 
                {
                    $nominal = str_replace('.', '', $request->nominal[$i]);

                    $dataArray[] = [
                        'kategori_pemasukan_id' => $request->jenis_pemasukan[$i],
                        'tanggal_pemasukan' => \Carbon\Carbon::parse($request->tanggal_pemasukan[$i])->format('Y-m-d'),
                        'nominal' => $nominal,
                        'keterangan' => isset($request->keterangan[$i]) ? $request->keterangan[$i] : '',
                        'created_at' => \Carbon\Carbon::now()->toDateTimeString(), // untuk mengisi field ini ketika insert array
                        'updated_at' => \Carbon\Carbon::now()->toDateTimeString() // untuk mengisi field ini ketika insert array
                    ];
                }
                TransPemasukan::insert($dataArray);
                $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !', 'data' => $dataArray];
                //  ----------------------------------------------
                DB::commit();
                // ----------------------------------------------
            } catch (\Throwable $e) {
                // ----------------------------------------------
                DB::rollback();
                // ----------------------------------------------
                throw $e;
                $return = ['error' => $e];
            } 
            return response()->json($return);
        }
    }

    public function storeTransPemasukanBulanan(Request $request)
    {
        if ($request->ajax()) 
        { 
            if ($request->year == date('Y')) // edit/insert hanya bs dilakukan ditahun ini 
            {
                // ----------------------------------
                DB::beginTransaction();
                // ----------------------------------
                try {
                    $nominal = str_replace('.', '', $request->nominal);

                    $thisMonth = $this->getMonthNumber($request->month);
                    $theDate = date('Y').'-'.$thisMonth.'-01'; // 2021-05-01
                    $getDate = date("Y-m-t", strtotime($theDate)); // t -> last day in this month
                    
                    $dataKas =  DB::table('transaksi_pemasukan')
                        ->where('keterangan', '=', 'Kas bulanan rt')
                        ->where(DB::raw("(DATE_FORMAT(tanggal_pemasukan,'%Y%m'))"), '=', date('Y').$thisMonth)
                        ->orderBy('id', 'desc')
                        ->limit(1)
                        ->get();

                    if (count($dataKas) != 0) // jk tdk ada di bulan ini atau bulan saat mengisi kas warga
                    {
                        // update transaksi pemasukan dari kas warga
                        TransPemasukan::
                            where('keterangan', '=', 'Kas bulanan rt')
                            ->where(DB::raw("(DATE_FORMAT(tanggal_pemasukan,'%Y%m'))"), '=', date('Y').$thisMonth)
                            ->update([
                                'nominal' => $nominal
                            ]);
                        $return = ['s' => 'success', 'm' => 'Data berhasil diperbarui !'];
                    }
                    else
                    {
                        // Insert transaksi pemasukan berdasarkan inputan pemasukan general 
                        $rules = array(
                            'tanggal_pemasukan' => 'required',
                            'nominal' => 'required'
                        );
                        $message = array(
                            'tanggal_pemasukan.required' => 'Tanggal wajib diisi !',
                            'nominal.required' => 'Jumlah / nominal wajib diisi !'
                        );
                        $validator = Validator::make($request->all(), $rules, $message);
                        if ($validator->fails()) {
                            return response()->json(['errors' => $validator->errors()->all()]);
                        }
                        // ----------------------------------------------
                        // get master pemasukan
                        $getMasterPemasukan =  DB::table('master_pemasukan')->where('nama_pemasukan', '=', 'Kas bulanan RT')->limit(1)->get();
                        // insert
                        $dataArray = [
                            'kategori_pemasukan_id' => $getMasterPemasukan[0]->id,
                            'tanggal_pemasukan' => $getDate,
                            'nominal' => $nominal,
                            'keterangan' => isset($request->keterangan) ? $request->keterangan : '',
                        ];
                        // ----------------------------------------------
                        $save = TransPemasukan::create($dataArray);
                        $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
                    }
                    // ----------------------------------------------
                    DB::commit();
                    // ----------------------------------------------
                } catch (\Throwable $e) {
                    // ----------------------------------------------
                    DB::rollback();
                    // ----------------------------------------------
                    throw $e;
                    $return = ['error' => $e];
                } 
            }
            else
            {
                $return = ['error' => 'Tidak valid !!!'];
            }
            return response()->json($return);
        }
    }

    function getMonthName($param)
    {
        // juni***06
        $explode = explode("***", $param);
        return $explode[0]; // juni
    }

    function getMonthNumber($param)
    {
        // juni***06
        $explode = explode("***", $param);
        return $explode[1]; // 06
    }

    public function updateKasWarga(Request $request)
    {      
        if ($request->ajax()) 
        {
            if ($request->year == date('Y')) // edit/insert hanya bs dilakukan ditahun ini 
            {
                $fieldMonth = $this->getMonthName($request->title);
                $nominal = str_replace('.', '', $request->value);
                TransKasWarga::where('id', $request->id)->update([
                    $fieldMonth => $nominal
                ]);
                $msg = ['s' => 'success', 'm' => 'Data berhasil diperbarui !'];
            }
            else
            {
                $msg = ['s' => 'error', 'm' => 'Tidak valid !'];
            }
        }
        return response()->json($msg, 200);
    }

    public function editMasterPemasukan($id)
    {
        $getData = MasterPemasukan::where('id', $id)->get();
        $return = ['s' => 'success', 'data' => $getData];
        return response()->json($return, 200);
    }

    public function updateMasterPemasukan(Request $request)
    {
        if ($request->ajax()) 
        {
            $rules = array(
                'edit_judul_pemasukan' => 'required'
            );
            $message = array(
                'edit_judul_pemasukan.required' => 'Judul pemasukan wajib diisi !',
            );
            $validator = Validator::make($request->all(), $rules, $message);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            }

            $getData =  DB::table('master_pemasukan')->where('id', '=', $request->id)->limit(1)->get();
            if ($getData[0]->nama_pemasukan == 'Kas bulanan RT')
            {
                MasterPemasukan::where('id', $request->id)->update([
                    'keterangan' => isset($request->edit_keterangan) ? $request->edit_keterangan : ''
                ]);
            }
            else
            {
                MasterPemasukan::where('id', $request->id)->update([
                    // 'nama_pemasukan' => $request->edit_judul_pemasukan,
                    'keterangan' => isset($request->edit_keterangan) ? $request->edit_keterangan : ''
                ]);
            }
            $return = ['s' => 'success', 'm' => 'Data berhasil diperbarui !'];
        }
        return response()->json($return, 200);
    }
}
