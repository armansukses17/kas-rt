<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Si | KAS RT</title>
        <link rel="shortcut icon" href="{{ '/images/favicon.png' }}" />
        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback" />
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ '/plugins/fontawesome-free/css/all.min.css' }}" />
        <!-- icheck bootstrap -->
        <link rel="stylesheet" href="{{ '/plugins/icheck-bootstrap/icheck-bootstrap.min.css' }}" />
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ '/dist/css/adminlte.min.css' }}" />
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <!-- /.login-logo -->
            <div class="card card-outline card-primary">
                <div class="card-header text-center">
                    <a href="{{ '/index2.html' }}" class="h1"><b>SiKaRT</b></a>
                </div>
                <div class="card-body">
                    <!-- <p class="login-box-msg">Sign in to start your session</p> -->
                    @error('msg')
                        <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            {{ $message }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('login') }}" class="login-form">
                        @csrf
                        <div class="input-group mb-3">
                            <input class="form-control @error('email') is-invalid @enderror" type="text" name="email" value="{{ old('email') }}" placeholder="Email" autofocus />
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="input-group mb-3">
                            <input class="form-control @error('password') is-invalid @enderror" name="password" type="password" placeholder="Password" />
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="row">
                            <div class="col-8">
                                <!-- <div class="icheck-primary">
                                    <input type="checkbox" id="remember" />
                                    <label for="remember">
                                        Remember Me
                                    </label>
                                </div> -->
                            </div>
                            <!-- /.col -->
                            <div class="col-4">
                                <button class="btn btn-primary btn-block">SIGN IN</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                    <!-- /.social-auth-links -->
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.login-box -->

        <!-- jQuery -->
        <script src="{{ '/plugins/jquery/jquery.min.js' }}"></script>
        <!-- Bootstrap 4 -->
        <script src="{{ '/plugins/bootstrap/js/bootstrap.bundle.min.js' }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ '/dist/js/adminlte.min.js' }}"></script>
    </body>
</html>
