<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warga extends Model
{
    protected $table = 'warga';
    protected $fillable = [
        'no_kk',
        'file_kk',
        'storage',
        'status_warga',
        'status_hub_kel_suami',
        'nik_suami',
        'nama_suami',
        'tempat_lahir_suami',
        'tanggal_lahir_suami',
        'jenis_kelamin_suami',
        'agama_suami',
        'no_telp_suami',
        'alamat_suami',
        'pekerjaan_suami',
        'status_hub_kel_istri',
        'nik_istri',
        'nama_istri',
        'tempat_lahir_istri',
        'tanggal_lahir_istri',
        'jenis_kelamin_istri',
        'agama_istri',
        'no_telp_istri',
        'alamat_istri',
        'pekerjaan_istri'
    ];
}
