<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ './dashboard' }}" class="brand-link">
        <!-- <img
            src="/dist/img/AdminLTELogo.png"
            alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3"
            style="opacity: .8"
        /> -->
        <i class="fa fa-cubes animation__shake" style="margin-left: 15px; margin-right: 3px; font-size: 25px;"></i>
        <span class="brand-text font-weight-light" style="font-weight: bold !important;"> SiKasRT</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar" style="padding-top: 15px;">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul
                class="nav nav-pills nav-sidebar flex-column"
                data-widget="treeview"
                role="menu"
                data-accordion="false"
            >
                <li class="nav-header">HOME</li>
                <li class="nav-item <?php if (request()->segment(1) == 'dashboard') { echo 'active'; } ?>">
                    <a href="{{ '/dashboard' }}" class="nav-link">
                        <i class="fa fa-circle nav-icon"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-header">MASTER DATA</li>
                <li class="nav-item <?php if (request()->segment(1) == 'dataWarga' || request()->segment(1) == 'dataWargaPendatang' || request()->segment(1) == 'warga') { echo 'active'; } ?>">
                    <a href="{{ '/dataWarga' }}" class="nav-link">
                        <i class="nav-icon fa fa-users"></i>
                        <p>Data Warga</p>
                    </a>
                </li>
                <li class="nav-item <?php if (request()->segment(1) == 'masterPemasukan') { echo 'active'; } ?>">
                    <a href="{{ '/masterPemasukan' }}" class="nav-link">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>Jenis Pemasukan/Pengeluaran</p>
                    </a>
                </li>
                <!-- <li class="nav-item <?php // if (request()->segment(1) == 'masterPengeluaran') { echo 'active'; } ?>">
                    <a href="{{ '/masterPengeluaran' }}" class="nav-link">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>Jenis Pengeluaran</p>
                    </a>
                </li> -->
                <li class="nav-header">TRANSAKSI</li>
                <li class="nav-item <?php if (request()->segment(1) == 'transaksiPemasukan') { echo 'active'; } ?>">
                    <a href="{{ '/transaksiPemasukan' }}" class="nav-link">
                        <i class="nav-icon fa fa-download"></i>
                        <p>Pemasukan Kas</p>
                    </a>
                </li>
                <li class="nav-item <?php if (request()->segment(1) == 'transaksiPengeluaran') { echo 'active'; } ?>">
                    <a href="{{ '/transaksiPengeluaran' }}" class="nav-link">
                        <i class="nav-icon fa fa-upload"></i>
                        <p>Pengeluaran Kas</p>
                    </a>
                </li>
                <li class="nav-header">LAPORAN</li>
                <li class="nav-item <?php if (request()->segment(1) == 'laporan') { echo 'active'; } ?>">
                    <a href="{{ '/laporan' }}" class="nav-link">
                        <i class="nav-icon fa fa-file"></i>
                        <p>Laporan Kas</p>
                    </a>
                </li>
                <li class="nav-header">PENGATURAN</li>
                <li class="nav-item <?php if (request()->segment(1) == 'userAccess') { echo 'active'; } ?>">
                    <a href="{{ '/userAccess' }}" class="nav-link">
                        <i class="nav-icon fa fa-cog"></i>
                        <p>Akun</p>
                    </a>
                </li>
                <li class="nav-header">KELUAR APLIKASI</li>
                <li class="nav-item">
                    <a href="{{ '/logout' }}" class="nav-link">
                        <i class="nav-icon far fa-circle text-danger"></i>
                        <p>Logout</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
