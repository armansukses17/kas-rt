@extends('master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h2>Jenis Pengeluaran</h2>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="hidden-sm hidden-xs btn-group">
                                <a href="#modalAddData" role="button" class="btn btn-danger btn-sm btn-add" data-toggle="modal"> Tambah Data </a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover table-bordered" id="tblData">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px;">No</th>
                                            <th style="width: 120px;">Judul Pengeluaran</th>
                                            <th style="width: 200px;">Keterangan</th>
                                            <th style="width: 50px;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<div id="modalAddData" class="modal animated jackInTheBox" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Jenis Pengeluaran</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>

            <div class="msgAlert"></div>

            <form id="addMasterPengeluaran"> <!-- method="post" action="saveUnit" -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Judul Pengeluaran</label>
                                <input type="text" name="judul_pengeluaran" id="judul_pengeluaran" class="form-control" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea class="form-control" rows="4" name="keterangan" id="keterangan" placeholder="Optional ..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div><!-- End: modal -->


<div id="modalEditData" class="modal animated jackInTheBox" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>

            <div class="msgAlert"></div>

            <form id="formEditData">
                {{ method_field('PUT') }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Judul Pengeluaran</label>
                                <input type="hidden" name="id" id="keyEdit"> 
                                <input type="text" name="edit_judul_pengeluaran" id="edit_judul_pengeluaran" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea class="form-control" rows="4" name="edit_keterangan" id="edit_keterangan" placeholder="Deskripsi ..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div><!-- End: modal -->

<script>
$(document).on('ready', function() {
    // $('#tblData').DataTable().ajax.reload();
});
$(function() {
    $("#tblData").dataTable().fnDestroy();
    $('#tblData').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ '/dtMasterPengeluaran' }}",
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false }, // no autoincrement
            { data: 'nama_pengeluaran', name: 'nama_pengeluaran' },
            { data: 'keterangan', name: 'keterangan' },
            { data: 'action', name: 'action' }
        ],
        "order": [[ '1', "desc" ]]
    });
});

$('#addMasterPengeluaran').on('submit', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        type: "POST",
        url : "{{ '/saveMasterPengeluaran' }}",
        data: formData,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
            if (data.s == 'success') {
                toastr.success(data.m);
                $('#tblData').DataTable().ajax.reload();
                $('.modal').modal('hide');
                $('#addMasterPengeluaran')[0].reset(); // reset form
            } 
            else {
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">'+value+'</span>';
                });
                toastr.error(messages);
            }
        },
        error: function(data) {
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">'+value+'</span>';
                });
                toastr.error(messages);
        }
    }); 
});

function editMasterPengeluaran(param) {
    $.ajax({
        type: "GET",
        url : "{{ '/masterPengeluaran/edit/' }}"+param,
        dataType: "json",
        success: function(data) {
            if (data.s == 'success') {
                $('#modalEditData').modal();
                $('#keyEdit').val(param);
                $('#edit_judul_pengeluaran').val(data.data[0].nama_pengeluaran);
                $('#edit_keterangan').val(data.data[0].keterangan);
            } 
            else {
                alert('error');
            }
        }
    }); 
}

$('#formEditData').on('submit', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        type: "POST",
        url : "{{ '/updateMasterPengeluaran' }}",
        data: formData,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
            if (data.s == 'success') {
                toastr.success(data.m);
                $('#tblData').DataTable().ajax.reload();
                $('.modal').modal('hide');
            } 
            else {
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">'+value+'</span>';
                });
                toastr.error(messages);
            }
        }
    }); 
});
</script>
@endsection
