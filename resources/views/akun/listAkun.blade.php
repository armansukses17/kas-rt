@extends('master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h2>Data Akun</h2>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover table-bordered" id="tblDataUser">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px;">No</th>
                                            <th style="width: 120px;">Pengguna</th>
                                            <th style="width: 200px;">Email</th>
                                            <th style="width: 80px;">User Aktif</th>
                                            <th style="width: 50px;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div id="modalEditData" class="modal animated jackInTheBox" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>

            <div class="msgAlert"></div>

            <form id="formEditData">
                {{ method_field('PUT') }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Email</label>
                                <input type="hidden" name="keyEdit" id="keyEdit"> 
                                <input type="text" name="email" id="email" class="form-control" readonly>
                            </div>
                            <div class="form-group">
                                <label>Password Baru</label>
                                <input type="password" name="passwordNew" id="passwordNew" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div><!-- End: modal -->

<script>
$(document).on('ready', function() {
    // $('#tblDataUser').DataTable().ajax.reload();
});

$(function() {
    callDataTables();
});

function callDataTables() {
    $("#tblDataUser").dataTable().fnDestroy();
    listDataTables();
}

function listDataTables(filterBy='') {
    $('#tblDataUser').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ '/datatablesUser' }}",
            data: {
                // _token: '{{ csrf_token() }}',
                filterBy: filterBy
            },
            type: "GET"
        },
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false }, // no autoincrement
            { data: 'nama_lengkap', name: 'nama_lengkap' },
            { data: 'email', name: 'email' },
            { data: 'is_active', name: 'is_active' },
            { data: 'action', name: 'action' }
        ],
        // "order": [[ '1', "desc" ]]
    });
}

function changePassword(param) {
    $.ajax({
        type: "GET",
        url : "{{ '/user/edit/' }}"+param,
        dataType: "json",
        success: function(data) {
            if (data.s == 'success') {
                $('#modalEditData').modal();
                $('#keyEdit').val(param);
                $('#email').val(data.data[0].email);
            } 
            else {
                alert('error');
            }
        }
    }); 
}

$('#formEditData').on('submit', function(e) {
    e.preventDefault();
    if (confirm('Yakin akan mengubah password ?')) {
        var formData = new FormData(this);
        $.ajax({
            type: "POST",
            url : "{{ '/updatePassword' }}",
            data: formData,
            dataType: "json",
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                if (data.s == 'success') {
                    toastr.success(data.m);
                    callDataTables();
                    $('.modal').modal('hide');
                } 
                else {
                    let messages = '';
                    $.each(data.errors, function(key, value) {
                        messages += '<span class="alert-error">'+value+'</span>';
                    });
                    toastr.error(messages);
                }
            }
        })
    }
});
</script>
@endsection
