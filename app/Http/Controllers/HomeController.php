<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TransPemasukan;
use App\TransPengeluaran;
use DB;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $totalPemasukan = DB::table('transaksi_pemasukan')
            ->select(DB::raw('SUM(nominal) as totalPemasukan'))
            ->where(DB::raw("(DATE_FORMAT(tanggal_pemasukan,'%Y'))"), '=', date('Y'))
            ->get();

        $totalPengeluaran = DB::table('transaksi_pengeluaran')
            ->select(DB::raw('SUM(nominal) as totalPengeluaran'))
            ->where(DB::raw("(DATE_FORMAT(tanggal_pengeluaran,'%Y'))"), '=', date('Y'))
            ->get();
            
        $total_kk = count(DB::table('warga')->select('id')->where('status_warga', '=', 'Warga')->get());
        $total_kk_pendatang = count(DB::table('warga')->select('id')->where('status_warga', '=', 'Pendatang')->get());
        $getWarga = DB::table('warga')->select(DB::raw("SUM(jumlah_keluarga) as totWarga"))->where('status_warga', '=', 'Warga')->get();
        $totWargaPendatang = DB::table('warga')->select(DB::raw("SUM(jumlah_keluarga) as totWarga"))->where('status_warga', '=', 'Pendatang')->get();

        $data = [
            'totalPemasukan' => json_decode(json_encode($totalPemasukan[0]), true),
            'totalPengeluaran' => json_decode(json_encode($totalPengeluaran[0]), true),
            'total_kk' => $total_kk,
            'total_kk_pendatang' => $total_kk_pendatang,
            'totalWarga' => $getWarga[0]->totWarga,
            'totalWargaPendatang' => $totWargaPendatang[0]->totWarga
        ];

        $pieChart = $this->pieChart();
        $barChart = $this->highchart();
        // print_r($barChart); die;
        return view('dashboard', ['total' => $data, 'pieChart' => $pieChart, 'barChart' => $barChart]);
    }
    
    public function highchart()
    {
        $thisYear = date('Y');

        $inOut = DB::table('transaksi_pemasukan as a')
            ->select(
                DB::raw("(DATE_FORMAT(a.tanggal_pemasukan, '%Y-%m')) as tanggal"), 
                DB::raw("SUM(a.nominal) as kas_masuk"),  
                DB::raw("(SELECT sum(b.nominal) FROM transaksi_pengeluaran b WHERE DATE_FORMAT(b.tanggal_pengeluaran, '%Y-%m') = DATE_FORMAT(a.tanggal_pemasukan, '%Y-%m') LIMIT 1) as `kas_keluar`")
            )
            ->where(DB::raw("(DATE_FORMAT(a.tanggal_pemasukan, '%Y'))"), '=', $thisYear)
            ->groupBy(DB::raw("(DATE_FORMAT(a.tanggal_pemasukan,'%Y-%m'))"))
            ->get();
        
        if (count($inOut) == 0)
        {
            $chart = [
                'month' => '',
                'in' => '',
                'out' => ''
            ];
        }
        else
        {
            foreach ($inOut as $row) 
            {
                $month[] = date_format(date_create($row->tanggal), 'F');
                $in[] = $row->kas_masuk;
                $out[] = $row->kas_keluar;
            }

            $chart = [
                'month' => json_encode($month, JSON_NUMERIC_CHECK),
                'in' => json_encode($in, JSON_NUMERIC_CHECK),
                'out' => json_encode($out, JSON_NUMERIC_CHECK)
            ];
        }
        return $chart;
    }

    public function pieChart()
	{
        $in = TransPemasukan::select(DB::raw("SUM(nominal) as countIn"))
            ->where(DB::raw("(DATE_FORMAT(tanggal_pemasukan,'%Y'))"), '=', date('Y'))
            ->get();
        $countIn = isset($in[0]->countIn) ? $in[0]->countIn : 0;

        $out = TransPengeluaran::select(DB::raw("SUM(nominal) as countOut"))
            ->where(DB::raw("(DATE_FORMAT(tanggal_pengeluaran,'%Y'))"), '=', date('Y'))
            ->get();
        $countOut = isset($out[0]->countOut) ? $out[0]->countOut : 0;

		$stringJson = "
            [
                { name: 'Pengeluaran', y: $countOut },
                { name: 'Pemasukan', y: $countIn }
            ]
        ";
		return $stringJson;
	}
}
