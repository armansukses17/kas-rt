<html>
<head>
	
</head>
<body>
	<table>
		<tr><td></td></tr>
		<tr>
			<td colspan="5" style="font-weight: bold; font-size: 14px;">
				{{$headerInfo[0]}} 
			</td>
		</tr>
		<tr>
			<td colspan="5" style="font-weight: bold; font-size: 14px;">
				{{$headerInfo[1]}} 
			</td>
		</tr>
		<tr>
			<td colspan="5" style="font-weight: bold; font-size: 14px;">
				{{$headerInfo[2]}} 
			</td>
		</tr>
	</table>
	<table>
		<thead>
			<tr>
				<th>No</th>
				<th>Tahun/Bulan</th>
				<th>Nominal Pemasukan</th>
				<th>Nominal Pengeluaran</th>
				<th>Sisa Anggaran Kas</th>
			</tr>
		</thead>
		<tbody>
			@php $i = 1 @endphp
			@foreach($data as $p)
			<?php
			$total = '';
			// $sisa = $p->kas_masuk - $p->kas_keluar;
			// (E4 + C5) - C6

			if ($i == 1)
			{
				$sisa = "=SUM(C7-D7)";
			}
			else 
			{
				$f = $i + 5; // loop ke 2 + 5 = 7
				$c = $i + 6;
				$d = $i + 6;
				$sisa = "=SUM(E$f,C$c-D$d)";
			}
			$total .= $sisa;
			?>
			<tr>
				<td>{{$i}}</td>
				<td>{{$p->tanggal}}</td>
				<td>{{$p->kas_masuk != '' ? $p->kas_masuk : 0}}</td>
				<td>{{$p->kas_keluar != '' ? $p->kas_keluar : 0}}</td>
				<td>{{$sisa}}</td>
			</tr>
			{{$i++}}
			@endforeach
			<tr>
				<td colspan="4" style="background: #008000; color: #ffffff; font-size: 14px; font-weight: bold;">TOTAL : </td>
				<td style="background: #008000; color: #ffffff; font-size: 14px; font-weight: bold;">{{$total}}</td>
			</tr>
		</tbody>
	</table>

</body>
</html>