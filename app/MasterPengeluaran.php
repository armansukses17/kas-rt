<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterPengeluaran extends Model
{
    protected $table = 'master_pengeluaran';
    protected $fillable = [
        'nama_pengeluaran',
        'keterangan'
    ];
}
