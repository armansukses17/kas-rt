@extends('master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h2>Jenis Pemasukan</h2>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="hidden-sm hidden-xs btn-group">
                                <a href="#modalAddData" role="button" class="btn btn-danger btn-sm btn-add" data-toggle="modal"> Tambah Data </a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover table-bordered" id="tblData">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px;">No</th>
                                            <th style="width: 120px;">Jenis Pemasukan/Pengeluaran</th>
                                            <th style="width: 200px;">Keterangan</th>
                                            <th style="width: 50px;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div id="modalAddData" class="modal animated jackInTheBox" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Jenis Pemasukan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>

            <div class="msgAlert"></div>

            <form id="addMasterPemasukan"> <!-- method="post" action="saveUnit" -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="required">Jenis Pemasukan/Pengeluaran</label>
                                <input type="text" name="judul_pemasukan" id="judul_pemasukan" class="form-control" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea class="form-control" rows="4" name="keterangan" id="keterangan" placeholder="Optional ..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div><!-- End: modal -->


<div id="modalEditData" class="modal animated jackInTheBox" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>

            <div class="msgAlert"></div>

            <form id="formEditData">
                {{ method_field('PUT') }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="required">Jenis Pemasukan/Pengeluaran</label>
                                <input type="hidden" name="id" id="keyEdit"> 
                                <input type="text" name="edit_judul_pemasukan" id="edit_judul_pemasukan" class="form-control" readonly="readonly">
                            </div>
                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea class="form-control" rows="4" name="edit_keterangan" id="edit_keterangan" placeholder="Deskripsi ..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div><!-- End: modal -->

<script>
$(document).on('ready', function() {
    // $('#tblData').DataTable().ajax.reload();
});

$('.modal').on('hidden.bs.modal', function () {
    // $('#edit_judul_pemasukan').attr('readonly', false);
})

$(function() {
    $("#tblData").dataTable().fnDestroy();
    $('#tblData').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ '/dtMasterPemasukan' }}",
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false }, // no autoincrement
            { data: 'nama_pemasukan', name: 'nama_pemasukan' },
            { data: 'keterangan', name: 'keterangan' },
            { data: 'action', name: 'action' }
        ],
        "order": [[ '1', "desc" ]]
    });
});

$('#addMasterPemasukan').on('submit', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        type: "POST",
        url : "{{ '/saveMasterPemasukan' }}",
        data: formData,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
            if (data.s == 'success') {
                toastr.success(data.m);
                $('#tblData').DataTable().ajax.reload();
                $('.modal').modal('hide');
                $('#addMasterPemasukan')[0].reset(); // reset form
            } 
            else {
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">'+value+'</span>';
                });
                toastr.error(messages);
            }
        },
        error: function(data) {
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">'+value+'</span>';
                });
                toastr.error(messages);
        }
    }); 
});

function editMasterPemasukan(param) {
    $.ajax({
        type: "GET",
        url : "{{ '/masterPemasukan/edit/' }}"+param,
        dataType: "json",
        success: function(data) {
            if (data.s == 'success') {
                $('#modalEditData').modal();
                $('#keyEdit').val(param);
                $('#edit_judul_pemasukan').val(data.data[0].nama_pemasukan);
                $('#edit_keterangan').val(data.data[0].keterangan);
                if (data.data[0].nama_pemasukan == 'Kas bulanan RT') {
                    $('#edit_judul_pemasukan').prop('readonly', true);
                    $('#edit_judul_pemasukan').prop('title', 'Khusus untuk judul ini tidak bisa diubah !');
                }
            } 
            else {
                alert('error');
            }
        }
    }); 
}

$('#formEditData').on('submit', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        type: "POST",
        url : "{{ '/updateMasterPemasukan' }}",
        data: formData,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
            if (data.s == 'success') {
                toastr.success(data.m);
                $('#tblData').DataTable().ajax.reload();
                $('.modal').modal('hide');
            } 
            else {
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">'+value+'</span>';
                });
                toastr.error(messages);
            }
        }
    }); 
});
</script>
@endsection
