<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Sistem Kas RT</title>
        <link rel="shortcut icon" href="{{ '/images/favicon.png' }}" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback" />
        <!-- <link rel="stylesheet" href="{{ '/font/css/font-awesome.min.css' }}" /> -->
        <link rel="stylesheet" href="{{ '/plugins/fontawesome-free/css/all.min.css' }}" />
        <link rel="stylesheet" href="{{ '/plugins/daterangepicker/daterangepicker.css' }}" />
        <link rel="stylesheet" href="{{ '/css/bootstrap-datepicker.min.css' }}" />
        <link rel="stylesheet" href="{{ '/plugins/icheck-bootstrap/icheck-bootstrap.min.css' }}" />
        <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" /> -->
        <link rel="stylesheet" href="{{ '/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css' }}" />
        <link rel="stylesheet" href="{{ '/plugins/select2/css/select2.min.css' }}" />
        <link rel="stylesheet" href="{{ '/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css' }}" />
        <link rel="stylesheet" href="{{ '/css/dataTables.bootstrap4.min.css' }}" />
        <link rel="stylesheet" href="{{ '/css/buttons.bootstrap4.min.css' }}" />
        <link rel="stylesheet" href="{{ '/plugins/toastr/toastr.min.css' }}">
        <link rel="stylesheet" href="{{ '/plugins/sweetalert2/sweetalert2.min.css' }}">
        <link rel="stylesheet" href="{{ '/dist/css/adminlte.min.css' }}" />
        <link rel="stylesheet" type="text/css" href="{{ '/css/custom.css' }}" />

        <!-- js -->
        <!-- <script src="{{ '/plugins/jquery/jquery.min.js' }}"></script> -->
        <script src="{{ '/plugins/jquery/jquery.min.js' }}"></script>
        <script src="{{ '/plugins/jquery-ui/jquery-ui.min.js' }}"></script>
    </head>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">

            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <i class="fa fa-cubes animation__shake" style="font-size: 50px;"></i>
            </div>

            <!-- Navbar -->
            <nav
                class="
                    main-header
                    navbar navbar-expand navbar-white navbar-light
                "
            >
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a
                            class="nav-link"
                            data-widget="pushmenu"
                            href="#"
                            role="button"
                            style="color: #ffffff;"
                            ><i class="fas fa-bars"></i></a>
                    </li>
                </ul>

                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a
                            class="nav-link"
                            data-widget="fullscreen"
                            href="#"
                            role="button"
                        >
                            <i class="fas fa-expand-arrows-alt" style="color: #ffffff;"></i>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.navbar -->
        </div>
    </body>
</html>
