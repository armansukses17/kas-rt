@extends('master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-1">
                <!-- <div class="col-sm-6">
                    <h2>Dashboard</h2>
                </div> -->
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8 mt-3">
                                    <div id="barChart"></div>
                                </div>
                                <div class="col-md-4 mt-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="info-box">
                                                <span class=" info-box-icon bg-info elevation-1"><i class="fas fa-copy"></i></span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">TOTAL KK TETAP</span >
                                                    <span class="info-box-number">{{ $total['total_kk'] }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="info-box mb-3">
                                                <span class="info-box-icon bg-danger elevation-1" ><i class="fas fa-users"></i></span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">TOTAL WARGA TETAP</span>
                                                    <span class="info-box-number">{{ $total['totalWarga'] }}</span >
                                                </div>
                                            </div>
                                        </div>
                                        <div class=" col-md-12">
                                            <div class="info-box mb-3">
                                                <span class="info-box-icon bg-success elevation-1"><i class="fa fa-server" aria-hidden="true"></i></span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">TOTAL KK PENDATANG</span >
                                                    <span class="info-box-number">{{ $total['total_kk_pendatang'] }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=" col-md-12">
                                            <div class="info-box mb-3">
                                                <span class="info-box-icon bg-warning elevation-1" ><i class="fa fa-male"></i></span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">TOTAL PENDATANG</span>
                                                    <span class="info-box-number">{{ $total['totalWargaPendatang'] }}</span >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 mt-3">
                                    <div id="pieChart"></div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-6">
                                    <div class=" description-block border-right">
                                        <h5 class="description-header" id="totPemasukanBottom">
                                        </h5>
                                        <span class="description-text">TOTAL PEMASUKAN</span>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="description-block">
                                        <h5 class="description-header" id="totPengeluaranBottom">
                                        </h5>
                                        <span  class="description-text">TOTAL PENGELUARAN</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!--/. container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<style>
.info-box-text {
    font-size: 13px;
}
.info-box .info-box-number {
    margin: 0;
    font-size: 18px;
}
</style>

<script src="{{ '/js/highcharts.js' }}"></script>
<script type="text/javascript">

$(function () {
    var totPemasukan = "<?php echo $total['totalPemasukan']['totalPemasukan'] ? $total['totalPemasukan']['totalPemasukan'] : ''; ?>";
    var totPengeluaran = "<?php echo $total['totalPengeluaran']['totalPengeluaran'] ? $total['totalPengeluaran']['totalPengeluaran'] : 0; ?>";

    $('#totPemasukanBottom').text(formatCurrency(totPemasukan));
    $('#totPengeluaranBottom').text(formatCurrency(totPengeluaran));
    
    var dataPemasukan = <?php echo $barChart['in']; ?>;
    var dataPengeluaran = <?php echo $barChart['out']; ?>;
    var month = <?php echo $barChart['month']; ?>;
    // var bulan = <?php // echo $chart['month']; ?>;
    $('#barChart').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Pemasukan dan pengeluaran kas tahun {{ date('Y') }}'
        },
        xAxis: {
            categories: month
        },
        yAxis: {
            title: {
                text: 'Rate'
            }
        },
        series: [{
            "name": "Pemasukan",
            "data": dataPemasukan,
            "color": '#007bff'
        }, {
            "name": "Pengeluaran",
            "data":  dataPengeluaran,
            "color": '#dd1326'
        }]
    });

    // ------------------------------

    Highcharts.setOptions({
        colors: ['#10e1d7', '#0deb1e']
    });

    // Build the chart
    Highcharts.chart('pieChart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Presentase jumlah pemasukan & pengeluaran pertahun (<?php echo date('Y'); ?>)'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    // format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    format: '<b>{point.name}</b>: {point.y:f}, <br>{point.percentage:.1f} %',
                    connectorColor: 'silver'
                }
            }
        },
        series: [{
            name: 'Percentage',
            data: <?php echo $pieChart; ?> // $('#dataPieChart').val()
        }]
    });

    // -----------------------------

    // $.ajax({
    //     type: "POST",
    //     url : "{{ '/bulananRtDibulanBaru' }}",
    //     data: {
    //         _token: '{{ csrf_token() }}'
    //     },
    //     dataType: "json",
    //     success: function(data) {
    //        console.log(data.m)
    //     }
    // }); 
});
</script>
@endsection
