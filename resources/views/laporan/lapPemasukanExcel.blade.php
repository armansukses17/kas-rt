<html>
<head>
	
</head>
<body>
	<table>
		<tr>
			<td colspan="5" style="font-weight: bold; font-size: 14px;">
				{{ $headerInfo }} 
			</td>
		</tr>
	</table>
	<table>
		<thead>
			<tr>
				<th>No</th>
				<th>Jenis Pemasukan</th>
				<th>Tanggal Pemasukan</th>
				<th>Nominal</th>
				<th>Keterangan</th>
			</tr>
		</thead>
		<tbody>
			@php $i=1 @endphp
			@foreach($data as $p)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{$p->nama_pemasukan}}</td>
				<td>{{$p->tanggal_pemasukan}}</td>
				<td>{{$p->nominal}}</td>
				<td>{{$p->keterangan}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>