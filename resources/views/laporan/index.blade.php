@extends('master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h2>LAPORAN TRANSAKSI KAS</h2>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body" style="padding: 0;">
                            <div class="col-lg-12" style="padding: 0;">
                                <div class="card card-primary card-outline card-outline-tabs">
                                    <div class="card-header p-0 border-bottom-0">
                                        <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="tabMasukKeluarKas" data-toggle="pill" href="#custom-tabs-three-MasukKeluarKas" role="tab" aria-controls="custom-tabs-three-MasukKeluarKas" aria-selected="false">Total Kas</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="tabPemasukan" data-toggle="pill" href="#custom-tabs-three-pemasukan" role="tab" aria-controls="custom-tabs-three-pemasukan" aria-selected="true">Rincian Pemasukan</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="tabPengeluaran" data-toggle="pill" href="#custom-tabs-three-pengeluaran" role="tab" aria-controls="custom-tabs-three-pengeluaran" aria-selected="false">Rincian Pengeluaran</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="card-body">
                                        <div class="tab-content" id="custom-tabs-three-tabContent">
                                            <div class="tab-pane fade" id="custom-tabs-three-pemasukan" role="tabpanel" aria-labelledby="tabPemasukan">
                                                <div class="tile">
                                                    <div class="row">
                                                        <div class="col-md-12 mb-3"><strong>Filter Berdasarkan :</strong></div>
                                                    </div>
                                                    <form class="row" id="formFilter">
                                                        <div class="form-group col-md-2">
                                                            <select class="chosen-select form-control" name="filterType" id="filterType">
                                                                <option value="">-- Pilih --</option>
                                                                <option value="1">Bulan</option>
                                                                <option value="2">Tahun</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-5" id="wrapperstartFilter" style="position: relative;">
                                                            <input type="text" name="sementara" id="sementara" class="form-control" disabled>
                                                            <img src="{{ '/images/loading.gif' }}" id="loading" alt="Loading ..." style="display: none; position: absolute; top: 50%; left: 50%;">
                                                        </div>
                                                        <div class="form-group col-md-5">
                                                            <button type="submit" class="btn btn-primary" id="btnFilter" disabled><i class="fa fa-search"></i> Load</button>
                                                            <span id="wrapDownload"></span>
                                                        </div>
                                                    </form>
                                                </div>
                                                <hr>
                                                 <div class="card-body table-responsive p-0 mt-3">
                                                    <table class="table table-hover table-bordered" id="tblDataPemasukan">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 1%;">No</th>
                                                                <th style="width: 120px;">Jenis Pemasukan</th>
                                                                <th style="width: 70px;">Tanggal / Bulan</th>
                                                                <th>Nominal</th>
                                                                <th style="width: 200px;">Keterangan</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="bodyReportPemasukan">
                                                        </tbody>
                                                    </table>
                                                    <div class="info-footer mt-3">
                                                        <span style="color: #f00;">*Info</span> : 
                                                        <small>Khusus data <b>Kas bulanan RT</b>, nominalnya akan digabung perbulan</small>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane fade" id="custom-tabs-three-pengeluaran" role="tabpanel" aria-labelledby="tabPengeluaran">
                                                <div class="tile">
                                                    <div class="row">
                                                        <div class="col-md-12 mb-3"><strong>Filter Berdasarkan :</strong></div>
                                                    </div>
                                                    <form class="row" id="formFilter2">
                                                        <div class="form-group col-md-2">
                                                            <select class="chosen-select form-control" name="filterType2" id="filterType2">
                                                                <option value="">-- Pilih --</option>
                                                                <option value="1">Bulan</option>
                                                                <option value="2">Tahun</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-5" id="wrapperstartFilter2" style="position: relative;">
                                                            <input type="text" name="sementara2" id="sementara2" class="form-control" disabled>
                                                            <img src="{{ '/images/loading.gif' }}" id="loading2" alt="Loading ..." style="display: none; position: absolute; top: 50%; left: 50%;">
                                                        </div>
                                                        <div class="form-group col-md-5">
                                                            <button type="submit" class="btn btn-primary" id="btnFilter2" disabled><i class="fa fa-search"></i> Load</button>
                                                            <span id="wrapDownload2"></span>
                                                        </div>
                                                    </form>
                                                </div>
                                                <hr>
                                                <div class="card-body table-responsive p-0 mt-3">
                                                    <table class="table table-hover table-bordered" id="tblDataPengeluaran">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 1%;">No</th>
                                                                <th style="width: 120px;">Jenis Pengeluaran</th>
                                                                <th>Tanggal</th>
                                                                <th>Nominal</th>
                                                                <th style="width: 200px;">Keterangan</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="bodyReportPengeluaran">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="tab-pane fade active show" id="custom-tabs-three-MasukKeluarKas" role="tabpanel" aria-labelledby="tabMasukKeluarKas">
                                                <div class="tile">
                                                    <div class="row">
                                                        <div class="col-md-12 mb-3"><strong>Filter Berdasarkan :</strong></div>
                                                    </div>
                                                    <form class="row" id="formFilter3">
                                                        <div class="form-group col-md-2">
                                                            <select class="chosen-select form-control" name="filterType3" id="filterType3">
                                                                <option value="">-- Pilih --</option>
                                                                <!-- <option value="1">Bulan</option> -->
                                                                <option value="2">Tahun</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-5" id="wrapperstartFilter3" style="position: relative;">
                                                            <input type="text" name="sementara3" id="sementara3" class="form-control" disabled>
                                                            <img src="{{ '/images/loading.gif' }}" id="loading3" alt="Loading ..." style="display: none; position: absolute; top: 50%; left: 50%;">
                                                        </div>
                                                        <div class="form-group col-md-5">
                                                            <button type="submit" class="btn btn-primary" id="btnFilter3" disabled><i class="fa fa-search"></i> Load</button>
                                                            <span id="wrapDownload3"></span>
                                                        </div>
                                                    </form>
                                                </div>
                                                <hr>
                                                <div class="card-body table-responsive p-0 mt-3">
                                                    <table class="table table-hover table-bordered" id="tblDataMasukKeluarKas">
                                                        <thead>
                                                            <tr>
                                                                <th style="width: 20px;">No</th>
                                                                <th style="width: 120px;">Tahun/Bulan</th>
                                                                <th>Kas Masuk</th>
                                                                <th>Kas Keluar</th>
                                                                <th style="width: 160px;">Sisa Anggaran</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="bodyReportMasukKeluarKas">
                                                           
                                                        </tbody>
                                                        <tfoot id="footKasMasukKeluar">
                                                            <tr>
                                                                <td colspan="4">TOTAL KAS</td>
                                                                <td align="right" id="colTotalAnggaran" style="background: #e62626;"></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<style>
.card.card-primary.card-outline.card-outline-tabs {
    margin-bottom: 0;
}
#footKasMasukKeluar {
    background: #007bff;
    color: #fff;
    height: 35px;
}
#footKasMasukKeluar tr td {
    font-size: 17px;
    font-weight: bold;
}
</style>

<script>
// ############################################################
// PEMASUKAN
// ############################################################
$(function() {
    callPemasukan();
    nonActiveBtnFilter();
});

function callPemasukan() {
    $("#tblDataPemasukan").dataTable().fnDestroy();
    listTablePemasukan();
}

$('#tabPemasukan').on('click', function() {
    resetWhenClickTab();
    callPemasukan();
    $('#bodyReportPemasukan').empty();
});

function resetWhenClickTab() {
    $('#formFilter')[0].reset();
    $('#btnFilter').prop('disabled', true);
    nonActiveBtnFilter();
    var aaaaaaa = ` <input type="text" name="sementara" id="sementara" class="form-control" disabled>
                        <img src="{{ '/images/loading.gif' }}" id="loading" alt="Loading ..." style="display: none; position: absolute; top: 50%; left: 50%;">`;
    $('#wrapperstartFilter').html(`${aaaaaaa}`);
}

function nonActiveBtnFilter() {
    $('#wrapDownload').html(`<button class="btn btn-success btn-md" disabled> Download Excels </button>`);
}

function changeYear() {
    nonActiveBtnFilter();
}

function changeMonth() {
    nonActiveBtnFilter();
}

function listTablePemasukan(filterType='', filterBy='', yearWithMonth='') {
    $('#tblDataPemasukan').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ '/dtLapTransPemasukan' }}",
            data: {
                //  _token: '{{ csrf_token() }}', 
                filterType: filterType, 
                filterBy: filterBy,
                yearWithMonth: yearWithMonth
            },
            type: "GET"
        },
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false }, // no autoincrement
            { data: 'nama_pemasukan', name: 'nama_pemasukan' },
            { data: 'tanggalBulanan', name: 'tanggalBulanan' },
            { data: 'formatNominal', name: 'formatNominal' },
            { data: 'keteranganBulanan', name: 'keteranganBulanan' },
        ],
        // "order": [[ '1', "desc" ]]
    });
}

$('#filterType').change(function() {
    $('#bodyReportPemasukan').empty();
    nonActiveBtnFilter();
    var type = $(this).val();
    $('#loading').show();
    var loadingHide = `<img src="{{ '/images/loading.gif' }}" id="loading" alt="Loading ..." style="display: none; position: absolute; top: 50%; left: 50%;">`;
    var loadingHide2 = ` <input type="text" name="sementara" id="sementara" class="form-control" disabled>
                        <img src="{{ '/images/loading.gif' }}" id="loading" alt="Loading ..." style="display: none; position: absolute; top: 50%; left: 50%;">`;

    if (type == 1) {
        setTimeout(function() { 
            $('#btnFilter').prop('disabled', false);
            $('#wrapperstartFilter').html(`
                <div class="row">
                    <div class="col-md-7">
                        <select class="chosen-select form-control" name="filterBy" id="filterBy" onchange="changeMonth()">
                            <option value="">-- Pilih Bulan --</option>
                            <option value="01">Januari</option>
                            <option value="02">Februari</option>
                            <option value="03">Maret</option>
                            <option value="04">April</option>
                            <option value="05">Mei</option>
                            <option value="06">Juni</option>
                            <option value="07">Juli</option>
                            <option value="08">Agustus</option>
                            <option value="09">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                    </div>
                    <div class="col-md-5">
                        <select class="chosen-select form-control" name="yearWithMonth" id="yearWithMonth" onchange="changeYear()">
                            <option value="">-- Pilih Tahun --</option>
                            <?php
                            $batas = date('Y') + 1;
                            for ($i = 2021; $i < $batas; $i++) { 
                                echo '<option value="'.$i.'">'.$i.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                ${loadingHide}
            `);
        }, 500);
    }
    else if (type == 2) {
        setTimeout(function() { 
            $('#btnFilter').prop('disabled', false);
            $('#wrapperstartFilter').html(`
                <select class="chosen-select form-control" name="filterBy" id="filterBy" onchange="changeYear()">
                    <option value="">-- Pilih Tahun --</option>
                    <?php
                    $batas = date('Y') + 1;
                    for ($i = 2021; $i < $batas; $i++) { 
                        echo '<option value="'.$i.'">'.$i.'</option>';
                    }
                    ?>
                </select>
            ${loadingHide}
            `);
        }, 500);
    }
    else {
        $('#btnFilter').prop('disabled', true);
        callPemasukan();
        $('#wrapperstartFilter').html(`${loadingHide2}`);
    }
});

$('#formFilter').on('submit',function(e) {
    e.preventDefault();
    var filterType = $('#filterType').val();
	var filterBy = $('#filterBy').val();
    var yearWithMonth = $('#yearWithMonth').val();

    $('#wrapDownload').html(`
        <a class="btn btn-success btn-md" id="btnDownloadExcel"> Download Excels </a>
    `);

    if ((filterBy == '') || (yearWithMonth == '')) {
        alert('Data belum dipilih !');
        return false;
    }
    else {
        $("a#btnDownloadExcel").on('click', function(e) {
            e.preventDefault();
            var query = {
                filterType: filterType,
                filterBy: filterBy,
                yearWithMonth: yearWithMonth
            }
            var url = "{{URL::to('lapPemasukan')}}?" + $.param(query)
            window.location = url;
        });
        $("a#btnDownloadExcel").css({'cursor': 'pointer', 'background-color': '#28a745', 'border-color': '#28a745'});
        // -----------------------------------------
        $("#tblDataPemasukan").dataTable().fnDestroy();
        listTablePemasukan(filterType, filterBy, yearWithMonth);
    }
});

// ############################################################
// PENGELUARAN
// ############################################################

function callPengeluaran() {
    $("#tblDataPengeluaran").dataTable().fnDestroy();
    listTablePengeluaran();
}

$('#tabPengeluaran').on('click', function() {
    resetWhenClickTab2();
    callPengeluaran();
    $('#bodyReportPengeluaran').empty();
});

function resetWhenClickTab2() {
    $('#formFilter2')[0].reset();
    $('#btnFilter2').prop('disabled', true);
    nonActiveBtnFilter2();
    var aaaaaaa = ` <input type="text" name="sementara2" id="sementara2" class="form-control" disabled>
                        <img src="{{ '/images/loading.gif' }}" id="loading2" alt="Loading ..." style="display: none; position: absolute; top: 50%; left: 50%;">`;
    $('#wrapperstartFilter2').html(`${aaaaaaa}`);
}

function nonActiveBtnFilter2() {
    $('#wrapDownload2').html(`<button class="btn btn-success btn-md" disabled> Download Excels </button>`);
}

function changeYear2() {
    nonActiveBtnFilter2();
}

function changeMonth2() {
    nonActiveBtnFilter2();
}

function listTablePengeluaran(filterType2='', filterBy2='', yearWithMonth2='') {
    $('#tblDataPengeluaran').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ '/dtLapTransPengeluaran' }}",
            data: {
                //  _token: '{{ csrf_token() }}', 
                filterType2: filterType2, 
                filterBy2: filterBy2,
                yearWithMonth2: yearWithMonth2
            },
            type: "GET"
        },
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false }, // no autoincrement
            { data: 'nama_pemasukan', name: 'nama_pemasukan' },
            { data: 'tanggal_pengeluaran', name: 'tanggal_pengeluaran' },
            { data: 'formatNominal', name: 'formatNominal' },
            { data: 'keterangan', name: 'keterangan' },
        ],
        // "order": [[ '1', "desc" ]]
    });
}

$('#filterType2').change(function() {
    $('#bodyReportPengeluaran').empty();
    nonActiveBtnFilter2();
    var type = $(this).val();
    $('#loading2').show();
    var loadingHide = `<img src="{{ '/images/loading.gif' }}" id="loading2" alt="Loading ..." style="display: none; position: absolute; top: 50%; left: 50%;">`;
    var loadingHide2 = ` <input type="text" name="sementara2" id="sementara2" class="form-control" disabled>
                        <img src="{{ '/images/loading.gif' }}" id="loading2" alt="Loading ..." style="display: none; position: absolute; top: 50%; left: 50%;">`;

    if (type == 1) {
        setTimeout(function() { 
            $('#btnFilter2').prop('disabled', false);
            $('#wrapperstartFilter2').html(`
                <div class="row">
                    <div class="col-md-7">
                        <select class="chosen-select form-control" name="filterBy2" id="filterBy2" onchange="changeMonth2()">
                            <option value="">-- Pilih Bulan --</option>
                            <option value="01">Januari</option>
                            <option value="02">Februari</option>
                            <option value="03">Maret</option>
                            <option value="04">April</option>
                            <option value="05">Mei</option>
                            <option value="06">Juni</option>
                            <option value="07">Juli</option>
                            <option value="08">Agustus</option>
                            <option value="09">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                    </div>
                    <div class="col-md-5">
                        <select class="chosen-select form-control" name="yearWithMonth2" id="yearWithMonth2" onchange="changeYear2()">
                            <option value="">-- Pilih Tahun --</option>
                            <?php
                            $batas = date('Y') + 1;
                            for ($i = 2021; $i < $batas; $i++) { 
                                echo '<option value="'.$i.'">'.$i.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                ${loadingHide}
            `);
        }, 500);
    }
    else if (type == 2) {
        setTimeout(function() { 
            $('#btnFilter2').prop('disabled', false);
            $('#wrapperstartFilter2').html(`
                <select class="chosen-select form-control" name="filterBy2" id="filterBy2" onchange="changeYear2()">
                    <option value="">-- Pilih Tahun --</option>
                    <?php
                    $batas = date('Y') + 1;
                    for ($i = 2021; $i < $batas; $i++) { 
                        echo '<option value="'.$i.'">'.$i.'</option>';
                    }
                    ?>
                </select>
            ${loadingHide}
            `);
        }, 500);
    }
    else {
        $('#btnFilter2').prop('disabled', true);
        callPengeluaran();
        $('#wrapperstartFilter2').html(`${loadingHide2}`);
    }
});

$('#formFilter2').on('submit',function(e) {
    e.preventDefault();
    var filterType2 = $('#filterType2').val();
	var filterBy2 = $('#filterBy2').val();
    var yearWithMonth2 = $('#yearWithMonth2').val();

     $('#wrapDownload2').html(`
        <a class="btn btn-success btn-md" id="btnDownloadExcel2"> Download Excels </a>
    `);

    if ((filterBy2 == '') || (yearWithMonth2 == '')) {
        alert('Data belum dipilih !');
        return false;
    }
    else {
        $("a#btnDownloadExcel2").on('click', function(e) {
            e.preventDefault();
            var query = {
                filterType2: filterType2,
                filterBy2: filterBy2,
                yearWithMonth2: yearWithMonth2
            }
            var url = "{{URL::to('lapPengeluaran')}}?" + $.param(query)
            window.location = url;
        });
        $("a#btnDownloadExcel2").css({'cursor': 'pointer', 'background-color': '#28a745', 'border-color': '#28a745'});
        // -----------------------------------------
        $("#tblDataPengeluaran").dataTable().fnDestroy();
        listTablePengeluaran(filterType2, filterBy2, yearWithMonth2);
    }
});

// ############################################################
// MASUK & KELUAR KAS
// ############################################################
function callMasukKeluarKas() {
    $("#tblDataMasukKeluarKas").dataTable().fnDestroy();
    listTableMasukKeluarKas();
}

$('#tabMasukKeluarKas').on('click', function() {
    resetWhenClickTab3();
    callMasukKeluarKas();
    $('#bodyReportMasukKeluarKas').empty();
});

function resetWhenClickTab3() {
    $('#formFilter3')[0].reset();
    $('#btnFilter3').prop('disabled', true);
    nonActiveBtnFilter3();
    var aaaaaaa = ` <input type="text" name="sementara3" id="sementara3" class="form-control" disabled>
                        <img src="{{ '/images/loading.gif' }}" id="loading3" alt="Loading ..." style="display: none; position: absolute; top: 50%; left: 50%;">`;
    $('#wrapperstartFilter3').html(`${aaaaaaa}`);
    $('#colTotalAnggaran').empty();
    $('#footKasMasukKeluar').hide();
}

function nonActiveBtnFilter3() {
    $('#wrapDownload3').html(`<button class="btn btn-success btn-md" disabled> Download Excels </button>`);
}

function changeYear3() {
    nonActiveBtnFilter3();
}

function changeMonth3() {
    nonActiveBtnFilter3();
}

function listTableMasukKeluarKas(filterType3='', filterBy3='', yearWithMonth3='') {
    $.ajax({
        type: "GET",
        url : "{{ '/dtLapTransMasukKeluarKas' }}",
        data: {
            //  _token: '{{ csrf_token() }}', 
            filterType3: filterType3, 
            filterBy3: filterBy3,
            yearWithMonth3: yearWithMonth3
        },
        dataType: "json",
        async: false, 
        success: function(data) {
            if (data.s == 'success') {
                let html = '';
                let start = '';
                let kasMasuk = '';
                let kasKeluar = '';
                let sisAnggaran = 0;
                for (i=0; i<data.m.length; i++) {
                    start = i + 1;
                    // ----------------------------------------------------------
                    // PR
                    // (index pertama tdk melakukan ini)
                    // Ambil sisa anggaran by index sebelumnya 
                    // sisa_anggaran = sisa_anggaran_sebelumnya + (masuk - keluar)
                    // ----------------------------------------------------------
                    
                    kasMasuk = (data.m[i].kas_masuk) ? data.m[i].kas_masuk : 0;
                    kasKeluar = (data.m[i].kas_keluar) ? data.m[i].kas_keluar : 0;

                    if (i == 0) {
                        sisAnggaran = parseInt(kasMasuk) - parseInt(kasKeluar);
                    } else {
                        sisAnggaran = (parseInt(sisAnggaran) + parseInt(kasMasuk)) - parseInt(kasKeluar);
                    }

                    html += `<tr id="row-${start}">
                            <td>${start}</td>
                            <td>${data.m[i].tanggal}</td>
                            <td align="right">${formatCurrency(kasMasuk)}</td>
                            <td align="right">${formatCurrency(kasKeluar)}</td>
                            <td align="right">${formatCurrency(sisAnggaran)}</td>
                        </tr>
                    `;
                }
                $('#bodyReportMasukKeluarKas').html(html);
                // -----------------------------------------
                setTimeout(function() { 
                    $('#colTotalAnggaran').text(formatCurrency(sisAnggaran));
                }, 200);
            } 
            else {
                alert('error');
            }
        }
    }); 
}

$('#filterType3').change(function() {
    $('#bodyReportMasukKeluarKas').empty();
    nonActiveBtnFilter3();
    var type = $(this).val();
    $('#loading3').show();
    var loadingHide = `<img src="{{ '/images/loading.gif' }}" id="loading3" alt="Loading ..." style="display: none; position: absolute; top: 50%; left: 50%;">`;
    var loadingHide2 = ` <input type="text" name="sementara3" id="sementara3" class="form-control" disabled>
                        <img src="{{ '/images/loading.gif' }}" id="loading3" alt="Loading ..." style="display: none; position: absolute; top: 50%; left: 50%;">`;

    if (type == 1) {
        setTimeout(function() { 
            $('#btnFilter3').prop('disabled', false);
            $('#wrapperstartFilter3').html(`
                <div class="row">
                    <div class="col-md-7">
                        <select class="chosen-select form-control" name="filterBy3" id="filterBy3" onchange="changeMonth2()">
                            <option value="">-- Pilih Bulan --</option>
                            <option value="01">Januari</option>
                            <option value="02">Februari</option>
                            <option value="03">Maret</option>
                            <option value="04">April</option>
                            <option value="05">Mei</option>
                            <option value="06">Juni</option>
                            <option value="07">Juli</option>
                            <option value="08">Agustus</option>
                            <option value="09">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                    </div>
                    <div class="col-md-5">
                        <select class="chosen-select form-control" name="yearWithMonth3" id="yearWithMonth3" onchange="changeYear2()">
                            <option value="">-- Pilih Tahun --</option>
                            <?php
                            $batas = date('Y') + 1;
                            for ($i = 2021; $i < $batas; $i++) { 
                                echo '<option value="'.$i.'">'.$i.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                ${loadingHide}
            `);
        }, 500);
    }
    else if (type == 2) {
        setTimeout(function() { 
            $('#btnFilter3').prop('disabled', false);
            $('#wrapperstartFilter3').html(`
                <select class="chosen-select form-control" name="filterBy3" id="filterBy3" onchange="changeYear2()">
                    <option value="">-- Pilih Tahun --</option>
                    <?php
                    $batas = date('Y') + 1;
                    for ($i = 2021; $i < $batas; $i++) { 
                        echo '<option value="'.$i.'">'.$i.'</option>';
                    }
                    ?>
                </select>
            ${loadingHide}
            `);
        }, 500);
    }
    else {
        $('#btnFilter3').prop('disabled', true);
        callMasukKeluarKas();
        $('#wrapperstartFilter3').html(`${loadingHide2}`);
        $('#footKasMasukKeluar').hide();
    }
});

$('#formFilter3').on('submit',function(e) {
    e.preventDefault();
    var filterType3 = $('#filterType3').val();
	var filterBy3 = $('#filterBy3').val();
    var yearWithMonth3 = $('#yearWithMonth3').val();

     $('#wrapDownload3').html(`
        <a class="btn btn-success btn-md" id="btnDownloadExcel3"> Download Excels </a>
    `);

    if ((filterBy3 == '') || (yearWithMonth3 == '')) {
        alert('Data belum dipilih !');
        return false;
    }
    else {
        $('#loading3').show();
        setTimeout(function() { 
            $('#loading3').hide();
            $("a#btnDownloadExcel3").on('click', function(e) {
                e.preventDefault();
                var query = {
                    filterType3: filterType3,
                    filterBy3: filterBy3,
                    yearWithMonth3: yearWithMonth3
                }
                var url = "{{URL::to('lapMasukKeluarKas')}}?" + $.param(query)
                window.location = url;
            });
            $("a#btnDownloadExcel3").css({'cursor': 'pointer', 'background-color': '#28a745', 'border-color': '#28a745'});
            // -----------------------------------------
            $("#tblDataMasukKeluarKas").dataTable().fnDestroy();
            listTableMasukKeluarKas(filterType3, filterBy3, yearWithMonth3);
            $('#footKasMasukKeluar').show();
         }, 500);
    }
});

</script>
@endsection
