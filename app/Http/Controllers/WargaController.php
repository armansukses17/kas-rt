<?php

namespace App\Http\Controllers;

use App\Warga;
use App\Anak;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class WargaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {        
        return view('warga.listData');
    }

    public function indexPendatang()
    {        
        return view('warga.listDataPendatang');
    }

    public function createWarga()
    {
        return view('warga.create');
    }

    public function createWargaPendatang()
    {
        return view('warga.createPendatang');
    }

    public function checkSuamiIstri($suami, $istri)
    {
        if (!empty($suami) AND !empty($istri))
        {
            return $suami .'/'. $istri;
        }
        elseif (!empty($suami))
        {
            return $suami;
        }
        else
        {
            return $istri;
        }
    }

    public function datatablesWarga()
    {
        $data = DB::table('warga')->select('*')->where('status_warga', '=', 'Warga')->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('suamiDanIstri', function ($a) {
                $nama = $this->checkSuamiIstri($a->nama_suami, $a->nama_istri);
                $suamiIstri = $nama;
                return $suamiIstri;
            })
            ->addColumn('noTelp', function ($b) {
                $noTelpSuamiIstri = $this->checkSuamiIstri($b->no_telp_suami, $b->no_telp_istri);
                $telp = $noTelpSuamiIstri;
                return $telp;
            })
            ->addColumn('alamatKeluarga', function ($c) {
                if (!empty($c->alamat_suami) AND !empty($c->alamat_istri))
                {
                    $alamatKeluarga = $c->alamat_suami;
                }
                elseif (!empty($c->alamat_suami))
                {
                    $alamatKeluarga = $c->alamat_suami;
                }
                else
                {
                    $alamatKeluarga = $c->alamat_istri;
                }

                $alamat = $alamatKeluarga;
                return $alamat;
            })
            ->addColumn('action', function($row) {
                $btn = '
                        <div class="hidden-sm hidden-xs btn-group">
                            <button class="btn btn-xs btn-info btn-edit" onclick="editWarga('.$row->id.')" title="Detail / Edit">
                                <i class="nav-icon fas fa-edit"></i>
                            </button> &nbsp
                        </div>
                        ';
                return $btn;
            })
            ->rawColumns(['suamiDanIstri', 'noTelp', 'alamatKeluarga', 'action'])
            ->make(true);
    }

    public function datatablesWargaPendatang()
    {
        $data = DB::table('warga')->select('*')->where('status_warga', '=', 'Pendatang')->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('suamiDanIstri', function ($a) {
                $nama = $this->checkSuamiIstri($a->nama_suami, $a->nama_istri);
                $suamiIstri = $nama;
                return $suamiIstri;
            })
            ->addColumn('noTelp', function ($b) {
                $noTelpSuamiIstri = $this->checkSuamiIstri($b->no_telp_suami, $b->no_telp_istri);
                $telp = $noTelpSuamiIstri;
                return $telp;
            })
            ->addColumn('alamatKeluarga', function ($c) {
                if (!empty($c->alamat_suami) AND !empty($c->alamat_istri))
                {
                    $alamatKeluarga = $c->alamat_suami;
                }
                elseif (!empty($c->alamat_suami))
                {
                    $alamatKeluarga = $c->alamat_suami;
                }
                else
                {
                    $alamatKeluarga = $c->alamat_istri;
                }

                $alamat = $alamatKeluarga;
                return $alamat;
            })
            ->addColumn('action', function($row) {
                $btn = '
                        <div class="hidden-sm hidden-xs btn-group">
                            <button class="btn btn-xs btn-info btn-edit" onclick="editWarga('.$row->id.')" title="Detail / Edit">
                                <i class="nav-icon fas fa-edit"></i>
                            </button> &nbsp
                        </div>
                        ';
                return $btn;
            })
            ->rawColumns(['suamiDanIstri', 'action'])
            ->make(true);
    }

    public function checkJmlOrtu($idHeader)
    {
        $checkAyahIbu = DB::table('warga')->select('*')->where('id', '=', $idHeader)->get();
        $ortuAyah = !empty($checkAyahIbu[0]->nama_suami) ? 1 : 0;
        $ortuIbu = !empty($checkAyahIbu[0]->nama_istri) ? 1 : 0;
        return ['checkAayah' =>  $ortuAyah, 'checkIbu' => $ortuIbu];
    }

    public function store(Request $request)
    {
        if ($request->ajax()) 
        {
            DB::beginTransaction();
            try {
                $rules = array(
                    'no_kk' => 'required',
                    'file_kk' => 'required|mimes:jpeg,png,jpg|max:2048'
                );
                $message = array(
                    'no_kk.required' => 'Nomor kartu keluarga wajib diisi !',
                    'file_kk.required' => 'File KK wajib diunggah !',
                    'file_kk.max' => 'Ukuran file KK max 2048MB !'
                );

                $validator = Validator::make($request->all(), $rules, $message);
                if ($validator->fails()) {
                    return response()->json(['errors'=>$validator->errors()->all()]);
                }

                $imagePath = $request->file('file_kk');
                $imageName = md5($imagePath->getClientOriginalName()).date('his');
                $newNamePlusExt = $imageName.".".$request->file_kk->extension();
                $path = $request->file('file_kk')->storeAs('public/file', $newNamePlusExt);
                // $path = $request->image->move(public_path('file'), $imageName);
                // -----------------------------------------
                $insertHeader = Warga::create([
                    'no_kk' => $request->no_kk,
                    'file_kk' => $newNamePlusExt,
                    'storage' => $path,
                    'status_warga' => base64_decode($request->status_warga),
                    // -------------------------------------
                    'status_hub_kel_suami' => (!empty($request->nama_suami) AND !empty($request->nama_istri)) ? 'Suami' : '',
                    'nik_suami' => isset($request->nik_suami) ? $request->nik_suami : '',
                    'nama_suami' => isset($request->nama_suami) ? $request->nama_suami : '',
                    'tempat_lahir_suami' => isset($request->tempat_lahir_suami) ? $request->tempat_lahir_suami : '',
                    'tanggal_lahir_suami' => isset($request->tanggal_lahir_suami) ? \Carbon\Carbon::parse($request->tanggal_lahir_suami)->format('Y-m-d') : '0000-00-00',
                    'jenis_kelamin_suami' => isset($request->nama_suami) ? 'L' : '',
                    'agama_suami' => isset($request->agama_suami) ? $request->agama_suami : '',
                    'no_telp_suami' => isset($request->no_telp_suami) ? $request->no_telp_suami : '',
                    'alamat_suami' => isset($request->alamat_suami) ? $request->alamat_suami : '',
                    'pekerjaan_suami' => isset($request->pekerjaan_suami) ? $request->pekerjaan_suami : '',
                    // -------------------------------------
                    'status_hub_kel_istri' => (!empty($request->nama_suami) AND !empty($request->nama_istri)) ? 'Istri' : '',
                    'nik_istri' => isset($request->nik_istri) ? $request->nik_istri : '',
                    'nama_istri' => isset($request->nama_istri) ? $request->nama_istri : '',
                    'tempat_lahir_istri' => isset($request->tempat_lahir_istri) ? $request->tempat_lahir_istri : '',
                    'tanggal_lahir_istri' => isset($request->tanggal_lahir_istri) ? \Carbon\Carbon::parse($request->tanggal_lahir_istri)->format('Y-m-d') : '0000-00-00',
                    'jenis_kelamin_istri' => isset($request->nama_istri) ? 'P' : '',
                    'agama_istri' => isset($request->agama_istri) ? $request->agama_istri : '',
                    'no_telp_istri' => isset($request->no_telp_istri) ? $request->no_telp_istri : '',
                    'alamat_istri' => isset($request->alamat_istri) ? $request->alamat_istri : '',
                    'pekerjaan_istri' => isset($request->pekerjaan_istri) ? $request->pekerjaan_istri : ''
                ]);
                $idHeader = $insertHeader->id;
                // ---------------------------------------------------
                $dataAnakArray = isset($request->nama_anak) ? $request->nama_anak : 0;
                $dataAnakList1 = isset($request->nama_anak[0]) ? $request->nama_anak[0] : '';
                // print_r(count($dataAnakArray)); die;
                if (!empty($dataAnakList1) || (count($dataAnakArray) > 0 AND !empty($request->nama_anak[0])))
                { 
                    for ($i = 0; $i < count($request->nik_anak); $i++) 
                    {
                        if (empty($request->nama_anak[$i])) continue; // tanpa array kosong

                        $dataArray[] = [
                            'id_header' => $idHeader,
                            'nik_anak' => $request->nik_anak[$i],
                            'nama_anak' => $request->nama_anak[$i],
                            'tempat_lahir_anak' => isset($request->tempat_lahir_anak[$i]) ? $request->tempat_lahir_anak[$i] : '',
                            'tanggal_lahir_anak' => isset($request->tanggal_lahir_anak[$i]) ? \Carbon\Carbon::parse($request->tanggal_lahir_anak[$i])->format('Y-m-d') : '0000-00-00',
                            'jenis_kelamin_anak' => isset($request->jenis_kelamin_anak[$i]) ? $request->jenis_kelamin_anak[$i] : '',
                            'agama_anak' => isset($request->agama_anak[$i]) ? $request->agama_anak[$i] : '',
                            'created_at' => \Carbon\Carbon::now()->toDateTimeString(), // untuk mengisi field ini ketika insert array
                            'updated_at' => \Carbon\Carbon::now()->toDateTimeString() // untuk mengisi field ini ketika insert array
                        ];
                    }
                    Anak::insert($dataArray);
                }
                // hitung ulang jumlah keluarga
                // ---------------------------------
                $this->hitungUlangJmlKeluarga($idHeader);
                // ---------------------------------------------------
                Session::flash('success_msg', 'Data berhasil disimpan !');
                $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
                // ---------------------------------------------------
                DB::commit();
            } catch (\Throwable $e) {
                // ---------------------------------------------------
                DB::rollback();
                // ---------------------------------------------------
                throw $e;
            } 
        }
        return response()->json($return, 200);
    }

    public function storePendatang(Request $request)
    {
        if ($request->ajax()) 
        {
            DB::beginTransaction();

            try {
                $rules = array(
                    'no_kk' => 'required',
                    'file_kk' => 'required|mimes:jpeg,png,jpg|max:2048',
                );
                $message = array(
                    'no_kk.required' => 'Nomor kartu keluarga wajib diisi !',
                    'file_kk.required' => 'File KK wajib diunggah !',
                    'file_kk.max' => 'Ukuran file KK max 2048MB !',
                );

                $validator = Validator::make($request->all(), $rules, $message);
                if ($validator->fails()) {
                    return response()->json(['errors'=>$validator->errors()->all()]);
                }

                $imagePath = $request->file('file_kk');
                $imageName = md5($imagePath->getClientOriginalName()).date('his');
                $newNamePlusExt = $imageName.".".$request->file_kk->extension();
                $path = $request->file('file_kk')->storeAs('public/file', $newNamePlusExt);
                // -----------------------------------------
                $insertHeader = Warga::create([
                    'no_kk' => $request->no_kk,
                    'file_kk' => $newNamePlusExt,
                    'storage' => $path,
                    'status_warga' => base64_decode($request->status_warga),
                    // -------------------------------------
                    'status_hub_kel_suami' => (!empty($request->nama_suami) AND !empty($request->nama_istri)) ? 'Suami' : '',
                    'nik_suami' => isset($request->nik_suami) ? $request->nik_suami : '',
                    'nama_suami' => isset($request->nama_suami) ? $request->nama_suami : '',
                    'tempat_lahir_suami' => isset($request->tempat_lahir_suami) ? $request->tempat_lahir_suami : '',
                    'tanggal_lahir_suami' => isset($request->tanggal_lahir_suami) ? \Carbon\Carbon::parse($request->tanggal_lahir_suami)->format('Y-m-d') : '0000-00-00',
                    'jenis_kelamin_suami' => isset($request->nama_suami) ? 'L' : '',
                    'agama_suami' => isset($request->agama_suami) ? $request->agama_suami : '',
                    'no_telp_suami' => isset($request->no_telp_suami) ? $request->no_telp_suami : '',
                    'alamat_suami' => isset($request->alamat_suami) ? $request->alamat_suami : '',
                    'pekerjaan_suami' => isset($request->pekerjaan_suami) ? $request->pekerjaan_suami : '',
                    // -------------------------------------
                    'status_hub_kel_istri' => (!empty($request->nama_suami) AND !empty($request->nama_istri)) ? 'Istri' : '',
                    'nik_istri' => isset($request->nik_istri) ? $request->nik_istri : '',
                    'nama_istri' => isset($request->nama_istri) ? $request->nama_istri : '',
                    'tempat_lahir_istri' => isset($request->tempat_lahir_istri) ? $request->tempat_lahir_istri : '',
                    'tanggal_lahir_istri' => isset($request->tanggal_lahir_istri) ? \Carbon\Carbon::parse($request->tanggal_lahir_istri)->format('Y-m-d') : '0000-00-00',
                    'jenis_kelamin_istri' => isset($request->nama_istri) ? 'P' : '',
                    'agama_istri' => isset($request->agama_istri) ? $request->agama_istri : '',
                    'no_telp_istri' => isset($request->no_telp_istri) ? $request->no_telp_istri : '',
                    'alamat_istri' => isset($request->alamat_istri) ? $request->alamat_istri : '',
                    'pekerjaan_istri' => isset($request->pekerjaan_istri) ? $request->pekerjaan_istri : ''
                ]);
                $idHeader = $insertHeader->id;
                // ---------------------------------------------------
                $dataAnakArray = isset($request->nama_anak) ? $request->nama_anak : 0;
                $dataAnakList1 = isset($request->nama_anak[0]) ? $request->nama_anak[0] : '';
                // print_r(count($dataAnakArray)); die;
                if (!empty($dataAnakList1) || (count($dataAnakArray) > 0 AND !empty($request->nama_anak[0])))
                { 
                    for ($i = 0; $i < count($request->nik_anak); $i++) 
                    {
                        if (empty($request->nama_anak[$i])) continue; // tanpa array kosong

                        $dataArray[] = [
                            'id_header' => $idHeader,
                            'nik_anak' => $request->nik_anak[$i],
                            'nama_anak' => $request->nama_anak[$i],
                            'tempat_lahir_anak' => isset($request->tempat_lahir_anak[$i]) ? $request->tempat_lahir_anak[$i] : '',
                            'tanggal_lahir_anak' => isset($request->tanggal_lahir_anak[$i]) ? \Carbon\Carbon::parse($request->tanggal_lahir_anak[$i])->format('Y-m-d') : '0000-00-00',
                            'jenis_kelamin_anak' => isset($request->jenis_kelamin_anak[$i]) ? $request->jenis_kelamin_anak[$i] : '',
                            'agama_anak' => isset($request->agama_anak[$i]) ? $request->agama_anak[$i] : '',
                            'created_at' => \Carbon\Carbon::now()->toDateTimeString(), // untuk mengisi field ini ketika insert array
                            'updated_at' => \Carbon\Carbon::now()->toDateTimeString() // untuk mengisi field ini ketika insert array
                        ];
                    }
                    Anak::insert($dataArray);
                }
                // hitung ulang jumlah keluarga
                // ---------------------------------
                $this->hitungUlangJmlKeluarga($idHeader);
                // ---------------------------------------------------
                Session::flash('success_msg', 'Data berhasil disimpan !');
                $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
                // ---------------------------------------------------
                DB::commit();
            } catch (\Throwable $e) {
                // ---------------------------------------------------
                DB::rollback();
                // ---------------------------------------------------
                throw $e;
            } 
        }
        return response()->json($return, 200);
    }

    public function edit($param)
    {
        $ortu = DB::table('warga')->where('id', '=', $param)->get()->toArray();
        $getAnak = DB::table('anak')->where('id_header', '=', $param)->get()->toArray();
        $anak = ['anak' => $getAnak];
        $mergeData = array_merge($ortu, $anak);
        return response()->json(['s' => 'success', 'data' => $mergeData], 200);
    }

    public function update(Request $request)
    {
        if ($request->ajax()) 
        {
            $findData = Warga::find(base64_decode($request->keyEdit));
            if (empty($findData->file_kk))
            {
                $rules = array(
                    'no_kk' => 'required',
                    'file_kk_edit' => 'required|mimes:jpeg,png,jpg|max:2048',
                );
                $message = array(
                    'no_kk.required' => 'Nomor kartu keluarga wajib diisi !',
                    'file_kk_edit.required' => 'File KK wajib diunggah !',
                    'file_kk_edit.max' => 'Ukuran file KK max 2048MB !',
                    'file_kk_edit.mimes' => 'File KK harus berformat (jpeg,png,jpg) !',
                );

                $validator = Validator::make($request->all(), $rules, $message);
                if ($validator->fails()) {
                    return response()->json(['errors'=>$validator->errors()->all()]);
                }

                // -----------------------------------------
                // Saat gambar diubah
                $imagePath = $request->file('file_kk_edit');
                $imageName = md5($imagePath->getClientOriginalName());
                $newNamePlusExt = $imageName.".".$request->file_kk_edit->extension();
                $path = $request->file('file_kk_edit')->storeAs('public/file', $newNamePlusExt);
                // -----------------------------------------

                Warga::where('id', base64_decode($request->keyEdit))->update([
                    'no_kk' => $request->no_kk,
                    'file_kk' => $newNamePlusExt,
                    'storage' => $path,
                    // -------------------------------------
                    'status_hub_kel_suami' => (!empty($request->nama_suami) AND !empty($request->nama_istri)) ? 'Suami' : '',
                    'nik_suami' => isset($request->nik_suami) ? $request->nik_suami : '',
                    'nama_suami' => isset($request->nama_suami) ? $request->nama_suami : '',
                    'tempat_lahir_suami' => isset($request->tempat_lahir_suami) ? $request->tempat_lahir_suami : '',
                    'tanggal_lahir_suami' => isset($request->tanggal_lahir_suami) ? \Carbon\Carbon::parse($request->tanggal_lahir_suami)->format('Y-m-d') : '0000-00-00',
                    'jenis_kelamin_suami' => isset($request->nama_suami) ? 'L' : '',
                    'agama_suami' => isset($request->agama_suami) ? $request->agama_suami : '',
                    'no_telp_suami' => isset($request->no_telp_suami) ? $request->no_telp_suami : '',
                    'alamat_suami' => isset($request->alamat_suami) ? $request->alamat_suami : '',
                    'pekerjaan_suami' => isset($request->pekerjaan_suami) ? $request->pekerjaan_suami : '',
                    // -------------------------------------
                    'status_hub_kel_istri' => (!empty($request->nama_suami) AND !empty($request->nama_istri)) ? 'Istri' : '',
                    'nik_istri' => isset($request->nik_istri) ? $request->nik_istri : '',
                    'nama_istri' => isset($request->nama_istri) ? $request->nama_istri : '',
                    'tempat_lahir_istri' => isset($request->tempat_lahir_istri) ? $request->tempat_lahir_istri : '',
                    'tanggal_lahir_istri' => isset($request->tanggal_lahir_istri) ? \Carbon\Carbon::parse($request->tanggal_lahir_istri)->format('Y-m-d') : '0000-00-00',
                    'jenis_kelamin_istri' => isset($request->nama_istri) ? 'P' : '',
                    'agama_istri' => isset($request->agama_istri) ? $request->agama_istri : '',
                    'no_telp_istri' => isset($request->no_telp_istri) ? $request->no_telp_istri : '',
                    'alamat_istri' => isset($request->alamat_istri) ? $request->alamat_istri : '',
                    'pekerjaan_istri' => isset($request->pekerjaan_istri) ? $request->pekerjaan_istri : ''
                ]);
            }
            else
            {
                $rules = array(
                    'no_kk' => 'required',
                );
                $message = array(
                    'no_kk.required' => 'Nomor kartu keluarga wajib diisi !',
                );

                $validator = Validator::make($request->all(), $rules, $message);
                if ($validator->fails()) {
                    return response()->json(['errors'=>$validator->errors()->all()]);
                }

                Warga::where('id', base64_decode($request->keyEdit))->update([
                    'no_kk' => $request->no_kk,
                    // -------------------------------------
                    'status_hub_kel_suami' => (!empty($request->nama_suami) AND !empty($request->nama_istri)) ? 'Suami' : '',
                    'nik_suami' => isset($request->nik_suami) ? $request->nik_suami : '',
                    'nama_suami' => isset($request->nama_suami) ? $request->nama_suami : '',
                    'tempat_lahir_suami' => isset($request->tempat_lahir_suami) ? $request->tempat_lahir_suami : '',
                    'tanggal_lahir_suami' => isset($request->tanggal_lahir_suami) ? \Carbon\Carbon::parse($request->tanggal_lahir_suami)->format('Y-m-d') : '0000-00-00',
                    'jenis_kelamin_suami' => isset($request->nama_suami) ? 'L' : '',
                    'agama_suami' => isset($request->agama_suami) ? $request->agama_suami : '',
                    'no_telp_suami' => isset($request->no_telp_suami) ? $request->no_telp_suami : '',
                    'alamat_suami' => isset($request->alamat_suami) ? $request->alamat_suami : '',
                    'pekerjaan_suami' => isset($request->pekerjaan_suami) ? $request->pekerjaan_suami : '',
                    // -------------------------------------
                    'status_hub_kel_istri' => (!empty($request->nama_suami) AND !empty($request->nama_istri)) ? 'Istri' : '',
                    'nik_istri' => isset($request->nik_istri) ? $request->nik_istri : '',
                    'nama_istri' => isset($request->nama_istri) ? $request->nama_istri : '',
                    'tempat_lahir_istri' => isset($request->tempat_lahir_istri) ? $request->tempat_lahir_istri : '',
                    'tanggal_lahir_istri' => isset($request->tanggal_lahir_istri) ? \Carbon\Carbon::parse($request->tanggal_lahir_istri)->format('Y-m-d') : '0000-00-00',
                    'jenis_kelamin_istri' => isset($request->nama_istri) ? 'P' : '',
                    'agama_istri' => isset($request->agama_istri) ? $request->agama_istri : '',
                    'no_telp_istri' => isset($request->no_telp_istri) ? $request->no_telp_istri : '',
                    'alamat_istri' => isset($request->alamat_istri) ? $request->alamat_istri : '',
                    'pekerjaan_istri' => isset($request->pekerjaan_istri) ? $request->pekerjaan_istri : ''
                ]);
            }
            // hitung ulang jumlah keluarga
            // ---------------------------------
            $this->hitungUlangJmlKeluarga(base64_decode($request->keyEdit));
            $return = ['s' => 'success', 'm' => 'Data berhasil diperbarui !'];
        }
        return response()->json($return, 200);
    }

    public function updatePendatang(Request $request)
    {
        if ($request->ajax()) 
        {
            $findData = Warga::find(base64_decode($request->keyEdit));
            if (empty($findData->file_kk))
            {
                $rules = array(
                    'no_kk' => 'required',
                    'file_kk_edit' => 'required|mimes:jpeg,png,jpg|max:2048',
                );
                $message = array(
                    'no_kk.required' => 'Nomor kartu keluarga wajib diisi !',
                    'file_kk_edit.required' => 'File KK wajib diunggah !',
                    'file_kk_edit.max' => 'Ukuran file KK max 2048MB !',
                    'file_kk_edit.mimes' => 'File KK harus berformat (jpeg,png,jpg) !',
                );

                $validator = Validator::make($request->all(), $rules, $message);
                if ($validator->fails()) {
                    return response()->json(['errors'=>$validator->errors()->all()]);
                }

                // -----------------------------------------
                // Saat gambar diubah
                $imagePath = $request->file('file_kk_edit');
                $imageName = md5($imagePath->getClientOriginalName());
                $newNamePlusExt = $imageName.".".$request->file_kk_edit->extension();
                $path = $request->file('file_kk_edit')->storeAs('public/file', $newNamePlusExt);
                // -----------------------------------------

                Warga::where('id', base64_decode($request->keyEdit))->update([
                    'no_kk' => $request->no_kk,
                    'file_kk' => $newNamePlusExt,
                    'storage' => $path,
                    // -------------------------------------
                    'status_hub_kel_suami' => (!empty($request->nama_suami) AND !empty($request->nama_istri)) ? 'Suami' : '',
                    'nik_suami' => isset($request->nik_suami) ? $request->nik_suami : '',
                    'nama_suami' => isset($request->nama_suami) ? $request->nama_suami : '',
                    'tempat_lahir_suami' => isset($request->tempat_lahir_suami) ? $request->tempat_lahir_suami : '',
                    'tanggal_lahir_suami' => isset($request->tanggal_lahir_suami) ? \Carbon\Carbon::parse($request->tanggal_lahir_suami)->format('Y-m-d') : '0000-00-00',
                    'jenis_kelamin_suami' => isset($request->nama_suami) ? 'L' : '',
                    'agama_suami' => isset($request->agama_suami) ? $request->agama_suami : '',
                    'no_telp_suami' => isset($request->no_telp_suami) ? $request->no_telp_suami : '',
                    'alamat_suami' => isset($request->alamat_suami) ? $request->alamat_suami : '',
                    'pekerjaan_suami' => isset($request->pekerjaan_suami) ? $request->pekerjaan_suami : '',
                    // -------------------------------------
                    'status_hub_kel_istri' => (!empty($request->nama_suami) AND !empty($request->nama_istri)) ? 'Istri' : '',
                    'nik_istri' => isset($request->nik_istri) ? $request->nik_istri : '',
                    'nama_istri' => isset($request->nama_istri) ? $request->nama_istri : '',
                    'tempat_lahir_istri' => isset($request->tempat_lahir_istri) ? $request->tempat_lahir_istri : '',
                    'tanggal_lahir_istri' => isset($request->tanggal_lahir_istri) ? \Carbon\Carbon::parse($request->tanggal_lahir_istri)->format('Y-m-d') : '0000-00-00',
                    'jenis_kelamin_istri' => isset($request->nama_istri) ? 'P' : '',
                    'agama_istri' => isset($request->agama_istri) ? $request->agama_istri : '',
                    'no_telp_istri' => isset($request->no_telp_istri) ? $request->no_telp_istri : '',
                    'alamat_istri' => isset($request->alamat_istri) ? $request->alamat_istri : '',
                    'pekerjaan_istri' => isset($request->pekerjaan_istri) ? $request->pekerjaan_istri : ''
                ]);
            }
            else
            {
                $rules = array(
                    'no_kk' => 'required',
                );
                $message = array(
                    'no_kk.required' => 'Nomor kartu keluarga wajib diisi !',
                );

                $validator = Validator::make($request->all(), $rules, $message);
                if ($validator->fails()) {
                    return response()->json(['errors'=>$validator->errors()->all()]);
                }

                Warga::where('id', base64_decode($request->keyEdit))->update([
                    'no_kk' => $request->no_kk,
                    // -------------------------------------
                    'status_hub_kel_suami' => (!empty($request->nama_suami) AND !empty($request->nama_istri)) ? 'Suami' : '',
                    'nik_suami' => isset($request->nik_suami) ? $request->nik_suami : '',
                    'nama_suami' => isset($request->nama_suami) ? $request->nama_suami : '',
                    'tempat_lahir_suami' => isset($request->tempat_lahir_suami) ? $request->tempat_lahir_suami : '',
                    'tanggal_lahir_suami' => isset($request->tanggal_lahir_suami) ? \Carbon\Carbon::parse($request->tanggal_lahir_suami)->format('Y-m-d') : '0000-00-00',
                    'jenis_kelamin_suami' => isset($request->nama_suami) ? 'L' : '',
                    'agama_suami' => isset($request->agama_suami) ? $request->agama_suami : '',
                    'no_telp_suami' => isset($request->no_telp_suami) ? $request->no_telp_suami : '',
                    'alamat_suami' => isset($request->alamat_suami) ? $request->alamat_suami : '',
                    'pekerjaan_suami' => isset($request->pekerjaan_suami) ? $request->pekerjaan_suami : '',
                    // -------------------------------------
                    'status_hub_kel_istri' => (!empty($request->nama_suami) AND !empty($request->nama_istri)) ? 'Istri' : '',
                    'nik_istri' => isset($request->nik_istri) ? $request->nik_istri : '',
                    'nama_istri' => isset($request->nama_istri) ? $request->nama_istri : '',
                    'tempat_lahir_istri' => isset($request->tempat_lahir_istri) ? $request->tempat_lahir_istri : '',
                    'tanggal_lahir_istri' => isset($request->tanggal_lahir_istri) ? \Carbon\Carbon::parse($request->tanggal_lahir_istri)->format('Y-m-d') : '0000-00-00',
                    'jenis_kelamin_istri' => isset($request->nama_istri) ? 'P' : '',
                    'agama_istri' => isset($request->agama_istri) ? $request->agama_istri : '',
                    'no_telp_istri' => isset($request->no_telp_istri) ? $request->no_telp_istri : '',
                    'alamat_istri' => isset($request->alamat_istri) ? $request->alamat_istri : '',
                    'pekerjaan_istri' => isset($request->pekerjaan_istri) ? $request->pekerjaan_istri : ''
                ]);
            }
            // hitung ulang jumlah keluarga
            // ---------------------------------
            $this->hitungUlangJmlKeluarga(base64_decode($request->keyEdit));
            $return = ['s' => 'success', 'm' => 'Data berhasil diperbarui !'];
        }
        return response()->json($return, 200);
    }

    public function updateAnak(Request $request)
    {      
        if ($request->ajax()) 
        {
            $fieldData = $request->title;
            $getID = explode('-', $request->id);
            if ($fieldData == 'tanggal_lahir_anak') {
                $value = \Carbon\Carbon::parse($request->tanggal_lahir_anak)->format('Y-m-d');
            }
            else
            {
                $value = $request->value;
            }
            Anak::where('id', $getID[1])->update([
                $request->title => $value 
            ]);
            $msg = ['s' => 'success', 'm' => 'Data berhasil diperbarui !'];
        }
        return response()->json($msg, 200);
    }

    public function storeDataAnak(Request $request)
    {
        if ($request->ajax()) 
        {
            $rules = array(
                'addNamaAnak' => 'required',
                // 'addNikAnak' => 'required'
            );
            $message = array(
                'addNamaAnak.required' => 'Nama Anak wajib diisi !',
                // 'addNikAnak.required' => 'Nik Anak wajib diisi !',
            );
            $validator = Validator::make($request->all(), $rules, $message);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            }
            
            $data = new Anak();
            $data->id_header = $request->keyAnak;
            $data->nik_anak = isset($request->addNikAnak) ? $request->addNikAnak : '';
            $data->nama_anak = $request->addNamaAnak;
            $data->tempat_lahir_anak = isset($request->addTempatLahirAnak) ? $request->addTempatLahirAnak : '';
            $data->tanggal_lahir_anak = isset($request->addTanggalLahirAnak) ? \Carbon\Carbon::parse($request->addTanggalLahirAnak)->format('Y-m-d') : '';
            $data->jenis_kelamin_anak = isset($request->addJenisKelaminAnak) ? $request->addJenisKelaminAnak : '';
            $data->agama_anak = isset($request->addAgamaAnak) ? $request->addAgamaAnak : '';
            $data->save();
            
            $thisId = $data->id;

            // hitung ulang jumlah keluarga
            // ---------------------------------
            $this->hitungUlangJmlKeluarga($request->keyAnak);
            $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
        }
        return response()->json($return, 200);
    }

    public function destroyAnak($id, $idOrtu)
    {
        DB::beginTransaction();
        try {
            $getID = str_replace('remove-', '', $id);
            $delete = Anak::where('id', $getID)->delete();
            // hitung ulang jumlah keluarga
            // ---------------------------------
            $this->hitungUlangJmlKeluarga($idOrtu);
            $return = ['s' => 'success', 'm' => 'Data berhasil dihapus'];
            // ---------------------------------------------------
            DB::commit();
        } catch (\Throwable $e) {
            // ---------------------------------------------------
            DB::rollback();
            // ---------------------------------------------------
            throw $e;
            $return = ['error' => $e];
        }
        return response()->json($return);
    }

    public function destroyKK($id, $img)
    {
        DB::beginTransaction();
        try {
            $key = base64_decode($id);
            $pic = base64_decode($img);
            $findData = Warga::find($key);

            if (!empty($img))
            {
                // $urlPath = storage_path('storage/file/'.$findData->file_kk);
                $urlPath = public_path('storage/file/'.$findData->file_kk);
                // print_r($urlPath); die;
                unlink($urlPath);
                if (!empty($findData->file_kk))
                {
                    Warga::where('id', $key)->update([
                        'file_kk' => '',
                        'storage' => ''
                    ]);
                }
            }
            $return = ['s' => 'success', 'm' => 'KK berhasil dihapus'];
            // ---------------------------------------------------
            DB::commit();
        } catch (\Throwable $e) {
            // ---------------------------------------------------
            DB::rollback();
            // ---------------------------------------------------
            throw $e;
            $return = ['error' => $e];
        }
        return response()->json($return);
    }

    public function hitungUlangJmlKeluarga($idHeader)
    {
        $checkJmlOrtu = $this->checkJmlOrtu($idHeader);

        $jmlAnak = Anak::where('id_header', $idHeader)->get();
        $getJumlahAnak = count($jmlAnak);

        // update jumlah keluarga
        Warga::where('id', $idHeader)->update([
            'jumlah_keluarga' => ($checkJmlOrtu['checkAayah'] + $checkJmlOrtu['checkIbu']) + $getJumlahAnak // (ayah+ibu) + anak
        ]);
        return true;
    }
}
