<?php

namespace App\Http\Controllers;

use App\User;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('akun.listAkun');
    }

    public function datatablesUser()
    {
        $data = DB::table('users')->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('is_active', function ($a) {
                if ($a->is_active == '1') {
                    $info = '<i class="fa fa-check" aria-hidden="true" style="color: #1ce318;" title="Active"></i>';
                } else {
                    $info = '<i class="fa fa-times" aria-hidden="true" style="color: #f00;" title="Non Active"></i>';
                }
               return $info;
            })
            ->addColumn('action', function($row) {
                $btn = '
                    <div class="hidden-sm hidden-xs btn-group">
                        <button class="btn btn-xs btn-info btn-edit" onclick="changePassword('.$row->id.')">
                            <i class="ace-icon fa fa-edit bigger-120"></i>
                        </button> &nbsp
                    </div>
                ';
                return $btn;
            })
            ->rawColumns(['is_active', 'action'])
            ->make(true);
    }

    public function edit($param)
    {
        $data = DB::table('users')->where('id', '=', $param)->get();
        return response()->json(['s' => 'success', 'data' => $data], 200);
    }

    public function store(Request $request)
    {
        if ($request->ajax()) 
        {
             $rules = array(
                'name' => 'required',
                'email' => 'required | unique:users',
                'password' => 'required',
                'roleAccess' => 'required'
            );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'role' => $request->roleAccess,
                'is_active' => 1
            ]);
            $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
        }
        return response()->json($return, 200);
    }

    public function update(Request $request)
    {
        // dd($request);
        if ($request->ajax()) 
        {
            $rules = array(
                'edit_name' => 'required',
                'edit_email' => 'required',
                // 'edit_password' => 'required',
                'edit_roleAccess' => 'required',
                'edit_is_active' => 'required'
            );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            User::where('id', base64_decode($request->id))->update([
                'name' => $request->edit_name,
                'email' => $request->edit_email,
                // 'password' => bcrypt($request->edit_password),
                'role' => $request->edit_roleAccess,
                'is_active' => $request->edit_is_active
            ]);
            $return = ['s' => 'success', 'm' => 'Data berhasil diperbarui !'];
        }
        return response()->json($return, 200);
    }

    public function updatePassword(Request $request)
    {
        if ($request->ajax()) 
        {
            $rules = array(
                'passwordNew' => 'required',
            );

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            User::where('id', $request->keyEdit)->update([
                'password' => bcrypt($request->passwordNew)
            ]);
            $return = ['s' => 'success', 'm' => 'Password berhasil diperbarui !'];
        }
        return response()->json($return, 200);
    }
}
