<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransPemasukan extends Model
{
    protected $table = 'transaksi_pemasukan';
    protected $fillable = [
        'kategori_pemasukan_id',
        'tanggal_pemasukan',
        'nominal',
        'keterangan'
    ];
}
