<?php

namespace App;

use DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class LapMasukKeluarKasExcel implements FromView, ShouldAutoSize, WithEvents
{
    public function view(): View
    {
        $filterType = isset($_GET['filterType3']) ? $_GET['filterType3'] : '';
        $filterBy = isset($_GET['filterBy3']) ? $_GET['filterBy3'] : '';
        $yearWithMonth = isset($_GET['yearWithMonth3']) ? $_GET['yearWithMonth3'] : '';
        $format = '';
        $format2 = '';
        $headerInfo = '';
         
        if (empty($filterBy)) 
        {
            $value = 'notFound!';
            $year = '';
            $format = '';
            $format2 = '';
            $headerInfo = '';
        } 
        else 
        {
            if ($filterType == 2) 
            {
                $format = '%Y/%m';
                $format2 = '%M %Y';
                $where = '%Y';
                $value = $filterBy;
                $headerInfo = "ANGGARAN KAS PER TAHUN $value";
                $headerInfo2 = "RT XXX / RW XXX";
                $headerInfo3 = "PERUMAHAN PESONA KAHURIPAN 3 TAHAP 4";
            }
        }

        $data = DB::table('transaksi_pemasukan as a')
            ->select(
                DB::raw("(DATE_FORMAT(a.tanggal_pemasukan, '$format2')) as tanggal"), 
                DB::raw("SUM(a.nominal) as kas_masuk"),  
                DB::raw("(SELECT sum(b.nominal) 
                            FROM transaksi_pengeluaran b 
                            WHERE DATE_FORMAT(b.tanggal_pengeluaran, '$format') = DATE_FORMAT(a.tanggal_pemasukan, '$format') 
                            GROUP BY ( DATE_FORMAT( b.tanggal_pengeluaran, '%Y/%m' ) ) LIMIT 1) as `kas_keluar`")
            )
            ->where(DB::raw("(DATE_FORMAT(a.tanggal_pemasukan, '$where'))"), '=', $value)
            ->groupBy(DB::raw("(DATE_FORMAT(a.tanggal_pemasukan,'$format'))"))
            ->get();

        return view('laporan.lapMasukKeluarKasExcel', [
            'data' => $data,
            'headerInfo' => [$headerInfo, $headerInfo2, $headerInfo3]
        ]);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) 
            {
                $cellHeaderTitle = 'A2:E2'; // header title
                $cellHeaderTitle2 = 'A3:E3'; // header title
                $cellHeaderTitle3 = 'A4:E4'; // header title
                $styleArrayTitle = [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ];
                $event->sheet->getStyle($cellHeaderTitle)->applyFromArray($styleArrayTitle);
                $event->sheet->getStyle($cellHeaderTitle2)->applyFromArray($styleArrayTitle);
                $event->sheet->getStyle($cellHeaderTitle3)->applyFromArray($styleArrayTitle);
                // -----------------------------------------------------------------------------
                // -----------------------------------------------------------------------------
                $cellHeaderTable = 'A6:E6'; // header tbl
                $styleHeaderTable = [
                    'font' => [
                        'bold' => TRUE,
                        'size' => 13,
                        'color' => [ 'rgb' => 'ffffff' ]
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['rgb' => '000000'],
                        ],
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => '008000',
                        ]
                    ],
                ];
                // $event->sheet->getDelegate()->getStyle($cellHeaderTable)->getFont()->setSize(13);
                $event->sheet->getStyle($cellHeaderTable)->applyFromArray($styleHeaderTable);
            }
        ];
    }
}
