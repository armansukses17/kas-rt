<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransPengeluaran extends Model
{
    protected $table = 'transaksi_pengeluaran';
    protected $fillable = [
        'kategori_pengeluaran_id',
        'tanggal_pengeluaran',
        'nominal',
        'keterangan'
    ];
}
