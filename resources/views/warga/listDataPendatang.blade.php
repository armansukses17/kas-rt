@extends('master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h2>Data Warga Berdasarkan Kepala Keluarga</h2>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body" style="padding: 0;">
                            <div class="col-lg-12" style="padding: 0;">
                                <div class="card card-primary card-outline card-outline-tabs" style="margin: 0;">
                                    <div class="card-header p-0 border-bottom-0">
                                        <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ '/dataWarga' }}" >Warga Tetap</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active">Warga Pendatang</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="card-body" id="cardBodyContentTab">
                                        <div class="tab-content" id="custom-tabs-three-tabContent">
                                            <div class="tab-pane fade active show">
                                                 <div class="card-body p-0 mt-3">
                                                     <div class="hidden-sm hidden-xs btn-group">
                                                        <a href="{{ 'warga/createPendatang' }}" class="btn btn-danger btn-sm btn-add"><i class="fa fa-plus"></i> Tambah Warga Pendatang</a>
                                                    </div>
                                                    <hr>
                                                    <table class="table table-hover table-bordered" id="tblData">
                                                        <thead>
                                                            <tr>
                                                                <td width="5px">No</td>
                                                                <td>No. KK</td>
                                                                <td>Nama</td>
                                                                <td>No. Telp.</td>
                                                                <td>Alamat</td>
                                                                <td>Jumlah Keluarga</td>
                                                                <td width="12px">Aksi</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modalShowKK" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-modal="true">
    <div class="modal-dialog modalXLarge">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">KARTU KELUARGA</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeImgKK()">&times;</button>
            </div>
            <img src="" id="full-img-kk" alt="image-kk" width="100%">
        </div>
    </div>
</div>

<!-- start modal edit -->
<div class="modal fade" id="modalEditData" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-modal="true">
    <div class="modal-dialog modalXLarge">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detai Data Warga</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            
            <form id="formEditData">
                {{ method_field('PUT') }}
                <div class="modal-body">
                    <div class="row">
                        <div class="card-body">
                            {{ csrf_field() }}
                            <input type="hidden" name="keyEdit" id="keyEdit">
                            <fieldset>
                                <div class="form-group">
                                    <label>Nomor Kepala Keluarga (No. KK)</label>
                                    <input type="text" name="no_kk" id="no_kk" class="form-control">
                                    <input type="hidden" name="status_warga" value="<?php echo base64_encode('Pendatang'); ?>">
                                </div>
                                <div id="imgKkShow" class="form-group" style="position: relative; width: 100px;">
                                    <input type="hidden" name="imageKkTypeHidden" id="imageKkTypeHidden">
                                    <div onclick="deleteImageKK()" style="position: absolute; color: #fff; right: 0; background: #f00; line-height: 12px; height: 15px; width: 15px; text-align: center; cursor: pointer;">x</div>
                                    <a onclick="showImageKK()" style="cursor: pointer;">
                                        <div style="position: absolute; left: 50%; top: 50%; transform: translate(-50%,-50%); font-size: 25px; color: #007bff;"><i class="fa fa-search-plus"></i></div>
                                    </a>
                                    <img src="" id="imageKK" alt="image-kk" width="100">
                                    <!-- {{ asset('storage/file/5a1924496964648337ec244fd9271803.jpg') }} -->
                                </div>
                                <div id="uploadNewKK" class="form-group" style="display: none;">
                                    <label class="required">Upload KK</label>
                                    <input type="file" name="file_kk_edit" id="file_kk_edit" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Anggota Keluarga</label>
                                    </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card card-primary card-outline card-outline-tabs">
                                            <div class="card-header p-0 border-bottom-0">
                                                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" id="column-suami-tab" data-toggle="pill" href="#column-suami" role="tab" aria-controls="column-suami" aria-selected="true">SUAMI</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="column-istri-tab" data-toggle="pill" href="#column-istri" role="tab" aria-controls="column-istri" aria-selected="false">ISTRI</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="column-anak-tab" data-toggle="pill" href="#column-anak" role="tab" aria-controls=column-anak" aria-selected="false">ANAK</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="card-body">
                                                <div class="tab-content" id="custom-tabs-three-tabContent">
                                                    <div class="tab-pane fade active show" id="column-suami" role="tabpanel" aria-labelledby="column-suami-tab">
                                                        <div class="form-group">
                                                            <label>Nama Lengkap</label>
                                                            <input type="text" name="nama_suami" id="nama_suami" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>NIK</label>
                                                            <input type="text" name="nik_suami" id="nik_suami" class="form-control">
                                                        </div>
                                                        <!-- <div class="form-group">
                                                            <label>Status Hubungan Keluarga</label>
                                                            <select class="form-control" name="status_hub_kel_suami" id="status_hub_kel_suami">
                                                                <option value="">-- Pilih --</option>
                                                                <option value="Kepala Keluarga">Kepala Keluarga</option>
                                                                <option value="Istri">Istri</option>
                                                                <option value="Anak">Anak</option>
                                                            </select>
                                                        </div> -->
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Tempat Lahir</label>
                                                                    <input type="text" name="tempat_lahir_suami" id="tempat_lahir_suami" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Tanggal Lahir</label>
                                                                    <input type="text" name="tanggal_lahir_suami" id="tanggal_lahir_suami" class="form-control datepicker-bts" autocomplete="off">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="form-group">
                                                            <label for="role">Jenis Kelamin</label>
                                                            <br>
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="opt1_suami" name="jenis_kelamin_suami" class="custom-control-input" value="L">
                                                                <label class="custom-control-label" for="opt1_suami">Laki - Laki</label>
                                                            </div>
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="opt2_suami" name="jenis_kelamin_suami" class="custom-control-input" value="P">
                                                                <label class="custom-control-label" for="opt2_suami">Perempuan</label>
                                                            </div>
                                                        </div> -->
                                                        <div class="form-group">
                                                            <label>Agama</label>
                                                            <select class="form-control" name="agama_suami" id="agama_suami">
                                                                <option value="">-- Pilih --</option>
                                                                <option value="islam">Islam</option>
                                                                <option value="kristen">Kristen</option>
                                                                <option value="hindu">Hindu</option>
                                                                <option value="budha">Budha</option>
                                                                <option value="katolik">Katolik</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>No. telp</label>
                                                            <input type="text" name="no_telp_suami" id="no_telp_suami" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Alamat</label>
                                                            <textarea name="alamat_suami" id="alamat_suami" class="form-control input-sm" rows="4"></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Pekerjaan</label>
                                                            <select class="form-control" name="pekerjaan_suami" id="pekerjaan_suami">
                                                                <option value="">-- Pilih --</option>
                                                                <option value="PNS">PNS</option>
                                                                <option value="TNI">TNI</option>
                                                                <option value="Polri">Polri</option>
                                                                <option value="Guru">Guru</option>
                                                                <option value="Wiraswasta">Wiraswasta</option>
                                                                <option value="Karyawan Swasta">Karyawan Swasta</option>
                                                                <option value="Buruh">Buruh</option>
                                                                <option value="Petani">Petani</option>
                                                                <option value="Nelayan">Nelayan</option>
                                                                <option value="Lainnya">Lainnya</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="column-istri" role="tabpanel" aria-labelledby="column-istri-tab">
                                                        <div class="form-group">
                                                            <label>Nama Lengkap</label>
                                                            <input type="text" name="nama_istri" id="nama_istri" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>NIK</label>
                                                            <input type="text" name="nik_istri" id="nik_istri" class="form-control">
                                                        </div>
                                                        <!-- <div class="form-group">
                                                            <label>Status Hubungan Keluarga</label>
                                                            <select class="form-control" name="status_hub_kel_istri" id="status_hub_kel_istri">
                                                                <option value="">-- Pilih --</option>
                                                                <option value="Kepala Keluarga">Kepala Keluarga</option>
                                                                <option value="Istri">Istri</option>
                                                                <option value="Anak">Anak</option>
                                                            </select>
                                                        </div> -->
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Tempat Lahir</label>
                                                                    <input type="text" name="tempat_lahir_istri" id="tempat_lahir_istri" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Tanggal Lahir</label>
                                                                    <input type="text" name="tanggal_lahir_istri" id="tanggal_lahir_istri" class="form-control datepicker-bts" autocomplete="off">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="form-group">
                                                            <label for="role">Jenis Kelamin</label>
                                                            <br>
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="opt1_istri" name="jenis_kelamin_istri" class="custom-control-input" value="L">
                                                                <label class="custom-control-label" for="opt1_istri">Laki - Laki</label>
                                                            </div>
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="opt2_istri" name="jenis_kelamin_istri" class="custom-control-input" value="P">
                                                                <label class="custom-control-label" for="opt2_istri">Perempuan</label>
                                                            </div>
                                                        </div> -->
                                                        <div class="form-group">
                                                            <label>Agama</label>
                                                            <select class="form-control" name="agama_istri" id="agama_istri">
                                                                <option value="">-- Pilih --</option>
                                                                <option value="islam">Islam</option>
                                                                <option value="kristen">Kristen</option>
                                                                <option value="hindu">Hindu</option>
                                                                <option value="budha">Budha</option>
                                                                <option value="katolik">Katolik</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>No. telp</label>
                                                            <input type="text" name="no_telp_istri" id="no_telp_istri" class="form-control">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Alamat</label>
                                                            <textarea name="alamat_istri" id="alamat_istri" class="form-control input-sm" rows="4"></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Pekerjaan</label>
                                                            <select class="form-control" name="pekerjaan_istri" id="pekerjaan_istri">
                                                                <option value="">-- Pilih --</option>
                                                                <option value="PNS">PNS</option>
                                                                <option value="TNI">TNI</option>
                                                                <option value="Polri">Polri</option>
                                                                <option value="Guru">Guru</option>
                                                                <option value="Wiraswasta">Wiraswasta</option>
                                                                <option value="Karyawan Swasta">Karyawan Swasta</option>
                                                                <option value="Buruh">Buruh</option>
                                                                <option value="Petani">Petani</option>
                                                                <option value="Nelayan">Nelayan</option>
                                                                <option value="Lainnya">Lainnya</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="column-anak" role="tabpanel" aria-labelledby="column-anak-tab">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <spa style="font-size: 14px;"n>Nama Anak (Urutkan dari yang tertua)</span>
                                                            </div>
                                                            <div class="col-md-6 text-right">
                                                                <a href="#modalAddDataAnak" role="button" data-toggle="modal">
                                                                    <button type="button" class="btn btn-sm btn-success btn-add" title="Tambah">
                                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                                    </button>
                                                                </a>
                                                        </div>
                                                        </div>
                                                        <table class="table table-bordered table-hover" id="tableAanak">
                                                            <thead style="line-height: 30px;">
                                                                <tr>
                                                                    <th style="width: 140px;">Nik</th>
                                                                    <th style="width: 210px;">Nama Anak</th>
                                                                    <th style="width: 140px;">Tempat Lahir</th>
                                                                    <th style="width: 90px;">Tanggal Lahir</th>
                                                                    <th style="width: 90px;">Jenis Kelamin</th>
                                                                    <th style="width: 90px;">Agama</th>
                                                                    <th style="width: 10px;">#</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="bodyListData">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.card -->
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="btnClose" data-dismiss="modal" aria-hidden="true">Keluar</button>
                    <button type="submit" class="btn btn-primary" id="btnUpdate">Perbarui</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal edit -->

<div class="modal fade" id="modalAddDataAnak" role="dialog" data-keyboard="false" data-backdrop="static" aria-modal="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Anak</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>

            <div class="msgAlert"></div>

            <form id="formAddAnak"> <!-- method="post" action="saveUnit" -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Nik</label>
                                <input type="text" name="addNikAnak" id="addNikAnak" class="form-control">
                                <input type="hidden" name="keyAnak" id="keyAnak">
                            </div>
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" name="addNamaAnak" id="addNamaAnak" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input type="text" name="addTempatLahirAnak" id="addTempatLahirAnak" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <input type="text" name="addTanggalLahirAnak" id="addTanggalLahirAnak" class="form-control datepicker-bts-modal" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label>Jenis Kelamin</label>
                                <select class="chosen-select form-control" name="addJenisKelaminAnak" id="addJenisKelaminAnak">
                                    <option value="">-- Pilih --</option>
                                    <option value="L"="">Laki - laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                             <div class="form-group">
                                <label>Agama</label>
                                <select class="chosen-select form-control" name="addAgamaAnak" id="addAgamaAnak">
                                    <option value="">-- Pilih --</option>
                                    <option value="islam">Islam</option>
                                    <option value="kristen">Kristen</option>
                                    <option value="hindu">Hindu</option>
                                    <option value="budha">Budha</option>
                                    <option value="katolik">Katolik</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" value="Simpan">
                </div>
            </form>
        </div>
    </div>
</div><!-- End: modal -->

<style>
.modal #bodyListData td {
    padding: 0;
}
.modal #bodyListData td input,
.modal #bodyListData td select {
    font-size: 10px;
}
</style>

<script>
var BASE_URL = {!! json_encode(url('/')) !!}

$(document).on('ready', function() {
    $("#tblData").dataTable().fnDestroy();
});

$('#modalAddDataAnak .close').on('click', function() {
    $('#modalEditData').css('overflow', 'auto');
    $('body').css('overflow', 'hidden');
});

$(function() {
    callDataTables();
});

function callDataTables() {
    $("#tblData").dataTable().fnDestroy();
    listDataTables();
}

function listDataTables(filterBy='') {
    $('#tblData').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ '/datatablesWargaPendatang' }}",
            data: {
                // _token: '{{ csrf_token() }}',
                filterBy: filterBy
            },
            type: "GET"
        },
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false }, // no autoincrement
            { data: 'no_kk', name: 'no_kk' },
            { data: 'suamiDanIstri', name: 'suamiDanIstri' },
            { data: 'noTelp', name: 'noTelp' },
            { data: 'alamatKeluarga', name: 'alamatKeluarga' },
            { data: 'jumlah_keluarga', name: 'jumlah_keluarga' },
            { data: 'action', name: 'action' }
        ],
        // "order": [[ '1', "desc" ]]
    });
}

function showImageKK() {
    $('#modalShowKK').modal().css('overflow', 'auto');
    $('#modalEditData').modal('hide');
}

function closeImgKK() {
    $('#modalEditData').modal('show').css('overflow', 'auto');
}

function deleteImageKK() {
    if (confirm('YAKIN AKAN MENGGANTI KK INI ?')) {
        var key = $('#keyEdit').val();
        var img = $('#imageKkTypeHidden').val();
        $.ajax({
            type: "GET",
            url : "{{ 'warga/deleteKK/' }}"+key+"/"+img,
            dataType: "json",
            success: function(data) {
                if (data.s == 'success') {
                    callDataTables();
                    $('#imgKkShow').hide();
                    $('#uploadNewKK').show();
                    $('#file_kk_edit').val('');
                    console.log(data.m);
                } 
                else {
                    alert('error');
                }
            }
        }); 
    }
}

$('#column-anak-tab').on('click', function() {
    $('#btnUpdate, #btnClose').hide();
});

$('#column-suami-tab, #column-istri-tab').on('click', function() {
    $('#btnUpdate, #btnClose').show();
});

function editWarga(param) {
    $('#imgKkShow').show();
    $('#uploadNewKK').hide();
    $.ajax({
        type: "GET",
        url : "{{ '/warga/edit/' }}"+param,
        dataType: "json",
        success: function(data) {
            if (data.s == 'success') {
                $('#modalEditData').modal();
                $('#keyEdit').val(btoa(param));
                $('#no_kk').val(data.data[0].no_kk);
                $("#full-img-kk, #imageKK").attr("src", BASE_URL+'/storage/file/'+data.data[0].file_kk);
                $("#imageKkTypeHidden").val(btoa(data.data[0].file_kk));

                if (data.data[0].file_kk == '') {
                    $('#imgKkShow').hide();
                    $('#uploadNewKK').show();
                }
                // ---------------------------------------------
                // suami
                $('#nik_suami').val(data.data[0].nik_suami);
                $('#nama_suami').val(data.data[0].nama_suami);
                $('#no_telp_suami').val(data.data[0].no_telp_suami);
                $('#tempat_lahir_suami').val(data.data[0].tempat_lahir_suami);
                $('#tanggal_lahir_suami').val(data.data[0].tanggal_lahir_suami);
                $('#agama_suami').val(data.data[0].agama_suami);
                $('#alamat_suami').val(data.data[0].alamat_suami);
                $('#pekerjaan_suami').val(data.data[0].pekerjaan_suami);
                $('#status_hub_kel_suami').val(data.data[0].status_hub_kel_suami);
                // if (data.data[0].jenis_kelamin_suami == 'L') {
                //     $('#opt1_suami').prop('checked', true);
                // } else {
                //     $('#opt2_suami').prop('checked', true);
                // }
                // ---------------------------------------------
                // istri
                $('#nik_istri').val(data.data[0].nik_istri);
                $('#nama_istri').val(data.data[0].nama_istri);
                $('#no_telp_istri').val(data.data[0].no_telp_istri);
                $('#tempat_lahir_istri').val(data.data[0].tempat_lahir_istri);
                $('#tanggal_lahir_istri').val(data.data[0].tanggal_lahir_istri);
                $('#agama_istri').val(data.data[0].agama_istri);
                $('#alamat_istri').val(data.data[0].alamat_istri);
                $('#pekerjaan_istri').val(data.data[0].pekerjaan_istri);
                $('#status_hub_kel_istri').val(data.data[0].status_hub_kel_istri);
                // if (data.data[0].jenis_kelamin_istri == 'L') {
                //     $('#opt1_istri').prop('checked', true);
                // } else {
                //     $('#opt2_istri').prop('checked', true);
                // }
                // ---------------------------------------------
                // anak
                var idOrtu = $('#keyAnak').val(data.data[0].id);

                if (data.data.anak != '') {
                    var html = '';
                    var jenis_kelamin_anak = '';
                    var agama_anak = '';
                    var lakiLaki = '';
                    var perempuan = '';
                    var islam = '';
                    var kristen = '';
                    var hindu = '';
                    var budha = '';
                    var katolik = '';
                    for (i=0; i<data.data.anak.length; i++) {
                        start = i + 1;

                        if ('L' == data.data.anak[i].jenis_kelamin_anak) {
                            lakiLaki = 'selected';
                            perempuan =  '';
                        } 
                        else if ('P' == data.data.anak[i].jenis_kelamin_anak) {
                            lakiLaki = '';
                            perempuan =  'selected';
                        } 
                        else {
                            lakiLaki = '';
                            perempuan =  '';
                        }

                        if ('islam' == data.data.anak[i].agama_anak) {
                            islam = 'selected';
                            kristen = '';
                            hindu = '';
                            budha = '';
                            katolik = '';
                        } 
                        else if ('kristen' == data.data.anak[i].agama_anak) {
                            islam = '';
                            kristen = 'selected';
                            hindu = '';
                            budha = '';
                            katolik = '';
                        } 
                        else if ('hindu' == data.data.anak[i].agama_anak) {
                            islam = '';
                            kristen = '';
                            hindu = 'selected';
                            budha = '';
                            katolik = '';
                        } 
                        else if ('budha' == data.data.anak[i].agama_anak) {
                            islam = '';
                            kristen = '';
                            hindu = '';
                            budha = 'selected';
                            katolik = '';
                        } 
                        else if ('katolik' == data.data.anak[i].agama_anak) {
                            islam = '';
                            kristen = '';
                            hindu = '';
                            budha = '';
                            katolik = 'selected';
                        } 
                        else {
                            islam = '';
                            kristen = '';
                            hindu = '';
                            budha = '';
                            katolik = '';
                        }

                        html += `<tr id="row-${data.data.anak[i].id}}">
                                <td>
                                    <input type="text" name="nik_anak[]" id="nik_anak-${data.data.anak[i].id}" class="form-control" value="${data.data.anak[i].nik_anak}" onchange="changeValue(this.id, this.value, this.title)" title="nik_anak" autocomplete="off">
                                </td>
                                <td>
                                    <input type="text" name="nama_anak[]" id="nama_anak-${data.data.anak[i].id}" class="form-control" value="${data.data.anak[i].nama_anak}" onchange="changeValue(this.id, this.value, this.title)" title="nama_anak" autocomplete="off">
                                </td>
                                <td>
                                    <input type="text" name="tempat_lahir_anak[]" id="tempat_lahir_anak-${data.data.anak[i].id}" class="form-control" value="${data.data.anak[i].tempat_lahir_anak}" onchange="changeValue(this.id, this.value, this.title)" title="tempat_lahir_anak" autocomplete="off">
                                </td>
                                <td>
                                    <input type="text" name="tanggal_lahir_anak[]" id="tanggal_lahir_anak-${data.data.anak[i].id}" class="form-control datepicker-bts-modal" value="${data.data.anak[i].tanggal_lahir_anak}" onchange="changeValue(this.id, this.value, this.title)" title="tanggal_lahir_anak" autocomplete="off">
                                </td>
                                <td>
                                    <select class="chosen-select form-control" name="jenis_kelamin_anak[]" id="jenis_kelamin_anak-${data.data.anak[i].id}" onchange="changeValue(this.id, this.value, this.title)" title="jenis_kelamin_anak">
                                        <option value="">-- Pilih --</option>
                                        <option value="L" ${lakiLaki}>Laki - laki</option>
                                        <option value="P" ${perempuan}>Perempuan</option>
                                    </select>
                                </td>
                                <td>
                                    <select class="chosen-select form-control" name="agama_anak[]" id="agama_anak-${data.data.anak[i].id}" onchange="changeValue(this.id, this.value, this.title)" title="agama_anak">
                                        <option value="">-- Pilih --</option>
                                        <option value="islam" ${islam}>Islam</option>
                                        <option value="kristen" ${kristen}>Kristen</option>
                                        <option value="hindu" ${hindu}>Hindu</option>
                                        <option value="budha" ${budha}>Budha</option>
                                        <option value="katolik" ${katolik}>Katolik</option>
                                    </select>
                                </td>
                                <td>
                                    <div class="hidden-sm hidden-md btn-group"> 
                                        <button type="button" class="btn btn-md btn-danger btn-remove delete-record" id="remove-${data.data.anak[i].id}" onclick="removeData(this.id)">
                                            <i class="fa fa-times" aria-hidden="true" style="line-height: 23px;"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        `;
                    }
                    $('#bodyListData').html(html);
                }
            } 
            else {
                alert('error');
            }
        }
    }); 
}

function removeData(param) {
    if (confirm('Are you sure you want to delete this?')) {
        var param2 = $('#keyAnak').val();
        $.ajax({
            type: "GET",
            url : "{{ 'anak/delete/' }}"+param+"/"+param2,
            dataType: "json",
            success: function(data) {
                if (data.s == 'success') {
                    toastr.success(data.m);
                    var keyData = $('#keyAnak').val();
                    editWarga(keyData)
                    callDataTables();
                } 
                else {
                    alert('error');
                }
            }
        }); 
    }
}

function changeValue(id, value, title) {
    // alert(id); return false;
    $.ajax({
        type: "PUT",
        url : "{{ '/updateAnak' }}",
        data: {
            _token: '{{ csrf_token() }}',
            id: id,
            value: value,
            title: title
        },
        dataType: "json",
        success: function(data) {
            if (data.s == 'success') {
                toastr.success('Berhasil diinput !');
                callDataTables();
            } 
            else {
                toastr.error(data.m);
            }
        },
        error: function(data) {
            toastr.error('Error !');
        }
    });
    return false; 
}


$('#formEditData').on('submit', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        type: "POST",
        url : "{{ '/updateWargaPendatang' }}",
        data: formData,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
            if (data.s == 'success') {
                toastr.success(data.m); 
                callDataTables();
                $('.modal').modal('hide');
            } 
            else {
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">- '+value+'</span><br>';
                });
                toastr.error(messages);
            }
        }
    }); 
});

// button hapus
$('table').on('click', '.btn-hapus', function () {
    var id = $(this).attr('data-value');
    var nama = $(this).attr('data-nama');
    $('.id').val(id);
    $('.nama').text(nama);
    $('#modalHapus').modal('show');
});

$('#formAddAnak').on('submit', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        type: "POST",
        url : "{{ '/addDataAnak' }}",
        data: formData,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
            if (data.s == 'success') {
                toastr.success(data.m);
                callDataTables();
                $('.modal').modal('hide');
                $('#formAddAnak')[0].reset(); // reset form
                var keyData = $('#keyAnak').val();
                editWarga(keyData)
            } 
            else {
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">'+value+'</span>';
                });
                toastr.error(messages);
            }
        },
        error: function(data) {
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">'+value+'</span>';
                });
                toastr.error(messages);
        }
    }); 
});
</script>
@endsection
