$(function () {
    'use strict'

    $('.daterange').daterangepicker({
        ranges: {
            Today: [moment(), moment()],
            Yesterday: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate: moment()
    }, function (start, end) {
        // eslint-disable-next-line no-alert
        alert('You chose: ' + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    })

    //Datemask dd/mm/yyyy
    // $("#datemask").inputmask("dd/mm/yyyy", { placeholder: "dd/mm/yyyy" });
    // $("#datemask").inputmask("dd/mm/yyyy", { placeholder: "dd/mm/yyyy" });
    // //Datemask2 mm/dd/yyyy
    // $("#datemask2").inputmask("mm/dd/yyyy", { placeholder: "mm/dd/yyyy" });
    // //Money Euro
    // $("[data-mask]").inputmask();

    //Date range picker
    $("#reservation").daterangepicker();
    //Date range picker with time picker
    $("#reservationtime").daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: "MM/DD/YYYY hh:mm A",
        },
    });

    // Bootstrap Date picker
    $(".datepicker-bts").datepicker({
        format: 'dd-mm-yyyy',
        orientation: "bottom auto",
        autoclose: true
    });

     // Bootstrap Date picker in modal
    $("body").delegate(".datepicker-bts-modal", "focusin", function () {
        $(this).datepicker({
            format: 'dd-mm-yyyy',
            orientation: "bottom auto",
            autoclose: true
        });
    });

    $("body").delegate(".money", "focusin", function () {
        $(this).mask('#.##0', { reverse: true });
    });

    // Ui Date picker
    $(".datepicker-ui").datepicker();
})
